#include <iostream>
#include <sstream>

#include <list>

#include <Ogre/AnimationSystem.h>
#include <TriggerFunctor.h>

#include <NodeFactory.h>

#include <ExampleApplication.h>
#include <OIS/OIS.h>

using namespace tecnofreak;
using namespace tecnofreak::ogre;

class SimpleLineGraph
{
public:
	SimpleLineGraph( const std::string& name, Ogre::SceneManager* sceneManager, int size )
	{
		m_lineGraph = sceneManager->createManualObject( name );
		m_lineGraph->setDynamic( true );
		m_lineGraph->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP );

		for ( int i = 0; i < size; i++ )
		{
			Ogre::Vector3 point( 0.8, 0, -1 );
			m_lineGraph->position( point );
			m_oldPoints.push_back( point );
		}

		m_lineGraph->end();

		Ogre::SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode();

		node->attachObject( m_lineGraph );


		m_lineGraph->setUseIdentityProjection( true );
		m_lineGraph->setUseIdentityView( true );
	}

	virtual ~SimpleLineGraph()
	{
	}

	void addValue( float elapsedSeconds, float measurement )
	{
		Ogre::Vector3 newPoint( 0.8, measurement, 0 );

		m_oldPoints.pop_front();

		m_lineGraph->beginUpdate( 0 );

		for ( std::list< Ogre::Vector3 >::iterator it = m_oldPoints.begin(); it != m_oldPoints.end(); it++ )
		{
			Ogre::Vector3& point = *it;
			it->x -= elapsedSeconds;
			if ( it->x < -0.8 )
			{
				it->x = -0.8;
			}

			m_lineGraph->position( point );

		}

		m_lineGraph->position( newPoint );

		m_lineGraph->end();

		m_oldPoints.push_back( newPoint );
	}

	std::list< Ogre::Vector3 > m_oldPoints;
	Ogre::ManualObject* m_lineGraph;
};

class TestApplication
	: public ExampleApplication
	, public Ogre::FrameListener
	, public OIS::MouseListener
{
public:
	TestApplication()
		: m_mouse( NULL )
		, m_frikiAnimationSystem( NULL )
		, m_attackListener( NULL )
		, m_listener( NULL )
		, m_testGraph( NULL )
	{
	}

	~TestApplication()
	{
		delete m_frikiAnimationSystem;
		delete m_attackListener;
		delete m_listener;
		delete m_testGraph;
	}

protected:

	class TestFrameListener : public ExampleFrameListener
	{
	public:
		TestFrameListener( RenderWindow* win, Camera* cam ) : ExampleFrameListener( win, cam, false, true ) {}
		OIS::Mouse* getMouse() { return mMouse; }
	};

	void createFrameListener()
	{
		TestFrameListener* frameListener = new TestFrameListener( mWindow, mCamera );
		mFrameListener = frameListener;
		mFrameListener->showDebugOverlay( true );
		mRoot->addFrameListener( mFrameListener );

		m_mouse = frameListener->getMouse();

		m_mouse->setEventCallback( this );
	}

	void triggerListener( const tecnofreak::ITrigger* trigger )
	{
		std::cout << "Received trigger " << trigger->getName() << std::endl;
	}

	void attackEndTriggerListener( const tecnofreak::ITrigger* trigger )
	{
		std::cout << "I only listen to the attack end trigger" << std::endl;
		m_frikiState->setValue( 1 );
	}

	void createScene()
	{

		mSceneMgr->setAmbientLight( ColourValue( 1, 1, 1 ) );

		Entity* ent1 = mSceneMgr->createEntity( "Friki", "Friki.mesh" );
		SceneNode* node1 = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		node1->attachObject( ent1 );

		m_frikiAnimationSystem = new AnimationSystem( ent1 );

		m_frikiAnimationSystem->loadAnimationTree( "diagram.xml" );
		m_frikiAnimationSystem->loadAnimationInfo( "animation_info.xml" );

		//testing the loading of the resources with the ogre resource system from a zipped file:
		//m_frikiAnimationSystem->loadAnimationTree( "diagram_ogreresource.xml", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
		//m_frikiAnimationSystem->loadAnimationInfo( "animation_info_ogreresource.xml", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );

		// I'd like to use boost::shared_ptr and boost::function for this->
		m_attackListener = new TriggerFunctor< TestApplication >( this, &TestApplication::attackEndTriggerListener );
		m_listener = new TriggerFunctor< TestApplication >( this, &TestApplication::triggerListener );

		m_frikiAnimationSystem->addSubscriber( "attack_end", m_attackListener );
		m_frikiAnimationSystem->addSubscriber( m_listener );

		// test if reloading of an animation system works
		//m_frikiAnimationSystem->load( "diagram.xml" );

		m_frikiState = m_frikiAnimationSystem->getParameter( "Default" );

		m_idleAnimationState = ent1->getAnimationState( "Idle" );

		m_testGraph = new SimpleLineGraph( "testGraph", mSceneMgr, 1000 );

		mRoot->addFrameListener( this );
	}

	bool frameStarted( const Ogre::FrameEvent& evt )
	{
		if ( m_mouse )
		{
			m_mouse->capture();
		}

		float elapsedSeconds = evt.timeSinceLastFrame;

		m_frikiAnimationSystem->update( elapsedSeconds );

		m_testGraph->addValue( elapsedSeconds, m_idleAnimationState->getWeight() );

		return true;
	}

	bool mouseMoved( const OIS::MouseEvent& e )
	{
		if ( e.state.Z.rel != 0 )
		{
			float currentState = m_frikiState->getFloatValue();
			int direction = ( 0 < e.state.Z.rel ) ? 1 : -1;
			currentState += direction;

			std::cout << "state " << currentState << std::endl;
			m_frikiState->setValue( currentState );

		}
		return true;
	}

	bool mousePressed( const OIS::MouseEvent& e, OIS::MouseButtonID button )
	{
		return true;
	}

	bool mouseReleased( const OIS::MouseEvent& e, OIS::MouseButtonID button )
	{
		return true;
	}


	OIS::Mouse* m_mouse;

	AnimationSystem* m_frikiAnimationSystem;
	IParameter* m_frikiState;
	ITriggerSubscriber* m_attackListener;
	ITriggerSubscriber* m_listener;

	SimpleLineGraph* m_testGraph;

	Ogre::AnimationState* m_idleAnimationState;
};

int main( const int argc, const char** argv )
{
	TestApplication app;

	try
	{
		INode* node = NodeFactory::getSingleton().createNode( "OutputNode" );
		delete node;

		app.go();
	}
	catch ( Exception& e )
	{
		std::cerr << e.getFullDescription() << std::endl;
		return EXIT_FAILURE;
	}
	catch ( std::exception& e )
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch ( ... )
	{
		std::cerr << "WTF??" << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}