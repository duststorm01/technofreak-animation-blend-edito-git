using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using TecnoFreakCommon;
using System.Windows.Forms;

namespace TecnoFreakViewport
{
	public class TecnoFreakViewport
		: IPluginViewport
	{
		public const int MOUSE_LEFT = 0;
		public const int MOUSE_MIDDLE = 1;
		public const int MOUSE_RIGHT = 2;

		private IPlatformImplementation m_platformImplementation = null;
		private Animable m_animable = null;
		private Panel m_viewportPanel = null;
		private Dictionary< MouseButtons, int > m_mouseButtonMappings = null;

		public TecnoFreakViewport()
		{
			//m_platformImplementation = new PlatformImplementation();
			m_platformImplementation = new Ogre.PlatformImplementation();

			m_mouseButtonMappings = new Dictionary< MouseButtons, int >();
			m_mouseButtonMappings[ MouseButtons.Left ] = MOUSE_LEFT;
			m_mouseButtonMappings[ MouseButtons.Middle ] = MOUSE_MIDDLE;
			m_mouseButtonMappings[ MouseButtons.Right ] = MOUSE_RIGHT;
		}

		~TecnoFreakViewport()
		{
			try
			{
				m_platformImplementation.Shutdown();
			}
			catch ( System.Exception e )
			{
				MessageBox.Show( e.Message );
			}
		}

		// IPluginViewport
		public void Init( Panel viewportPanel )
		{
			m_platformImplementation.Init( viewportPanel.Handle.ToString() );
			BackgroundColor = Color.Gray;
			ShowSkeleton = false;

			m_viewportPanel = viewportPanel;
			m_viewportPanel.MouseMove += new MouseEventHandler( OnMouseMove );
			m_viewportPanel.MouseDown += new MouseEventHandler( OnMouseDown );
			m_viewportPanel.MouseUp += new MouseEventHandler( OnMouseUp );
			m_viewportPanel.MouseClick += new MouseEventHandler( OnMouseClick );
			if ( m_viewportPanel.Parent != null )
			{
				m_viewportPanel.Parent.MouseWheel += new MouseEventHandler( OnMouseWheel );
			}

			m_animable = new Animable( m_platformImplementation );
		}

		public void Redraw()
		{
			m_platformImplementation.Redraw();

			bool changed = m_platformImplementation.CheckBoneSelectionChangedSinceLastCall();
			if ( changed && BoneSelectionChanged != null )
			{
				BoneSelectionChanged( this, new EventArgs() );
			}
		}
		// ~IPluginViewport

		
		// IViewport
		public event ViewportEventHandler BackgroundColorChanged;
		public event ViewportEventHandler ViewportModeChanged;
		public event ViewportEventHandler SkeletonVisibilityChanged;
		public event ViewportEventHandler BoneSelectionChanged;

		private Color m_backgroundColor = Color.Gray;
		public Color BackgroundColor
		{
			get { return m_backgroundColor; }
			set
			{
				bool valueChanged = ! m_backgroundColor.Equals( value );

				m_backgroundColor = value;
				const float TO_FLOAT_COLOR_VALUE = 1.0f / 255.0f;
				float r = value.R * TO_FLOAT_COLOR_VALUE;
				float g = value.G * TO_FLOAT_COLOR_VALUE;
				float b = value.B * TO_FLOAT_COLOR_VALUE;
				m_platformImplementation.SetBackgroundColor( r, g, b );

				if ( valueChanged && BackgroundColorChanged != null )
				{
					BackgroundColorChanged( this, new EventArgs() );
				}
			}
		}

		public String ViewportMode
		{
			get { return ""; } 
			set {}
		}

		public List<String> AvailableViewportModes
		{
			get { return new List< String >(); }
		}

		private bool m_showSkeleton = false;
		public bool ShowSkeleton
		{
			get { return m_showSkeleton; }
			set
			{
				bool changedValue = ( m_showSkeleton != value );

				m_showSkeleton = value;
				m_platformImplementation.ShowSkeleton( m_showSkeleton );

				if ( changedValue && SkeletonVisibilityChanged != null )
				{
					SkeletonVisibilityChanged( this, new EventArgs() );
				}
			}
		}

		public void SetMouseBoneSelectionMode( bool enableBoneSelectionMode )
		{
			m_platformImplementation.SetMouseBoneSelectionMode( enableBoneSelectionMode );
			if ( enableBoneSelectionMode )
			{
				m_platformImplementation.ShowSkeleton( true );
			}
			else
			{
				m_platformImplementation.ShowSkeleton( m_showSkeleton );
			}
		}

		public void ResetBoneDisplayWeights()
		{
			m_platformImplementation.ResetBoneDisplayWeights();
		}

		public int GetBoneCount()
		{
			return m_platformImplementation.GetBoneCount();
		}

		public void SetBoneSelectedByIndex( int boneId, bool select )
		{
			m_platformImplementation.SetBoneSelectedByIndex( boneId, select );
		}

		public void ClearSelectedBones()
		{
			m_platformImplementation.ClearSelectedBones();
		}

		public bool IsBoneSelectedByIndex( int boneId )
		{
			return m_platformImplementation.IsBoneSelectedByIndex( boneId );
		}

		public float GetBoneDisplayWeightByIndex( int boneId )
		{
			return m_platformImplementation.GetBoneDisplayWeightByIndex( boneId );
		}

		public void SetBoneDisplayWeightByIndex( int boneId, float weight )
		{
			m_platformImplementation.SetBoneDisplayWeightByIndex( boneId, weight );
		}

		public String GetBoneNameByIndex( int boneId )
		{
			return m_platformImplementation.GetBoneNameByIndex( boneId );
		}
		// ~IViewport

		// IAnimableLoader
		public event EventHandler AnimableChanged;

		public void LoadAnimable( String filename )
		{
			bool animableLoadSuccess = m_platformImplementation.LoadAnimable( filename );
			if ( animableLoadSuccess )
			{
				m_animable.ReloadAnimationData();
				if ( AnimableChanged != null )
				{
					AnimableChanged( this, new EventArgs() );
				}
			}
		}

		public void ClearAnimable()
		{
			bool animableClearSuccess = m_platformImplementation.ClearAnimable();
			if ( animableClearSuccess )
			{
				m_animable.ReloadAnimationData();
				if ( AnimableChanged != null )
				{
					AnimableChanged( this, new EventArgs() );
				}
			}
		}

		public IAnimable GetAnimable()
		{
			return m_animable;
		}

		public String ShowOpenFileDialog()
		{
			OpenFileDialog openDialog = new OpenFileDialog();
			openDialog.Title = m_platformImplementation.GetFileOpenTitle();
			openDialog.CheckFileExists = true;
			openDialog.CheckPathExists = true;
			openDialog.RestoreDirectory = true;
			openDialog.Filter = m_platformImplementation.GetFileOpenFilter();
			openDialog.Multiselect = false;

			DialogResult result = openDialog.ShowDialog();

			bool fileChosen = ( result == DialogResult.OK );

			if ( fileChosen )
			{
				return openDialog.FileName;
			}
			else
			{
				return null;
			}
		}
		// ~IAnimableLoader

		// Event handling

		protected void OnMouseClick( object sender, MouseEventArgs e )
		{
			int mouseButton = m_mouseButtonMappings[ e.Button ];
			int x = e.X;
			int y = e.Y;
			m_platformImplementation.OnMouseClick( mouseButton, x, y );
		}

		protected void OnMouseMove( object sender, MouseEventArgs e )
		{
			int x = e.X;
			int y = e.Y;
			m_platformImplementation.OnMouseMove( x, y );
		}

		protected void OnMouseWheel( object sender, MouseEventArgs e )
		{
			int delta = e.Delta;
			m_platformImplementation.OnMouseWheel( delta );
		}

		protected void OnMouseDown( object sender, MouseEventArgs e )
		{
			int mouseButton = m_mouseButtonMappings[ e.Button ];
			int x = e.X;
			int y = e.Y;
			m_platformImplementation.OnMouseDown( mouseButton, x, y );
		}

		protected void OnMouseUp( object sender, MouseEventArgs e )
		{
			int mouseButton = m_mouseButtonMappings[ e.Button ];
			int x = e.X;
			int y = e.Y;
			m_platformImplementation.OnMouseUp( mouseButton, x, y );
		}
		// ~Event handling
	}
}
