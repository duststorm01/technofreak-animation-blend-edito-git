using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace TecnoFreakViewport.Ogre
{
	public class PlatformImplementation
		: IPlatformImplementation
	{
		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void InitImpl( String windowHandle );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void ShutdownImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void RedrawImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern IntPtr GetFileOpenTitleImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern IntPtr GetFileOpenFilterImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern bool LoadAnimableImpl( String path, String filename, String name );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern bool ClearAnimableImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern Int32 GetAnimationCountImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern IntPtr GetAnimationNameImpl( Int32 animationIndex );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void OnMouseClickImpl( Int32 mouseButton, Int32 x, Int32 y );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void OnMouseMoveImpl( Int32 x, Int32 y );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void OnMouseWheelImpl( Int32 delta );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void OnMouseDownImpl( Int32 mouseButton, Int32 x, Int32 y );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void OnMouseUpImpl( Int32 mouseButton, Int32 x, Int32 y );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void LoadAnimationTreeImpl( String filename );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void SetParameterValueImpl( String parameterName, float parameterValue );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void ShowSkeletonImpl( bool showSkeleton );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void SetBackgroundColorImpl( float r, float g, float b );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void SetMouseBoneSelectionModeImpl( bool enableBoneSelectionMode );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void ResetBoneDisplayWeightsImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern int GetBoneCountImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void SetBoneSelectedByIndexImpl( int boneId, bool select );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void ClearSelectedBonesImpl();

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern bool IsBoneSelectedByIndexImpl( int boneId );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern float GetBoneDisplayWeightByIndexImpl( int boneId );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern void SetBoneDisplayWeightByIndexImpl( int boneId, float weight );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern IntPtr GetBoneNameByIndexImpl( int boneId );

		[DllImport( "TecnoFreakViewportOgre.dll", CharSet = CharSet.Ansi )]
		static extern bool CheckBoneSelectionChangedSinceLastCallImpl();

		public PlatformImplementation()
		{
		}

		#region IPlatformImplementation Members

		public void Init( String windowHandle )
		{
			InitImpl( windowHandle );
		}

		public void Shutdown()
		{
			ShutdownImpl();
		}

		public void Redraw()
		{
			RedrawImpl();
		}

		public string GetFileOpenTitle()
		{
			String fileOpenTitleName = Marshal.PtrToStringAnsi( GetFileOpenTitleImpl() );
			return fileOpenTitleName;
		}

		public string GetFileOpenFilter()
		{
			String fileOpenFilter = Marshal.PtrToStringAnsi( GetFileOpenFilterImpl() );
			return fileOpenFilter;
		}

		public bool LoadAnimable( string filename )
		{
			String path = Path.GetDirectoryName( filename );
			String file = Path.GetFileName( filename );
			String name = Path.GetFileNameWithoutExtension( file );

			bool loadSuccess = LoadAnimableImpl( path, file, name );
			return loadSuccess;
		}

		public bool ClearAnimable()
		{
			bool clearSuccess = ClearAnimableImpl();
			return clearSuccess;
		}

		public int GetAnimationCount()
		{
			int animationCount = GetAnimationCountImpl();
			return animationCount;
		}

		public string GetAnimationName( int animationIndex )
		{
			String animationName = Marshal.PtrToStringAnsi( GetAnimationNameImpl( animationIndex ) );
			return animationName;
		}

		public void OnMouseClick( int mouseButton, int x, int y )
		{
			OnMouseClickImpl( mouseButton, x, y );
		}

		public void OnMouseMove( int x, int y )
		{
			OnMouseMoveImpl( x, y );
		}

		public void OnMouseWheel( int delta )
		{
			OnMouseWheelImpl( delta );
		}

		public void OnMouseDown( int mouseButton, int x, int y )
		{
			OnMouseDownImpl( mouseButton, x, y );
		}

		public void OnMouseUp( int mouseButton, int x, int y )
		{
			OnMouseUpImpl( mouseButton, x, y );
		}

		public void LoadAnimationTree( String filename )
		{
			LoadAnimationTreeImpl( filename );
		}

		public void SetParameterValue( String parameterName, float parameterValue )
		{
			SetParameterValueImpl( parameterName, parameterValue );
		}

		public void ShowSkeleton( bool showSkeleton )
		{
			ShowSkeletonImpl( showSkeleton );
		}

		public void SetBackgroundColor( float r, float g, float b )
		{
			SetBackgroundColorImpl( r, g, b );
		}

		public void SetMouseBoneSelectionMode( bool enableBoneSelectionMode )
		{
			SetMouseBoneSelectionModeImpl( enableBoneSelectionMode );
		}

		public void ResetBoneDisplayWeights()
		{
			ResetBoneDisplayWeightsImpl();
		}

		public int GetBoneCount()
		{
			return GetBoneCountImpl();
		}

		public void SetBoneSelectedByIndex( int boneId, bool select )
		{
			SetBoneSelectedByIndexImpl( boneId, select );
		}

		public void ClearSelectedBones()
		{
			ClearSelectedBonesImpl();
		}

		public bool IsBoneSelectedByIndex( int boneId )
		{
			return IsBoneSelectedByIndexImpl( boneId );
		}

		public float GetBoneDisplayWeightByIndex( int boneId )
		{
			return GetBoneDisplayWeightByIndexImpl( boneId );
		}

		public void SetBoneDisplayWeightByIndex( int boneId, float weight )
		{
			SetBoneDisplayWeightByIndexImpl( boneId, weight );
		}

		public String GetBoneNameByIndex( int boneId )
		{
			String boneName = Marshal.PtrToStringAnsi( GetBoneNameByIndexImpl( boneId ) );
			return boneName;
		}

		public bool CheckBoneSelectionChangedSinceLastCall()
		{
			return CheckBoneSelectionChangedSinceLastCallImpl();
		}

		#endregion
	}
}
