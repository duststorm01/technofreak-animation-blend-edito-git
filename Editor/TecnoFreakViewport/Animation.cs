using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;

namespace TecnoFreakViewport
{
	public class Animation
		: IAnimation
	{
		private IPlatformImplementation m_platformImplementation = null;
		private int m_aniamtionIndex = -1;

		public Animation( IPlatformImplementation platformImplementation, int animationIndex )
		{
			m_platformImplementation = platformImplementation;
			m_aniamtionIndex = animationIndex;
		}

		#region IAnimation Members

		public event AnimationEventHandler EnabledChanged;

		public event AnimationEventHandler WeightChanged;

		public event AnimationEventHandler LoopChanged;

		public event AnimationEventHandler SpeedChanged;

		public void Update( float elapsedSeconds )
		{
			
		}

		public string Name
		{
			get { return m_platformImplementation.GetAnimationName( m_aniamtionIndex ); }
		}

		public bool Enabled
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public float Weight
		{
			get
			{
				return 0;
			}
			set
			{
				
			}
		}

		public bool Loop
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public float Speed
		{
			get
			{
				return 1;
			}
			set
			{
			}
		}

		public float Length
		{
			get { return 1; }
		}

		public float TimePosition
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		#endregion
	}
}
