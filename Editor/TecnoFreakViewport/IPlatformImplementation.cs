using System;
using System.Collections.Generic;
using System.Text;

namespace TecnoFreakViewport
{
	public interface IPlatformImplementation
	{
		void Init( String handle );
		void Shutdown();
		void Redraw();

		String GetFileOpenTitle();
		String GetFileOpenFilter();

		bool LoadAnimable( String filename );
		bool ClearAnimable();

		int GetAnimationCount();
		String GetAnimationName( int animationIndex );

		void OnMouseClick( int mouseButton, int x, int y );
		void OnMouseMove( int x, int y );
		void OnMouseWheel( int delta );
		void OnMouseDown( int mouseButton, int x, int y );
		void OnMouseUp( int mouseButton, int x, int y );

		void LoadAnimationTree( String filename );
		void SetParameterValue( String parameterName, float parameterValue );

		void ShowSkeleton( bool showSkeleton );
		void SetBackgroundColor( float r, float g, float b );

		void SetMouseBoneSelectionMode( bool enableBoneSelectionMode );
		void ResetBoneDisplayWeights();
		int GetBoneCount();
		void SetBoneSelectedByIndex( int boneId, bool select );
		void ClearSelectedBones();
		bool IsBoneSelectedByIndex( int boneId );
		float GetBoneDisplayWeightByIndex( int boneId );
		void SetBoneDisplayWeightByIndex( int boneId, float weight );
		String GetBoneNameByIndex( int boneId );
		bool CheckBoneSelectionChangedSinceLastCall();
	}
}
