using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;

namespace TecnoFreakViewport
{
	public class Animable
		: IAnimable
	{
		private IPlatformImplementation m_platformImplementation = null;
		private Dictionary< String, IAnimation > m_animations = null;

		public Animable( IPlatformImplementation platformImplementation )
		{
			m_platformImplementation = platformImplementation;
			m_animations = new Dictionary< String, IAnimation >();
		}

		public void ReloadAnimationData()
		{
			m_animations.Clear();
			for ( int i = 0; i < m_platformImplementation.GetAnimationCount(); i++ )
			{
				Animation animation = new Animation( m_platformImplementation, i );
				String animationName = animation.Name;
				m_animations.Add( animationName, animation );
			}

			if ( AnimationsChanged != null )
			{
				AnimationsChanged( this, new EventArgs() );
			}
		}

		#region IAnimable Members

		public event AnimableEventHandler SpeedChanged;

		public event AnimableEventHandler EnabledChanged;

		public event AnimableEventHandler AnimationTreeControlChanged;

		public event AnimableEventHandler Paused;

		public event AnimableEventHandler Started;

		public event AnimableEventHandler Stopped;

		public event AnimableEventHandler AnimationsChanged;

		public float Speed
		{
			get
			{
				return 1;
			}
			set
			{
			}
		}

		public bool Enabled
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		public bool AnimationTreeControl
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		public Dictionary< String, IAnimation > Animations
		{
			get { return m_animations; }
		}

		public IAnimation GetAnimation( String animationName )
		{
			return m_animations[ animationName ];
		}

		public void Pause()
		{	
		}

		public void Start()
		{	
		}

		public void Stop()
		{
		}

		public void LoadAnimationTree( String filename )
		{
			m_platformImplementation.LoadAnimationTree( filename );
		}

		public void SetParameterValue( String parameterName, float parameterValue )
		{
			m_platformImplementation.SetParameterValue( parameterName, parameterValue );
		}

		#endregion
	}
}
