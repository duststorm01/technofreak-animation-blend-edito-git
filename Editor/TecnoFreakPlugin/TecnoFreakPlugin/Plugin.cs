/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreak.v2.Controller.AnimationEngine;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using TecnoFreak.v2.Controller.AnimationEngine.Builder;
using TecnoFreakCommon;
using TecnoFreak.v2.Controller.Interfaces;

namespace TecnoFreak.v2.TecnoFreakPlugin
{
	public class PluginNode : BaseNode
	{
		public class PortTrackBar : TFControls.PortControl.PortTrackBar {}
		public class PortNumericUpDown : TFControls.PortControl.PortNumericUpDown {}

		public PluginNode( String name )
			: base( name )
		{

		}
	}

	public class PluginNodeBuilder : INodeBuilder
	{
		public virtual PluginNode OnCreateNode()
		{
			return null;
		}

		public BaseNode CreateNode()
		{
			return OnCreateNode();
		}
	}

	public class Plugin : IPlugin
	{
		NodeFactory m_nodeFactory;
		IAnimable m_animable;

		public IAnimable Animable
		{
			get { return m_animable; }
		}

		public void Init( NodeFactory nodeFactory, IAnimable animable )
		{
			m_nodeFactory = nodeFactory;
			m_animable = animable;

			OnInit();
		}

		public void AddBuilder( String name, PluginNodeBuilder builder )
		{
			m_nodeFactory.AddBuilder( name, builder );
		}

		public virtual void OnInit()
		{
		}
	}
}
