using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Windows.Forms;
using System.Drawing;
using TFControls.PortControl;
using System.Xml;
using TecnoFreak.v2.Model.Interfaces;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{

	public enum PortDataType
	{
		Animation,
		Number
	}

	public class BaseNode : Node
	{
		public BaseNode( String name )
			: base( name )
		{
			Weight = 1;
		}

		//evaluate the weight of the child nodes.
		public virtual void EvaluateChildWeights(List<AnimationNode> reachedNodes , float elapsedSeconds)
		{
			return;
		}

		// return the value that should go through the parameter port.
		public virtual object EvaluateValue( Port port )
		{
			float dummyValue = 0;
			return dummyValue;
		}

		[NonSerialized]
		private ToolStripMenuItem m_customOperations;

		public ToolStripMenuItem CustomOperations
		{
			get { return m_customOperations; }
			protected set { m_customOperations = value; }
		}


		private float m_weight;

		public float Weight
		{
			get { return m_weight; }
			set { m_weight = value; }
		}

		private IAnimable m_animable;

		public IAnimable Animable
		{
			get { return m_animable; }
			set { m_animable = value; }
		}

		private String m_runtimeName;

		public String RuntimeName
		{
			get { return m_runtimeName; }
			set { m_runtimeName = value; }
		}

		protected BaseNode GetConnectedNode( String portName )
		{
			Port port = GetPort( portName );
			if ( port.Connections.Count == 0 )
			{
				return null;
			}

			return ( BaseNode ) port.Connections[ 0 ].Parent;
		}

		protected Port GetConnectedPort( String portName )
		{
			Port port = GetPort( portName );
			if ( port.Connections.Count == 0 )
			{
				return null;
			}

			return port.Connections[ 0 ];
		}


		protected NiceNodeRenderer NiceNodeRenderer
		{
			get { return ( NiceNodeRenderer ) NodeRenderer; }
		}

		protected void initUpDown( NumericUpDown control )
		{
			control.Maximum = decimal.MaxValue;
			control.Minimum = decimal.MinValue;
			control.DecimalPlaces = 1;
			control.Increment = 0.1M;
			control.Size = new Size( 45, 20 );
		}

		protected PortNumericUpDown NewDefaultUpDown()
		{
			PortNumericUpDown u = new PortNumericUpDown();
			initUpDown( u.NumericUpDownControl );

			return u;
		}

		protected PortNumericUpDown NewDefaultUpDown( uint decimalPlaces )
		{
			PortNumericUpDown u = new PortNumericUpDown();
			initUpDown( u.NumericUpDownControl );
			u.SetDecimalPlaces( decimalPlaces );

			return u;
		}

		protected PortNumericUpDown NewDefaultUpDown( uint decimalPlaces, float value )
		{
			PortNumericUpDown u = NewDefaultUpDown( decimalPlaces );
			u.NumericUpDownControl.Value = ( decimal ) value;

			return u;
		}

		protected Port CreateNumericInPort( String name, IPortControl valueControl )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.In, PortDataType.Number );
			p.ValueControl = valueControl;
			return p;
		}

		protected Port CreateNumericOutPort( String name )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.Out, PortDataType.Number );
			return p;
		}

		protected Port CreateAnimationInPort( String name )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.In, PortDataType.Animation );
			return p;
		}

		protected Port CreateAnimationOutPort( String name )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.Out, PortDataType.Animation );
			return p;
		}

		// when possible use this function instead of calling EvaluateChildWeights for the connected nodes.
		protected void SetWeightAndEvaluateAnimation( String inAnimationPortName, float weightToApply, List<AnimationNode> reachedNodes, float elapsedSeconds )
		{
			BaseNode node = GetConnectedNode( inAnimationPortName );
			if ( node == null )
			{
				return;
			}

			node.Weight = weightToApply;
			node.EvaluateChildWeights( reachedNodes, elapsedSeconds );

		}

		protected float GetValue( String numericPortName )
		{
			Port port = GetPort( numericPortName );

			if ( port.Type == PortType.In )
			{
				BaseNode node = GetConnectedNode( numericPortName );
				if ( node != null )
				{
					Port connectedPort = GetConnectedPort( numericPortName );
					return ( float ) node.EvaluateValue( connectedPort );
				}
			}

			
			if ( port.ValueControl != null )
			{
				return port.ValueControl.Value;
			}

			return 0;
		}


		// write custom data to xml node structure.
		// Anything that will be needed to reconstruct a node from the state that the
		// default constructor leaves it in should go here.
		public virtual void WriteCustomXmlData( XmlWriter writer )
		{
		}

		// read custom data from xml node structure
		// Suppose node is built from it's default constructor and port values haven't
		// been initialised.
		public virtual void ReadCustomXmlData( XmlReader reader )
		{
		}

		// Write data to the custom node xml element for the runtime to load.
		// The custom element is already created and can be directly written to.
		public virtual void WriteRuntimeXmlData( XmlWriter writer )
		{
		}
	}
}

