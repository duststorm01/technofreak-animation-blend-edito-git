using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;

namespace TecnoFreak.v2.Controller.AnimationEngine.Builder
{
	public class NodeBuilder
	{
		public NodeBuilder()
		{
		}

		public virtual BaseNode CreateNode()
		{
			return null;
		}
	}
}
