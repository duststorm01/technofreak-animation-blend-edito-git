using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreak.v2.Model.Interfaces;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode;
using System.Windows.Forms;
using TecnoFreak.v2.View;
using System.Drawing;
using TecnoFreak.v2.Controller.AnimationEngine.Builder;

namespace TecnoFreak.v2.Controller.AnimationEngine
{
	public class NodeCreatedEventArgs : EventArgs
	{
		BaseNode m_node;
		public BaseNode Node
		{
			get { return m_node; }
		}

		public NodeCreatedEventArgs( BaseNode node )
		{
			m_node = node;
		}
	}

	public class NodeFactory
	{

		public event EventHandler< NodeCreatedEventArgs > NodeCreated;

		public NodeFactory()
		{
			Init( null, new ToolStripMenuItem() );
		}

		public void Init( IAnimable animable, ToolStripMenuItem menu )
		{
			m_menu = menu;
			m_menu.DropDownItems.Clear();

			if ( animable != null )
			{
				foreach ( IAnimation animation in animable.Animations.Values )
				{
					AddBuilder( "Animation:Current:" + animation.Name, new AnimationNodeBuilder( animation ) );
				}
			}
			AddBuilder( "Animation:Custom", new CustomAnimationNodeBuilder( animable ) );

			AddBuilder( "Blender:Blend", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.BlendNode" ) );
			AddBuilder( "Blender:Mix", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MixNode" ) );
			AddBuilder( "Blender:Average", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.AverageNode" ) );
			AddBuilder( "Blender:LookAt", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.LookAtNode" ) );

			AddBuilder( "Math:Abs", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.AbsNode" ) );
			AddBuilder( "Math:Ceil", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.CeilNode" ) );
			AddBuilder( "Math:Clamp", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.ClampNode" ) );
			AddBuilder( "Math:Div", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.DivNode" ) );
			AddBuilder( "Math:Map", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MapNode" ) );
			AddBuilder( "Math:Max", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MaxNode" ) );
			AddBuilder( "Math:Min", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MinNode" ) );
			AddBuilder( "Math:Minus", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MinusNode" ) );
			AddBuilder( "Math:Mul", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MulNode" ) );
			AddBuilder( "Math:Plus", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.PlusNode" ) );

			AddBuilder( "Transition:Select", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.TransitionSelectNode" ) );
			AddBuilder( "Transition:Random", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.RandomSelectNode" ) );

			AddBuilder( "Parameter", new ParameterNodeBuilder() );

			AddBuilder( "Constant:E", new ConstantNodeBuilder( "E", ( float )( Math.E ) ) );
			AddBuilder( "Constant:PI", new ConstantNodeBuilder( "PI", ( float )( Math.PI ) ) );
			AddBuilder( "Constant:Custom", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.ConstantNode" ) );

			AddBuilder( "SmoothValue", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.SmoothValueNode" ) );
		}

		public BaseNode CreateNode( String type )
		{
			if ( ! m_nodeBuilders.ContainsKey( type ) )
			{
				return null;
			}

			NodeBuilder builder = m_nodeBuilders[ type ];

			if ( builder == null )
			{
				return null;
			}

			BaseNode node = builder.CreateNode();
			if ( node != null )
			{
				if ( NodeCreated != null )
				{
					NodeCreated( this, new NodeCreatedEventArgs( node ) );
				}
			}
			return node;
		}

		ToolStripMenuItem FindItem( ToolStripItemCollection items, String name )
		{
			for ( int i = 0; i < items.Count; i++ )
			{
				ToolStripMenuItem item = ( ToolStripMenuItem ) items[ i ];
				if ( item.Text == name )
				{
					return item;
				}
			}

			return null;
		}

		public void AddBuilder( String type, NodeBuilder builder )
		{
			string[] categories = type.Split( ':' );
			ToolStripItemCollection items = m_menu.DropDownItems;
			for ( int i = 0; i < categories.Length - 1; i++ )
			{
				String category = categories[ i ];

				ToolStripMenuItem itemCategory = FindItem( items, category );
				if ( itemCategory == null )
				{
					itemCategory = new ToolStripMenuItem();
					itemCategory.Text = category;
					items.Add( itemCategory );
				}

				items = itemCategory.DropDownItems;
			}

			String lastItemName = categories[ categories.Length - 1 ];

			ToolStripMenuItem lastItem = FindItem( items, lastItemName );
			if ( lastItem == null )
			{
				lastItem = new ToolStripMenuItem();
				lastItem.Text = lastItemName;
				items.Add( lastItem );
			}

			lastItem.Name = type;
			lastItem.Click += new EventHandler( contextMenuItemClick );
			
			m_nodeBuilders[ type ] = builder;
		}

		void contextMenuItemClick( object sender, EventArgs e )
		{
			ToolStripMenuItem item = ( ToolStripMenuItem )( sender );
			String name = item.Name;
			CreateNode( name );
		}

		Dictionary< String, NodeBuilder > m_nodeBuilders = new Dictionary< String, NodeBuilder >();

		public ToolStripMenuItem CreateNodeMenu
		{
			get { return m_menu; }
		}
		ToolStripMenuItem m_menu = new ToolStripMenuItem();
	}
}
