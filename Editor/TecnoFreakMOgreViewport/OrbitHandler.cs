/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Mogre;

namespace TecnoFreakMOgreViewport
{
	class OrbitHandler
	{
		public OrbitHandler(Panel subject, Control wheelSubject, Mogre.Camera camera, Mogre.SceneManager sceneManager)
		{
			Subject = subject;
			WheelSubject = wheelSubject;

			CameraNodes = new CameraNodes( CameraNodes.RotationOrder.YawPitchRoll, sceneManager.RootSceneNode, camera );

			DegreesPerScreenUnit = 1;
			ZoomStepsPerScreenUnit = 4;
			ZoomStepDistance = 0.2f;

			DistanceToTarget = 30;
			Target = new Vector3(0, 0, 0);

			Subject.MouseMove += new MouseEventHandler(Subject_MouseMove);
			Subject.MouseDown += new MouseEventHandler(Subject_MouseDown);
			Subject.MouseUp += new MouseEventHandler(Subject_MouseUp);
			Subject.MouseClick += new MouseEventHandler(Subject_MouseClick);

			WheelSubject.MouseWheel += new MouseEventHandler(Subject_MouseWheel);

			Root.Singleton.FrameStarted += new FrameListener.FrameStartedHandler(FrameStarted);

			Enabled = true;
		}



		#region Event Handlers

		protected bool FrameStarted(FrameEvent e)
		{
			this.CameraNodes.Update();
			return true;
		}

		void Model_ModelChanged(object sender, EventArgs e)
		{
			DistanceToTarget = 3 * Model.BoundingRadius;
			ZoomStepDistance = 0.0025f * Model.BoundingRadius;

			Target = new Vector3(0, Model.Center.y, 0);
		}

		void Subject_MouseClick(object sender, MouseEventArgs e)
		{
			if (!Enabled)
			{
				return;
			}

			Subject.Focus();
		}

		protected void Subject_MouseMove(object sender, MouseEventArgs e)
		{
			if (!Enabled)
			{
				return;
			}

			MousePosition = new Vector2(e.X, e.Y);

			if (OrbitButtonDown)
			{
				this.CameraNodes.NextUpdateYawDegrees += -MouseIncrement.x * DegreesPerScreenUnit;
				this.CameraNodes.NextUpdatePitchDegrees += -MouseIncrement.y * DegreesPerScreenUnit;
			}

			if (ZoomButtonDown)
			{
				DistanceToTarget += MouseIncrement.y * ZoomStepsPerScreenUnit * ZoomStepDistance;
			}
		}

		protected void Subject_MouseWheel(object sender, MouseEventArgs e)
		{
			if (!Enabled)
			{
				return;
			}

			DistanceToTarget += -e.Delta * ZoomStepDistance;
		}

		protected void Subject_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == OrbitButton)
			{
				MousePosition = new Vector2(e.X, e.Y);
				OrbitButtonDown = true;
			}

			if (e.Button == ZoomButton)
			{
				ZoomButtonDown = true;
			}
		}

		protected void Subject_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == OrbitButton)
			{
				OrbitButtonDown = false;
			}

			if (e.Button == ZoomButton)
			{
				ZoomButtonDown = false;
			}
		}


		#endregion



		#region Attributes
		private CameraNodes m_cameraNodes;
		private IModel m_model;

		private Panel m_subject;
		private Control m_wheelSubject;

		private Vector2 m_mousePosition;
		private Vector2 m_oldMousePosition;
		private Vector2 m_mouseIncrement;

		private bool m_zoomButtonDown;
		private bool m_orbitButtonDown;

		private float m_degreesPerScreenUnit;
		private float m_zoomStepsPerScreenUnit;
		private float m_zoomStepDistance;

		private bool m_enabled;
		#endregion



		#region Properties
		public CameraNodes CameraNodes
		{
			get { return m_cameraNodes; }
			protected set { m_cameraNodes = value; }
		}

		public IModel Model
		{
			get { return m_model; }
			set
			{
				if (Model != value)
				{
				}

				if (Model != null)
				{
					Model.ModelChanged -= Model_ModelChanged;
				}

				m_model = value;
				Model.ModelChanged += new ModelEventHandler(Model_ModelChanged);
			}
		}

		public Panel Subject
		{
			get { return m_subject; }
			protected set { m_subject = value; }
		}

		public Control WheelSubject
		{
			get { return m_wheelSubject; }
			protected set { m_wheelSubject = value; }
		}

		public Vector2 MousePosition
		{
			get { return m_mousePosition; }
			protected set
			{
				OldMousePosition = MousePosition;
				m_mousePosition = value;
				MouseIncrement = MousePosition - OldMousePosition;
			}
		}

		public Vector2 OldMousePosition
		{
			get { return m_oldMousePosition; }
			protected set { m_oldMousePosition = value; }
		}

		public Vector2 MouseIncrement
		{
			get { return m_mouseIncrement; }
			protected set { m_mouseIncrement = value; }
		}

		public bool OrbitButtonDown
		{
			get { return m_orbitButtonDown; }
			set { m_orbitButtonDown = value; }
		}

		public bool ZoomButtonDown
		{
			get { return m_zoomButtonDown; }
			set { m_zoomButtonDown = value; }
		}

		public float DegreesPerScreenUnit
		{
			get { return m_degreesPerScreenUnit; }
			set { m_degreesPerScreenUnit = value; }
		}

		public float ZoomStepsPerScreenUnit
		{
			get { return m_zoomStepsPerScreenUnit; }
			set { m_zoomStepsPerScreenUnit = value; }
		}

		public float ZoomStepDistance
		{
			get { return m_zoomStepDistance; }
			set { m_zoomStepDistance = value; }
		}

		public MouseButtons ZoomButton
		{
			get { return MouseButtons.Middle; }
		}

		public MouseButtons OrbitButton
		{
			get { return MouseButtons.Left; }
		}

		public float DistanceToTarget
		{
			get { return CameraNodes.PostPosition.z; }
			set { CameraNodes.PostPosition = new Vector3(0, 0, System.Math.Max(0, value)); }
		}

		public Vector3 Target
		{
			get { return CameraNodes.PrePosition; }
			set { CameraNodes.PrePosition = value; }
		}

		public bool Enabled
		{
			get { return m_enabled; }
			set { m_enabled = value; }
		}

		#endregion
	}
}
