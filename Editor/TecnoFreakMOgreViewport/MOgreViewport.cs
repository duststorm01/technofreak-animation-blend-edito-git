/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;
using Mogre;
using System.Xml;
using System.Windows.Forms;
using System.Drawing;

namespace TecnoFreakMOgreViewport
{
	public class MOgreViewport : IPluginViewport
	{
		static String ms_configFileName = "viewport.xml";

		Mogre.Root m_root;
		Mogre.RenderWindow m_renderWindow;
		Mogre.Camera m_camera;
		Mogre.Viewport m_viewport;
		Mogre.SceneManager m_sceneManager;

		OrbitHandler m_cameraController;
		Model m_model;

		public MOgreViewport()
		{
		}

		public String ShowOpenFileDialog()
		{
			OpenFileDialog openDialog = new OpenFileDialog();
			openDialog.Title = "Load Ogre Model";
			openDialog.CheckFileExists = true;
			openDialog.CheckPathExists = true;
			openDialog.RestoreDirectory = true;
			openDialog.Filter = "Ogre Mesh File (*.mesh)|*.mesh" + "|" + "All Files (*.*)|*.*";
			openDialog.Multiselect = false;

			DialogResult result = openDialog.ShowDialog();

			bool fileChosen = (result == DialogResult.OK);

			if (fileChosen)
			{
				return openDialog.FileName;
			}
			else
			{
				return null;
			}
		}

		public void Init(Panel viewportPanel)
		{
			m_root = new Mogre.Root();

			CreateResourceGroups();
			InitializeRenderSystem();

			m_root.Initialise(false);

			CreateRenderWindow(viewportPanel.Handle.ToString());

			Mogre.ResourceGroupManager.Singleton.InitialiseAllResourceGroups();

			m_sceneManager = m_root.CreateSceneManager(Mogre.SceneType.ST_GENERIC, "Default" );
			m_camera = m_sceneManager.CreateCamera("Default");
			m_camera.NearClipDistance = 0.1f;
			m_camera.AutoAspectRatio = true;
			m_viewport = m_renderWindow.AddViewport(m_camera);

			CalculateAvailableViewportModes();

			ViewportMode = "Default";
			ShowSkeleton = false;
			UpdateSkeletonVisibility();
			
			m_model = new Model(m_root, m_sceneManager);
			m_model.ModelChanged += new ModelEventHandler(m_model_ModelChanged);
			m_model.LoadModelSuccess += new EventHandler(m_model_ModelChanged);

			m_cameraController = new OrbitHandler(viewportPanel, viewportPanel.Parent, m_camera, m_sceneManager);
			m_cameraController.Model = m_model;

			m_root.FrameStarted += new FrameListener.FrameStartedHandler(m_root_FrameStarted);
		}

		public void Redraw()
		{
			m_root.RenderOneFrame();
		}

		bool m_root_FrameStarted(FrameEvent evt)
		{
			m_renderWindow.WindowMovedOrResized();
			return true;
		}

		private void CreateViewport()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		private void CreateRenderWindow(String hWndString)
		{
			Mogre.NameValuePairList initParams = GetDefaultInitParams();
			initParams["externalWindowHandle"] = hWndString;

			String name = "Ogre RenderWindow" + hWndString;

			m_renderWindow = m_root.CreateRenderWindow(name, 0, 0, false, initParams);
		}

		void CreateResourceGroups()
		{
			//Mogre.ResourceGroupManager.Singleton.AddResourceLocation("TecnoFreakData.zip", "Zip", "TecnoFreak");
			Mogre.ResourceGroupManager.Singleton.AddResourceLocation("TecnoFreak.data", "Zip", "TecnoFreak");
			Mogre.ResourceGroupManager.Singleton.AddResourceLocation("Temp", "FileSystem", "TecnoFreak");
		}

		private void InitializeRenderSystem()
		{
			SetRenderSystem("Direct3D9 Rendering Subsystem");

			m_root.RenderSystem.SetConfigOption("Full Screen", "No");
			m_root.RenderSystem.SetConfigOption("Video Mode", "1024 x 768 @ 32-bit colour");
		}

		void SetRenderSystem(String chosenRenderSystemName)
		{
			foreach (Mogre.RenderSystem renderSystem in m_root.GetAvailableRenderers())
			{
				if (renderSystem.Name == chosenRenderSystemName)
				{
					m_root.RenderSystem = renderSystem;
					return;
				}
			}

			throw new Exception("Unable to locate RenderSystem with name'" + chosenRenderSystemName + "'");
		}

		public Mogre.NameValuePairList GetDefaultInitParams()
		{
			Mogre.NameValuePairList initParams = new Mogre.NameValuePairList();
			ReadInitParamsFromConfigFile(initParams);

			return initParams;
		}

		private void ReadInitParamsFromConfigFile(Mogre.NameValuePairList initParams)
		{
			try
			{
				String completeConfigFileName = Application.StartupPath + "/" + ms_configFileName;
				XmlTextReader reader = new XmlTextReader(completeConfigFileName);

				XmlDocument document = new XmlDocument();
				document.Load(reader);

				if (!document.HasChildNodes)
				{
					return;
				}

				XmlNode viewportNode = document.FirstChild;

				foreach (XmlNode node in viewportNode.ChildNodes)
				{
					if (node.Name.ToLower() == "fsaa")
					{
						initParams["FSAA"] = node.InnerText;
					}
				}

				reader.Close();
			}
			catch (System.Exception)
			{

			}
		}

		#region IViewport Members



		public event ViewportEventHandler BackgroundColorChanged;

		public event ViewportEventHandler ViewportModeChanged;

		public event ViewportEventHandler SkeletonVisibilityChanged;

		public Color BackgroundColor
		{
			get
			{
				return m_backgroundColor;
			}
			set
			{
				if (BackgroundColor.Equals(value))
				{
					return;
				}

				m_backgroundColor = value;
				m_viewport.BackgroundColour = ColorConverter.getColor(BackgroundColor);
				if (BackgroundColorChanged != null)
				{
					BackgroundColorChanged(this, null);
				}
			}
		}

		public String ViewportMode
		{
			get
			{
				return m_viewportMode;
			}
			set
			{
				if (ViewportMode == value)
				{
					return;
				}

				m_viewportMode = value;
				m_viewport.MaterialScheme = ViewportMode;

				if (ViewportModeChanged != null)
				{
					ViewportModeChanged(this, null);
				}
			}
		}

		public bool ShowSkeleton
		{
			get
			{
				return m_showSkeleton;
			}
			set
			{
				if (ShowSkeleton == value)
				{
					return;
				}

				m_showSkeleton = value;

				UpdateSkeletonVisibility();

				if (SkeletonVisibilityChanged != null)
				{
					SkeletonVisibilityChanged(this, null);
				}
			}
		}

		#endregion

		#region IViewport related operations

		private void CalculateAvailableViewportModes()
		{
			AvailableViewportModes = new List<string>();

			Mogre.MaterialPtr viewportMaterial = (Mogre.MaterialPtr)Mogre.MaterialManager.Singleton.GetByName(ViewportMaterialName);

			AvailableViewportModes.Add("Default");

			foreach (Mogre.Technique technique in viewportMaterial.GetTechniqueIterator())
			{
				String currentMode = technique.SchemeName;
				if (!AvailableViewportModes.Contains(currentMode))
				{
					AvailableViewportModes.Add(currentMode);
				}
			}
		}

		public static void MakeViewportCompliant(Mogre.Entity entity)
		{
			Mogre.MaterialPtr viewportMaterial = (Mogre.MaterialPtr)Mogre.MaterialManager.Singleton.GetByName(ViewportMaterialName);


			Dictionary<String, Mogre.MaterialPtr> materials = new Dictionary<String, Mogre.MaterialPtr>();
			for (uint i = 0; i < entity.NumSubEntities; i++)
			{
				String materialName = entity.GetSubEntity(i).MaterialName;
				if (!materials.ContainsKey(materialName))
				{
					Mogre.MaterialPtr subEntityMaterial = (Mogre.MaterialPtr)Mogre.MaterialManager.Singleton.GetByName(materialName);
					if (subEntityMaterial == null)
					{
						entity.GetSubEntity(i).MaterialName = ViewportMaterialName;
					}
					else
					{
						materials[materialName] = subEntityMaterial;
					}
				}
			}

			foreach (Mogre.MaterialPtr material in materials.Values)
			{
				List<String> schemesImplemented = new List<String>();

				foreach (Mogre.Technique technique in material.GetTechniqueIterator())
				{
					schemesImplemented.Add(technique.SchemeName);
				}

				foreach (Mogre.Technique viewportTechnique in viewportMaterial.GetTechniqueIterator())
				{
					String schemeName = viewportTechnique.SchemeName;
					if (!schemesImplemented.Contains(schemeName))
					{
						Mogre.Technique newTechinque = material.CreateTechnique();
						viewportTechnique.CopyTo(newTechinque);
					}
				}
			}
		}

		private void UpdateSkeletonVisibility()
		{
			uint skeletonMask = Model.SkeletonVisibilityFlag;
			uint inverseSkeletonMask = ~skeletonMask;

			uint cleanCurrentMask = m_viewport.VisibilityMask & inverseSkeletonMask;

			if (ShowSkeleton)
			{
				m_viewport.SetVisibilityMask(cleanCurrentMask | skeletonMask);
			}
			else
			{
				m_viewport.SetVisibilityMask(cleanCurrentMask);
			}
		}
		#endregion

		#region Attributes
		private Color m_backgroundColor;

		private List<String> m_availableViewportModes;
		private String m_viewportMode;

		private bool m_showSkeleton;
		#endregion

		#region Properties
		public List<String> AvailableViewportModes
		{
			get { return m_availableViewportModes; }
			protected set { m_availableViewportModes = value; }
		}

		static protected String ViewportMaterialName
		{
			get { return "TecnoFreak/ViewportMaterial"; }
		}
		#endregion









		#region IAnimableLoader

		public event EventHandler AnimableChanged;
		void m_model_ModelChanged( object sender, EventArgs e )
		{
			if ( AnimableChanged != null )
			{
				AnimableChanged( this, e );
			}
		}

		public void LoadAnimable(String filename)
		{
			m_model.LoadModel(filename);
		}

		public void ClearAnimable()
		{
			m_model.DeleteModel();
		}

		public IAnimable GetAnimable()
		{
			return m_model;
		}


		#endregion
	}
}
