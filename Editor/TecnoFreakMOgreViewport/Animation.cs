/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;

namespace TecnoFreakMOgreViewport
{
	public class Animation : IAnimation
	{
		public Animation(Mogre.AnimationState ogreAnimationState)
		{
			OgreAnimationState = ogreAnimationState;
			Speed = 1;
		}

		#region IAnimation Members

		public event AnimationEventHandler EnabledChanged;
		public event AnimationEventHandler WeightChanged;
		public event AnimationEventHandler LoopChanged;
		public event AnimationEventHandler SpeedChanged;

		public void Update(float elapsedSeconds)
		{
			OgreAnimationState.AddTime(elapsedSeconds * Speed);
		}

		public String Name
		{
			get { return OgreAnimationState.AnimationName; }
		}

		public bool Enabled
		{
			get { return OgreAnimationState.Enabled; }
			set
			{
				if (Enabled == value)
				{
					return;
				}

				OgreAnimationState.Enabled = value;
				if (EnabledChanged != null)
				{
					EnabledChanged(this, new EventArgs());
				}
			}
		}

		public float Weight
		{
			get { return OgreAnimationState.Weight; }
			set
			{
				if (Weight == value)
				{
					return;
				}

				OgreAnimationState.Weight = value;
				if (WeightChanged != null)
				{
					WeightChanged(this, new EventArgs());
				}
			}
		}

		public bool Loop
		{
			get { return OgreAnimationState.Loop; }
			set
			{
				if (Loop == value)
				{
					return;
				}

				OgreAnimationState.Loop = value;
				if (LoopChanged != null)
				{
					LoopChanged(this, new EventArgs());
				}
			}
		}

		public float Length
		{
			get { return OgreAnimationState.Length; }
		}

		public float Speed
		{
			get { return m_speed; }
			set
			{
				if (Speed == value)
				{
					return;
				}

				m_speed = value;
				if (SpeedChanged != null)
				{
					SpeedChanged(this, new EventArgs());
				}
			}
		}

		public float TimePosition
		{
			get { return OgreAnimationState.TimePosition; }
			set { OgreAnimationState.TimePosition = value; }
		}

		#endregion



		#region Attributes
		private Mogre.AnimationState m_ogreAnimationState;

		private float m_speed;
		#endregion

		#region Properties
		protected Mogre.AnimationState OgreAnimationState
		{
			get { return m_ogreAnimationState; }
			set { m_ogreAnimationState = value; }
		}

		public bool Valid
		{
			get { return (0 < Length); }
		}
		#endregion
	}
}
