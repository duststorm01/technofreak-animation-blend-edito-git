/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using Mogre;

namespace TecnoFreakMOgreViewport
{
	public class CameraNodes
	{
		public enum RotationOrder
		{
			PitchYawRoll,
			YawPitchRoll
		}

		public CameraNodes( RotationOrder rotationOrderInfo, Mogre.SceneNode parentNode, Mogre.Camera camera )
		{
			m_camera = camera;
			RotationOrderInfo = rotationOrderInfo;
			
			CreateNewCameraNodes( parentNode );
		}



		#region Operations

		private void CreateNewCameraNodes( Mogre.SceneNode parentNode )
		{
			OrderedNodes = new List< Mogre.SceneNode >();
			OrderedNodes.Add( parentNode );
			for ( int i = 0; i < 5; i++ )
			{
				SceneNode childNode = OrderedNodes[ i ].CreateChildSceneNode();
				OrderedNodes.Add( childNode );
			}

			PrePositionNode = OrderedNodes[ 1 ];

			if ( RotationOrderInfo == RotationOrder.PitchYawRoll )
			{
				PitchNode = OrderedNodes[ 2 ];
				YawNode = OrderedNodes[ 3 ];
				RollNode = OrderedNodes[ 4 ];
			}
			else if ( RotationOrderInfo == RotationOrder.YawPitchRoll )
			{
				YawNode = OrderedNodes[ 2 ];
				PitchNode = OrderedNodes[ 3 ];
				RollNode = OrderedNodes[ 4 ];
			}

			PostPositionNode = OrderedNodes[ 5 ];

			LastNode.AttachObject( m_camera );
		}

		public void Update()
		{
			PitchNode.Pitch( new Degree( NextUpdatePitchDegrees ) );
			YawNode.Yaw( new Degree( NextUpdateYawDegrees ) );
			RollNode.Roll( new Degree( NextUpdateRollDegrees ) );

			NextUpdatePitchDegrees = 0;
			NextUpdateYawDegrees = 0;
			NextUpdateRollDegrees = 0;
		}

		#endregion

		#region Attributes
		private Mogre.SceneNode m_prePositionNode;
		private Mogre.SceneNode m_postPositionNode;

		private Mogre.SceneNode m_pitchNode;
		private Mogre.SceneNode m_yawNode;
		private Mogre.SceneNode m_rollNode;

		private float m_nextUpdatePitchDegrees;
		private float m_nextUpdateYawDegrees;
		private float m_nextUpdateRollDegrees;

		private Mogre.Camera m_camera;

		private RotationOrder m_rotationOrderInfo;
		private List< Mogre.SceneNode > m_orderedNodes;
		#endregion


		#region Properties
		public Mogre.SceneNode PrePositionNode
		{
			get { return m_prePositionNode; }
			protected set { m_prePositionNode = value; }
		}
		public Mogre.SceneNode PostPositionNode
		{
			get { return m_postPositionNode; }
			protected set { m_postPositionNode = value; }
		}

		public Mogre.SceneNode PitchNode
		{
			get { return m_pitchNode; }
			protected set { m_pitchNode = value; }
		}
		public Mogre.SceneNode YawNode
		{
			get { return m_yawNode; }
			protected set { m_yawNode = value; }
		}
		public Mogre.SceneNode RollNode
		{
			get { return m_rollNode; }
			protected set { m_rollNode = value; }
		}

		public Mogre.SceneNode LastNode
		{
			get { return OrderedNodes[OrderedNodes.Count - 1]; }
		}

		public Mogre.Camera Camera
		{
			get { return m_camera; }
			protected set { m_camera = value; }
		}

		public RotationOrder RotationOrderInfo
		{
			get { return m_rotationOrderInfo; }
			protected set { m_rotationOrderInfo = value; }
		}

		public List< Mogre.SceneNode > OrderedNodes
		{
			get { return m_orderedNodes; }
			protected set { m_orderedNodes = value; }
		}

		public float NextUpdatePitchDegrees
		{
			get { return m_nextUpdatePitchDegrees; }
			set { m_nextUpdatePitchDegrees = value; }
		}

		public float NextUpdateYawDegrees
		{
			get { return m_nextUpdateYawDegrees; }
			set { m_nextUpdateYawDegrees = value; }
		}

		public float NextUpdateRollDegrees
		{
			get { return m_nextUpdateRollDegrees; }
			set { m_nextUpdateRollDegrees = value; }
		}

		public Mogre.Vector3 PrePosition
		{
			get { return PrePositionNode.Position; }
			set { PrePositionNode.Position = value; }
		}

		public Mogre.Vector3 PostPosition
		{
			get { return PostPositionNode.Position; }
			set { PostPositionNode.Position = value; }
		}

		#endregion
	}
}
