/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Mogre;
using TecnoFreakCommon;
using ManagedAnimationSystem;

namespace TecnoFreakMOgreViewport
{
	public class Model : IAnimable, IModel
	{
		public Model( Root root, SceneManager sceneManager )
		{
			Root = root;
			SceneManager = sceneManager;

			AdditionalMaterialPaths = new StringVector();
			AdditionalMaterialPaths.Add( "../materials" );
			AdditionalMaterialPaths.Add( "../materials/programs" );
			AdditionalMaterialPaths.Add( "../materials/scripts" );
			AdditionalMaterialPaths.Add( "../materials/textures" );

			Speed = 1;
			Enabled = true;
			CreateAnimations();

			SkeletonEntityNames = new List<String>();
			SkeletonTagPoints = new List<TagPoint>();

			Root.FrameStarted += new FrameListener.FrameStartedHandler( UpdateAnimations );
		}

		OgreAnimationSystem m_ogreAnimationSystem = null;

		#region IModelIO Members

		public void LoadModel( String filename )
		{
			if ( ModelLoaded )
			{
				throw new Exception( "Cannot load a new character model if there is one previously loaded." );
			}

			String path = Path.GetDirectoryName( filename );
			String file = Path.GetFileName( filename );
			String name = Path.GetFileNameWithoutExtension( file );

			CreateResourcesFromPath( path );

			OgreEntity = SceneManager.CreateEntity( name, file );
			MOgreViewport.MakeViewportCompliant(OgreEntity);
			OgreEntity.VisibilityFlags = ModelVisibilityFlag;
			
			if ( OgreEntity.Skeleton != null )
			{
				OgreEntity.Skeleton.BlendMode = SkeletonAnimationBlendMode.ANIMBLEND_AVERAGE;
			}
			
			OgreSceneNode = SceneManager.RootSceneNode.CreateChildSceneNode();
			OgreSceneNode.AttachObject( OgreEntity );

            if ( OgreEntity.HasSkeleton )
            {
				OgreEntity.Skeleton.BlendMode = SkeletonAnimationBlendMode.ANIMBLEND_CUMULATIVE;
            }

			m_ogreAnimationSystem = new OgreAnimationSystem( OgreEntity.AllAnimationStates );

			onModelLoaded();

			
		}


		private void CreateResourcesFromPath( String path )
		{
			try
			{
				ResourceGroupManager.Singleton.UnloadResourceGroup( CharacterResourceGroupName );
				ResourceGroupManager.Singleton.DestroyResourceGroup( CharacterResourceGroupName );
			}
			catch ( Exception )
			{
			}

			ResourceGroupManager.Singleton.AddResourceLocation( path, "FileSystem", CharacterResourceGroupName );
			ResourceGroupManager.Singleton.InitialiseResourceGroup( CharacterResourceGroupName );

			if ( LastLoadedCharacterPath != path )
			{
				CreateMaterialResourcesFromPath( path );
				LastLoadedCharacterPath = path;
			}
		}

		private void CreateMaterialResourcesFromPath( String path )
		{
			try
			{
				ResourceGroupManager.Singleton.UnloadResourceGroup( CharacterMaterialResourceGroupName );
				ResourceGroupManager.Singleton.DestroyResourceGroup( CharacterMaterialResourceGroupName );
			}
			catch ( Exception )
			{
			}

			foreach ( String partialPath in AdditionalMaterialPaths )
			{
				String completePath = Path.Combine( path, partialPath );
				ResourceGroupManager.Singleton.AddResourceLocation( completePath, "FileSystem", CharacterMaterialResourceGroupName );
			}

			ResourceGroupManager.Singleton.InitialiseResourceGroup( CharacterMaterialResourceGroupName );
		}

		public void SaveModel( String filename )
		{
			throw new Exception( "The method or operation is not implemented." );
			//onModelSaved();
		}

		public void DeleteModel()
		{
			if ( ModelLoaded )
			{
				DestroySkeleton();
				OgreSceneNode.DetachObject( OgreEntity );
				SceneManager.DestroyEntity( OgreEntity );
				SceneManager.DestroySceneNode( OgreSceneNode.Name );

				OgreEntity = null;
				OgreSceneNode = null;
			}
			
			onModelDeleted();
		}

		public event EventHandler LoadModelSuccess;

		public event EventHandler SaveModelSuccess;

		public event EventHandler DeleteModelSuccess;

		#endregion


		#region IAnimable Members

		public event AnimableEventHandler SpeedChanged;
		public event AnimableEventHandler EnabledChanged;
		public event AnimableEventHandler AnimationTreeControlChanged;

		public event AnimableEventHandler Paused;
		public event AnimableEventHandler Started;
		public event AnimableEventHandler Stopped;

		public event AnimableEventHandler AnimationsChanged;

		public float Speed
		{
			get { return m_speed; }
			set
			{
				if ( Speed == value )
				{
					return;
				}

				m_speed = value;
				if ( SpeedChanged != null )
				{
					SpeedChanged( this, new EventArgs() );
				}
			}
		}

		public bool Enabled
		{
			get { return m_enabled; }
			set
			{
				if ( Enabled == value )
				{
					return;
				}

				m_enabled = value;
				if ( EnabledChanged != null )
				{
					EnabledChanged( this, new EventArgs() );
				}
			}
		}

		private bool m_animationTreeControl = false;
		public bool AnimationTreeControl
		{
			get { return m_animationTreeControl; }
			set
			{
				if ( AnimationTreeControl == value )
				{
					return;
				}

				m_animationTreeControl = value;
				if ( AnimationTreeControlChanged != null )
				{
					AnimationTreeControlChanged( this, new EventArgs() );
				}
			}
		}

		public IAnimation GetAnimation( String animationName )
		{
			return Animations[ animationName ];
		}

		public void Pause()
		{
			if ( !Enabled )
			{
				return;
			}

			Enabled = false;
			if ( Paused != null )
			{
				Paused( this, new EventArgs() );
			}
		}

		public void Start()
		{
			if ( Enabled )
			{
				return;
			}

			Enabled = true;
			if ( Started != null )
			{
				Started( this, new EventArgs() );
			}
		}

		public void Stop()
		{
			if ( !Enabled )
			{
				return;
			}

			Enabled = false;

			foreach ( IAnimation animation in Animations.Values )
			{
				animation.TimePosition = 0;
			}

			if ( Stopped != null )
			{
				Stopped( this, new EventArgs() );
			}
		}

		private void CreateAnimations()
		{
			Animations = new Dictionary<String, IAnimation>();

			if ( OgreEntity != null && OgreEntity.AllAnimationStates != null )
			{
				foreach( Mogre.AnimationState animationState in OgreEntity.AllAnimationStates.GetAnimationStateIterator() )
				{
					Animation animation = new Animation( animationState );
					if ( animation.Valid )
					{
						Animations.Add( animation.Name, animation );
					}
				}
			}

			if ( AnimationsChanged != null )
			{
				AnimationsChanged( this, new EventArgs() );
			}
		}

		private bool UpdateAnimations( FrameEvent evt )
		{
			if ( !Enabled )
			{
				return true;
			}

			float elapsedSeconds = evt.timeSinceLastFrame;

			if ( AnimationTreeControl )
			{
				UpdateAnimationTree( elapsedSeconds * Speed );
			}
			else
			{
				foreach ( IAnimation animation in Animations.Values )
				{
					animation.Update( elapsedSeconds * Speed );
				}
			}

			return true;
		}

		public void UpdateAnimationTree( float elapsedSeconds )
		{
			if ( m_ogreAnimationSystem == null )
			{
				return;
			}
			
			m_ogreAnimationSystem.update( elapsedSeconds );
			
		}

		public void LoadAnimationTree( String filename )
		{
			if ( m_ogreAnimationSystem == null )
			{
				return;
			}

			m_ogreAnimationSystem.load( filename );
		}

		public void SetParameterValue( string parameterName, float parameterValue )
		{
			if ( m_ogreAnimationSystem == null )
			{
				return;
			}

			if ( ! m_ogreAnimationSystem.hasParameter( parameterName ) )
			{
				return;
			}

			m_ogreAnimationSystem.getParameter( parameterName ).setValue( parameterValue );
		}

		#endregion


		#region IModel Members

		public event ModelEventHandler ModelChanged;

		public float BoundingRadius
		{
			get
			{
				if ( OgreEntity == null )
				{
					return 0;
				}

				return OgreEntity.BoundingRadius;
			}
		}

		public Vector3 Position
		{
			get
			{
				if ( OgreSceneNode == null )
				{
					return new Vector3();
				}

				return OgreSceneNode.Position;
			}
		}

		public Mogre.Vector3 Center
		{
			get
			{
				if ( OgreEntity == null )
				{
					return new Vector3();
				}

				return Position + OgreEntity.BoundingBox.Center;
			}
		}

		#endregion


		#region Common Callbacks
		private void onModelLoaded()
		{
			CreateAnimations();
			CreateSkeleton();

			if ( LoadModelSuccess != null )
			{
				LoadModelSuccess( this, new EventArgs() );
			}

			if ( ModelChanged != null )
			{
				ModelChanged( this, new EventArgs() );
			}
		}

		private void onModelDeleted()
		{
			CreateAnimations();
			CreateSkeleton();

			if ( DeleteModelSuccess != null )
			{
				DeleteModelSuccess( this, new EventArgs() );
			}

			if ( ModelChanged != null )
			{
				ModelChanged( this, new EventArgs() );
			}
		}

		private void onModelSaved()
		{
			if ( SaveModelSuccess != null )
			{
				SaveModelSuccess( this, new EventArgs() );
			}
		}
		#endregion


		#region Skeleton Creation
		private void CreateSkeleton()
		{
			if ( OgreEntity == null )
			{
				return;
			}

			if ( OgreEntity.Skeleton == null )
			{
				return;
			}

			DestroySkeleton();

			foreach ( Bone bone in OgreEntity.Skeleton.GetBoneIterator() )
			{
				List<Node> childNodes = new List<Node>();
				foreach ( Node child in bone.GetChildIterator() )
				{
					childNodes.Add( child );
				}

				foreach ( Node child in childNodes )
				{
					String BoneEntityName = bone.Name + " -> " + child.Name;
					Entity boneEntity = SceneManager.CreateEntity( BoneEntityName, BoneMeshName );
					boneEntity.VisibilityFlags = SkeletonVisibilityFlag;

					TagPoint boneTagPoint = OgreEntity.AttachObjectToBone( bone.Name, boneEntity );

					boneTagPoint.Orientation = ( ( Vector3 ) ( boneTagPoint.Orientation * Vector3.UNIT_Y ) ).GetRotationTo( child.Position );
					boneTagPoint.Scale( BoneScaleFactor, child.Position.Length, BoneScaleFactor );

					SkeletonEntityNames.Add( BoneEntityName );
					SkeletonTagPoints.Add( boneTagPoint );
				}


				if ( childNodes.Count == 0 )
				{
					String BoneEntityName = bone.Name;
					Entity boneEntity = SceneManager.CreateEntity( BoneEntityName, BoneMeshName );
					boneEntity.VisibilityFlags = SkeletonVisibilityFlag;

					TagPoint boneTagPoint = OgreEntity.AttachObjectToBone( bone.Name, boneEntity );

					boneTagPoint.Scale( new Vector3( BoneScaleFactor * 2 ) );
					boneTagPoint.Yaw( new Degree( 180 ) );
					boneTagPoint.Roll( new Degree( 90 ) );

					SkeletonEntityNames.Add( BoneEntityName );
					SkeletonTagPoints.Add( boneTagPoint );
				}
			}
		}


		private void DestroySkeleton()
		{
			foreach ( TagPoint boneTagPoint in SkeletonTagPoints )
			{
				boneTagPoint.Parent.RemoveChild( boneTagPoint );
				boneTagPoint.Dispose();
			}
			SkeletonTagPoints = new List<TagPoint>();

			foreach ( String boneEntityName in SkeletonEntityNames )
			{
				SceneManager.DestroyEntity( boneEntityName );
			}
			SkeletonEntityNames = new List<String>();
		}
		#endregion

		#region Attributes
		private Root m_root;
		private SceneManager m_sceneManager;

		private Entity m_ogreEntity;
		private SceneNode m_ogreSceneNode;

		private String m_lastLoadedCharacterPath;
		private StringVector m_additionalMaterialPaths;

		private float m_speed;
		private bool m_enabled;
		private Dictionary<String, IAnimation> m_animations;

		private List<String> m_skeletonEntityNames;
		private List<TagPoint> m_skeletonTagPoints;
		#endregion

		#region Properties
		protected Root Root
		{
			get { return m_root; }
			set { m_root = value; }
		}

		protected SceneManager SceneManager
		{
			get { return m_sceneManager; }
			set { m_sceneManager = value; }
		}

		protected Entity OgreEntity
		{
			get { return m_ogreEntity; }
			set { m_ogreEntity = value; }
		}

		protected SceneNode OgreSceneNode
		{
			get { return m_ogreSceneNode; }
			set { m_ogreSceneNode = value; }
		}

		protected String LastLoadedCharacterPath
		{
			get { return m_lastLoadedCharacterPath; }
			set { m_lastLoadedCharacterPath = value; }
		}

		protected StringVector AdditionalMaterialPaths
		{
			get { return m_additionalMaterialPaths; }
			set { m_additionalMaterialPaths = value; }
		}

		public bool ModelLoaded
		{
			get { return ( OgreEntity != null ); }
		}

		protected String CharacterResourceGroupName
		{
			get { return "CharacterResourceGroup"; }
		}

		protected String CharacterMaterialResourceGroupName
		{
			get { return "CharacterMaterialRsourceGroup"; }
		}

		public Dictionary<String, IAnimation> Animations
		{
			get { return m_animations; }
			protected set { m_animations = value; }
		}

		protected float BoneScaleFactor
		{
			get { return BoundingRadius * 0.02f; }
		}

		protected String BoneMeshName
		{
			get { return "bone.mesh"; }
		}

		protected List<String> SkeletonEntityNames
		{
			get { return m_skeletonEntityNames; }
			set { m_skeletonEntityNames = value; }
		}

		protected List<TagPoint> SkeletonTagPoints
		{
			get { return m_skeletonTagPoints; }
			set { m_skeletonTagPoints = value; }
		}

		static public uint ModelVisibilityFlag
		{
			get { return 1; }
		}
		static public uint SkeletonVisibilityFlag
		{
			get { return 2; }
		}
		#endregion


	}
}
