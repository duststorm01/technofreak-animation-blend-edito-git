/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TecnoFreakCommon
{
	public delegate void ViewportEventHandler( object sender, EventArgs e );

	public interface IViewport
	{
		event ViewportEventHandler BackgroundColorChanged;
		event ViewportEventHandler ViewportModeChanged;
		event ViewportEventHandler SkeletonVisibilityChanged;
		event ViewportEventHandler BoneSelectionChanged;

		Color BackgroundColor
		{
			get;
			set;
		}

		String ViewportMode
		{
			get;
			set;
		}

		List<String> AvailableViewportModes
		{
			get;
		}

		bool ShowSkeleton
		{
			get;
			set;
		}

		void SetMouseBoneSelectionMode( bool enableBoneSelectionMode );
		void ResetBoneDisplayWeights();
		int GetBoneCount();
		void SetBoneSelectedByIndex( int boneId, bool select );
		void ClearSelectedBones();
		bool IsBoneSelectedByIndex( int boneId );
		float GetBoneDisplayWeightByIndex( int boneId );
		void SetBoneDisplayWeightByIndex( int boneId, float weight );
		String GetBoneNameByIndex( int boneId );
	}
}
