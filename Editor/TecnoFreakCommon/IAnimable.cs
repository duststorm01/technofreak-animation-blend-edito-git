/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace TecnoFreakCommon
{
	public delegate void AnimableEventHandler(object sender, EventArgs e);

	public interface IAnimable
	{
		event AnimableEventHandler SpeedChanged;
		event AnimableEventHandler EnabledChanged;
		event AnimableEventHandler AnimationTreeControlChanged;

		event AnimableEventHandler Paused;
		event AnimableEventHandler Started;
		event AnimableEventHandler Stopped;

		event AnimableEventHandler AnimationsChanged;


		float Speed
		{
			get;
			set;
		}

		bool Enabled
		{
			get;
			set;
		}

		bool AnimationTreeControl
		{
			get;
			set;
		}

		Dictionary<String, IAnimation> Animations
		{
			get;
		}

		IAnimation GetAnimation(String animationName);

		void Pause();
		void Start();
		void Stop();

		void LoadAnimationTree(String filename);

		void SetParameterValue(string parameterName, float parameterValue);
	}
}
