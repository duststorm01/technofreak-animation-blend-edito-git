/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace TecnoFreak.v2.Controller.Project
{
	public class ProjectNameValidator
	{
		public static bool validName( String projectLocation, String projectName )
		{
			try
			{
				DirectoryInfo projectBaseDirectory = new DirectoryInfo( projectLocation );

				if ( ! projectBaseDirectory.Exists )
				{
					return false;
				}

				
				DirectoryInfo projectDirectory = new DirectoryInfo( projectLocation + Path.DirectorySeparatorChar + projectName );

				
				if ( projectDirectory.Exists )
				{
					return false;
				}

				if ( !UserHasWritePermission( projectBaseDirectory ) )
				{
					return false;
				}

				return true;

			}
			catch ( Exception )
			{
				return false;
			}
		}

		// http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=2877865&SiteID=1
		// FIXME: Doesn't work with public folders in Vista ( currentUser )
		private static bool UserHasWritePermission( DirectoryInfo projectDirectory )
		{
			
			try
			{
				String currentUser = WindowsIdentity.GetCurrent().Name.ToString();
				DirectorySecurity ds = projectDirectory.GetAccessControl();
				AuthorizationRuleCollection arc = ds.GetAccessRules( true, true, typeof( NTAccount ) );

				foreach ( FileSystemAccessRule fsar in arc )
				{
					if ( fsar.IdentityReference.Value == currentUser )
					{
						FileSystemRights rights = fsar.FileSystemRights ^ FileSystemRights.Write;
						if ( rights != 0 )
						{
							return true;
						}
					}
				}

				return false;
			}
			catch ( Exception )
			{
				return false;
			}
		}

	}
}
