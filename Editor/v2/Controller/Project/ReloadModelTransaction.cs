/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreak.v2.Controller.ModelIO;

namespace TecnoFreak.v2.Controller.Project
{
	public class ReloadModelTransaction : Interfaces.ITransaction
	{

		public ReloadModelTransaction( Model.Project project, String filename )
		{
			Project = project;
			Filename = filename;
		}

		private Model.Project m_project;
		private String m_filename;

		public String Filename
		{
			get { return m_filename; }
			set { m_filename = value; }
		}

		public Model.Project Project
		{
			get { return m_project; }
			set { m_project = value; }
		}

		public void Execute()
		{
			LoadModelTransaction lmt = new LoadModelTransaction( Project.AnimableLoader );
			lmt.Filename = Filename;

			lmt.Execute();
			if ( !lmt.OperationSuccess )
			{
				return;
			}

			Project.AddModel( lmt.Filename );
			Project.MarkAsActive( lmt.Filename );
		}
	}
}
