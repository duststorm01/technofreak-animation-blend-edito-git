/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using TecnoFreak.v2.View;
using System.IO;


namespace TecnoFreak.v2.Controller.Project
{
	public class NewProjectTransaction : Interfaces.ITransaction
	{

		public NewProjectTransaction( Model.Project project )
		{
			Project = project;
			OperationSuccess = false;
		}

		private Model.Project m_project;

		public Model.Project Project
		{
			get { return m_project; }
			set { m_project = value; }
		}

		private String m_projectName;

		public String ProjectName
		{
			get { return m_projectName; }
			private set { m_projectName = value; }
		}
		private String m_projectLocation;

		public String ProjectLocation
		{
			get { return m_projectLocation; }
			private set { m_projectLocation = value; }
		}

		private bool m_operationSuccess;

		public bool OperationSuccess
		{
			get { return m_operationSuccess; }
			private set { m_operationSuccess = value; }
		}

		#region Transaction Members

		public void Execute()
		{
			OperationSuccess = false;

			if ( Project == null )
			{
				return;
			}

			bool continueTransaction = true;

			if ( Project.HasChanges )
			{
				continueTransaction = AskUserToSaveProject();
				if ( ! continueTransaction )
				{
					return;
				}
			}

			continueTransaction = AskUserProjectParameters();
			if ( ! continueTransaction )
			{
				return;
			}

			Project.New( ProjectLocation, ProjectName );

			CreateProjectFiles();

			OperationSuccess = true;
		}

		private void CreateProjectFiles()
		{
			DirectoryInfo projectDirectory = new DirectoryInfo( ProjectLocation + Path.DirectorySeparatorChar + ProjectName );
			projectDirectory.Create();

			SaveProject( projectDirectory.FullName + Path.DirectorySeparatorChar + "project.tfproj" );
		}



		private bool AskUserProjectParameters()
		{
			const bool CONTINUE_TRANSACTION = true;

			NewProjectView projectView = new NewProjectView();
			DialogResult result = projectView.ShowDialog();

			if ( result == DialogResult.OK )
			{
				ProjectLocation = projectView.ProjectLocation;
				ProjectName = projectView.ProjectName;
				return CONTINUE_TRANSACTION;
			}
			else
			{
				return ! CONTINUE_TRANSACTION;
			}
		}

		private bool AskUserToSaveProject()
		{
			const bool CONTINUE_TRANSACTION = true;

			DialogResult result = MessageBox.Show( "The current project may have unsaved changes, do you want to save it now?", "Unsaved changes", MessageBoxButtons.YesNoCancel );

			if ( result == DialogResult.Yes )
			{
				SaveProject();
				return CONTINUE_TRANSACTION;
			}
			else if ( result == DialogResult.No )
			{
				return CONTINUE_TRANSACTION;
			}
			else if ( result == DialogResult.Cancel )
			{
				return !CONTINUE_TRANSACTION;
			}
			else
			{
				// We shouldn't get here...
				Debug.Assert( false );
				return !CONTINUE_TRANSACTION;
			}
		}

		private void SaveProject()
		{
			ProjectIO pio = new ProjectIO();
			pio.SaveProject( Project );
		}

		private void SaveProject( String filename )
		{
			ProjectIO pio = new ProjectIO();
			pio.SaveProject( Project, filename );
		}

		#endregion
	}
}
