/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace TecnoFreak.v2.Controller.Project
{
	public class LoadProjectTransaction : Interfaces.ITransaction
	{

		public LoadProjectTransaction( Model.Project project )
		{
			Project = project;
			OperationSuccess = false;
		}

		private Model.Project m_project;

		public Model.Project Project
		{
			get { return m_project; }
			set { m_project = value; }
		}

		String m_projectFilename;
		public System.String ProjectFilename
		{
			get { return m_projectFilename; }
			private set { m_projectFilename = value; }
		}

		private bool m_operationSuccess;

		public bool OperationSuccess
		{
			get { return m_operationSuccess; }
			private set { m_operationSuccess = value; }
		}


		public void Execute()
		{
			OperationSuccess = false;

			if ( Project == null )
			{
				return;
			}

			bool continueTransaction = true;


			if ( Project.HasChanges )
			{
				continueTransaction = AskUserToSaveProject();
				if ( !continueTransaction )
				{
					return;
				}
			}

			continueTransaction = AskUserForProjectFile();
			if ( !continueTransaction )
			{
				return;
			}

			ProjectIO pio = new ProjectIO();
			pio.LoadProject( Project, ProjectFilename );

			Project.MarkSaved();

			OperationSuccess = true;
		}

		private bool AskUserForProjectFile()
		{
			OpenFileDialog fd = new OpenFileDialog();
			fd.Filter = "TecnoFreak Project Files (*.tfproj)|*.tfproj" + "|" + "All Files (*.*)|*.*";
			DialogResult result = fd.ShowDialog();

			if ( result == DialogResult.OK )
			{
				ProjectFilename = fd.FileName;
				return true;
			}
			else
			{
				return false;
			}
		}


		private bool AskUserToSaveProject()
		{
			const bool CONTINUE_TRANSACTION = true;

			DialogResult result = MessageBox.Show( "The current project may have unsaved changes, do you want to save it now?", "Unsaved changes", MessageBoxButtons.YesNoCancel );

			if ( result == DialogResult.Yes )
			{
				SaveProject();
				return CONTINUE_TRANSACTION;
			}
			else if ( result == DialogResult.No )
			{
				return CONTINUE_TRANSACTION;
			}
			else if ( result == DialogResult.Cancel )
			{
				return !CONTINUE_TRANSACTION;
			}
			else
			{
				// We shouldn't get here...
				Debug.Assert( false );
				return !CONTINUE_TRANSACTION;
			}
		}

		private void SaveProject()
		{
			ProjectIO pio = new ProjectIO();
			pio.SaveProject( Project );
		}

	}
}
