/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace TecnoFreak.v2.Controller.Project
{
	public class ProjectIO
	{
		public ProjectIO()
		{

		}

// 		private DiagramIO.DiagramIO m_diagramLoader = new TecnoFreak.v2.Controller.DiagramIO.DiagramIO();
// 
// 		public DiagramIO.DiagramIO DiagramLoader
// 		{
// 			get { return m_diagramLoader; }
// 		}

		public void LoadProject( Model.Project project, String filename )
		{
			
			String path = Path.GetDirectoryName( filename );
			DirectoryInfo si = new DirectoryInfo( path );
			
			project.New( si.Parent.FullName, si.Name.ToString() );

			project.Filename = filename;

			XmlTextReader reader = new XmlTextReader( filename );
			XmlDocument doc = new XmlDocument();
			doc.Load( reader );

			XmlElement projectNode = ( XmlElement ) doc.GetElementsByTagName( "project" ).Item( 0 );

			XmlElement modelListNode = ( XmlElement ) projectNode.GetElementsByTagName( "model_list" ).Item( 0 );
			ReadModels( modelListNode, project );
			/*
			reader.ReadStartElement( "project" );

			reader.ReadToFollowing( "model_list" );
			ReadModels( reader.ReadSubtree(), project );

			reader.Skip();

			reader.ReadEndElement();

			reader.Close();
			*/
			project.ActiveDiagram.LoadDiagram( project.ActiveDiagramPath );

			reader.Close();
		}

		private void ReadModels( XmlElement modelList, TecnoFreak.v2.Model.Project project )
		{
			foreach ( XmlNode node in modelList )
			{
				XmlElement model = ( XmlElement ) node;
				ReadModel( model, project );
			}
			/*reader.ReadStartElement( "model_list" );

			while ( reader.ReadToFollowing( "model" ) )
			{
				XmlReader subtreeReader = reader.ReadSubtree();
				subtreeReader.MoveToContent();
				ReadModel( subtreeReader, project );
			}*/
		}

		private void ReadModel( XmlElement model, TecnoFreak.v2.Model.Project project )
		{
			/*
			reader.ReadStartElement( "model" );

			String modelPath = reader.ReadString();
			project.AddModel( modelPath );
			 * */

			String modelPath = model.InnerText;
			project.AddModel( modelPath );
		}

		public void SaveProject( Model.Project project )
		{
			SaveProject( project, project.Filename );
		}

		public void SaveProject( Model.Project project, String filename )
		{
			project.Filename = filename;

			XmlTextWriter writer = new XmlTextWriter( filename, Encoding.UTF8 );
			writer.Formatting = Formatting.Indented;

			writer.WriteStartDocument();
			writer.WriteStartElement( "project" );

			WriteModels( writer, project );

			writer.WriteEndElement();
			writer.WriteEndDocument();

			writer.Flush();
			writer.Close();

			project.ActiveDiagram.SaveDiagram( project.ActiveDiagramPath );

			project.MarkSaved();
		}

		private void WriteModels( XmlTextWriter writer, Model.Project project )
		{
			writer.WriteStartElement( "model_list" );

			foreach ( String modelPath in project.ModelsPath )
			{
				WriteModel( writer, modelPath );
			}

			writer.WriteEndElement();
		}

		private void WriteModel( XmlTextWriter writer, string modelPath )
		{
			writer.WriteStartElement( "model" );
			writer.WriteValue( modelPath );
			writer.WriteEndElement();
		}
	}
}
