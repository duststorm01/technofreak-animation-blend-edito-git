/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Drawing;
using TFControls.Diagram;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using TecnoFreakCommon;
using TecnoFreak.v2.Controller.Interfaces;

namespace TecnoFreak.v2.Controller.DiagramIO
{
	public class DiagramIO
	{
		#region Attributes

		private Dictionary<Node, int> m_nodeIdDictionary = new Dictionary<Node, int>();

		XmlSerializer m_userDataSerializer = new XmlSerializer( typeof( PortDataType ) );
		XmlSerializer m_pointSerializer = new XmlSerializer( typeof( Point ) );
		XmlSerializer m_sizeSerializer = new XmlSerializer( typeof( Size ) );

		private OutputNode m_outputNode;

		INodeFactory m_nodeFactory = null;

		#endregion

		#region Properties
		protected Dictionary<Node, int> NodeIdDictionary
		{
			get { return m_nodeIdDictionary; }
		}

		protected XmlSerializer UserDataSerializer
		{
			get { return m_userDataSerializer; }
		}

		protected XmlSerializer PointSerializer
		{
			get { return m_pointSerializer; }
		}

		protected XmlSerializer SizeSerializer
		{
			get { return m_sizeSerializer; }
		}

		public OutputNode OutputNode
		{
			get { return m_outputNode; }
			protected set { m_outputNode = value; }
		}

		#endregion

		public DiagramIO( INodeFactory nodeFactory )
		{
			m_nodeFactory = nodeFactory;
		}

		public void SaveDiagram( Diagram diagram, String filename )
		{
			NodeIdDictionary.Clear();

			XmlTextWriter writer = new XmlTextWriter( filename, Encoding.UTF8 );
			writer.Formatting = Formatting.Indented;

			writer.WriteStartDocument();
			writer.WriteStartElement( "diagram" );

			WriteProperties( writer, diagram );
			WriteNodes( writer, diagram );
			WriteConnections( writer, diagram );

			writer.WriteEndElement();
			writer.WriteEndDocument();

			writer.Flush();
			writer.Close();
		}

		#region Write stuff

		private void WriteProperties( XmlWriter writer, Diagram diagram )
		{
			writer.WriteStartElement( "properties" );
			SizeSerializer.Serialize( writer, diagram.Size );
			writer.WriteEndElement();
		}

		private void WriteNodes( XmlWriter writer, Diagram diagram )
		{
			writer.WriteStartElement( "node_list" );

			for ( int nodeId = 0; nodeId < diagram.Nodes.Count; nodeId++ )
			{
				WriteNode( writer, diagram, nodeId );
			}

			writer.WriteEndElement();
		}

		private void WriteNode( XmlWriter writer, Diagram diagram, int nodeId )
		{
			BaseNode node = ( BaseNode ) diagram.Nodes[ nodeId ];

			// we need to store a relationship between nodes and id's, as the diagram
			// mantains no information on the id's of items on it's list.
			NodeIdDictionary.Add( node, nodeId );

			String type = node.GetType().ToString();

			writer.WriteStartElement( "node" );

			writer.WriteStartAttribute( "id" );
			writer.WriteValue( nodeId );
			writer.WriteEndAttribute();

			writer.WriteStartAttribute( "name" );
			writer.WriteString( node.Name );
			writer.WriteEndAttribute();

			writer.WriteStartAttribute( "type" );
			writer.WriteString( type );
			writer.WriteEndAttribute();

			writer.WriteStartElement( "position" );
			PointSerializer.Serialize( writer, node.NodeView.Position );
			writer.WriteEndElement();

			writer.WriteStartElement( "custom" );
			node.WriteCustomXmlData( writer );
			writer.WriteEndElement();

			WritePorts( writer, node );

			writer.WriteEndElement();
		}

		private void WritePorts( XmlWriter writer, Node node )
		{
			writer.WriteStartElement( "port_list" );
			
			writer.WriteStartElement( "in_port_list" );
			foreach ( Port inPort in node.InPorts.Values )
			{
				WritePort( writer, inPort );
			}
			writer.WriteEndElement();

			writer.WriteStartElement( "out_port_list" );
			foreach ( Port outPort in node.OutPorts.Values )
			{
				WritePort( writer, outPort );
			}
			writer.WriteEndElement();

			writer.WriteEndElement();
		}

		private void WritePort( XmlWriter writer, Port port )
		{
			writer.WriteStartElement( "port" );

			writer.WriteStartAttribute( "name" );
			writer.WriteString( port.Name );
			writer.WriteEndAttribute();

			if ( port.ValueControl != null )
			{
				writer.WriteStartAttribute( "value" );
				writer.WriteValue( port.ValueControl.Value );
				writer.WriteEndAttribute();

				writer.WriteStartAttribute( "min" );
				writer.WriteValue( port.ValueControl.MinValue );
				writer.WriteEndAttribute();

				writer.WriteStartAttribute( "max" );
				writer.WriteValue( port.ValueControl.MaxValue );
				writer.WriteEndAttribute();
			}

			writer.WriteStartElement( "user_data" );
			UserDataSerializer.Serialize( writer, port.UserData );
			writer.WriteEndElement();

			writer.WriteEndElement();
		}

		private void WriteConnections( XmlWriter writer, Diagram diagram )
		{
			writer.WriteStartElement( "connection_list" );

			for ( int nodeId = 0; nodeId < diagram.Nodes.Count; nodeId++ )
			{
				Node node = diagram.Nodes[ nodeId ];

				// connections are bidirectional, so there is no need to read both set of ports.
				foreach ( Port inPort in node.InPorts.Values )
				{
					foreach ( Port outPort in inPort.Connections )
					{
						writer.WriteStartElement( "connection" );

						writer.WriteStartAttribute( "node_from" );
						writer.WriteValue( nodeId );
						writer.WriteEndAttribute();

						writer.WriteStartAttribute( "port_from" );
						writer.WriteString( inPort.Name );
						writer.WriteEndAttribute();

						writer.WriteStartAttribute( "node_to" );
						writer.WriteValue( NodeIdDictionary[ outPort.Parent ] );
						writer.WriteEndAttribute();

						writer.WriteStartAttribute( "port_to" );
						writer.WriteString( outPort.Name );
						writer.WriteEndAttribute();

						writer.WriteEndElement();
					}
				}
			}
			
			writer.WriteEndElement();
		}

		#endregion


		public void LoadDiagram( IAnimable animable, Diagram diagram, String filename )
		{
			NodeIdDictionary.Clear();

			diagram.New();

			XmlTextReader reader = new XmlTextReader( filename );

			reader.ReadStartElement( "diagram" );

			reader.ReadToFollowing( "properties" );
			ReadProperties( reader.ReadSubtree(), diagram );
			
			reader.ReadToFollowing( "node_list" );
			ReadNodes( reader.ReadSubtree(), diagram );
			
			reader.ReadToFollowing( "connection_list" );	
			ReadConnections( reader.ReadSubtree(), diagram );

			reader.Skip();

			reader.ReadEndElement();

			reader.Close();

			SetAnimable( animable, diagram );
		}

		private void SetAnimable( IAnimable animable, Diagram diagram )
		{
			foreach ( Node node in diagram.Nodes )
			{
				BaseNode baseNode = ( BaseNode ) node;
				baseNode.Animable = animable;
			}
		}
	
		private void ReadConnections( XmlReader reader, Diagram diagram )
		{
			reader.ReadStartElement( "connection_list" );

			while ( reader.ReadToFollowing( "connection" ) )
			{
				XmlReader subtreeReader = reader.ReadSubtree();
				subtreeReader.MoveToContent();
				ReadConnection( subtreeReader, diagram );
			}

		}

		private void ReadConnection( XmlReader reader, Diagram diagram )
		{
			reader.MoveToAttribute( "node_from" );
			int nodeFromId = reader.ReadContentAsInt();

			reader.MoveToAttribute( "port_from" );
			String portFromName = reader.Value;

			reader.MoveToAttribute( "node_to" );
			int nodeToId = reader.ReadContentAsInt();

			reader.MoveToAttribute( "port_to" );
			String portToName = reader.Value;

			try
			{
				Node nodeFrom = diagram.Nodes[ nodeFromId ];
				Port portFrom = nodeFrom.Ports[ portFromName ];

				Node nodeTo = diagram.Nodes[ nodeToId ];
				Port portTo = nodeTo.Ports[ portToName ];

				diagram.Connector.Connect( portFrom, portTo );
			}
			catch ( Exception )
			{
				// At the moment, silently ignore problems...
			}
		}

		private void ReadNodes( XmlReader reader, Diagram diagram )
		{
			diagram.Nodes.Clear();

			reader.ReadStartElement( "node_list" );
			
			while ( reader.ReadToFollowing( "node" ) )
			{
				XmlReader subtreeReader = reader.ReadSubtree();
				subtreeReader.MoveToContent();
				ReadNode( subtreeReader, diagram );
			}

		}

		private void ReadNode( XmlReader reader, Diagram diagram )
		{

			reader.MoveToAttribute( "name" );
			String name = reader.Value;

			reader.MoveToAttribute( "type" );
			String typeString = reader.Value;

			reader.MoveToAttribute( "id" );
			int nodeId = reader.ReadContentAsInt();
			
			reader.MoveToElement();

			//Type type = Type.GetType( typeString );
			//BaseNode node = ( BaseNode ) Activator.CreateInstance( type );
			BaseNode node = m_nodeFactory.CreateNodeByType( typeString );

			node.Name = name;

			reader.ReadStartElement( "node" );
			reader.ReadStartElement( "position" );
			node.NodeView.Position = ( Point ) PointSerializer.Deserialize( reader );
			reader.ReadEndElement();



			reader.ReadToFollowing( "custom" );
			XmlReader customPropertiesReader = reader.ReadSubtree();
			customPropertiesReader.MoveToContent();
			node.ReadCustomXmlData( customPropertiesReader );

			// TODO: Load node values
			reader.ReadToFollowing( "port_list" );
			XmlReader portListReader = reader.ReadSubtree();
			portListReader.MoveToContent();
			ReadPortList( portListReader, node );
			
			diagram.AddNode( node );
			if ( typeString == typeof( OutputNode ).ToString() )
			{
				OutputNode = ( OutputNode ) node;
			}
		}

		private void ReadPortList( XmlReader reader, BaseNode node )
		{
			reader.ReadToFollowing( "in_port_list" );
			XmlReader inPortListReader = reader.ReadSubtree();
			inPortListReader.MoveToContent();
			ReadPorts( inPortListReader, node );

			reader.ReadToFollowing( "out_port_list" );
			XmlReader outPortListReader = reader.ReadSubtree();
			outPortListReader.MoveToContent();
			ReadPorts( outPortListReader, node );
		}

		private void ReadPorts( XmlReader reader, BaseNode node )
		{
			while ( reader.ReadToFollowing( "port" ) )
			{
				XmlReader subtreeReader = reader.ReadSubtree();
				subtreeReader.MoveToContent();
				ReadPort( subtreeReader, node );
			}
		}

		private void ReadPort( XmlReader reader, BaseNode node )
		{
			reader.MoveToAttribute( "name" );
			String portName = reader.Value;

			Port port = node.GetPort( portName );

			if ( port.ValueControl != null )
			{
				reader.MoveToAttribute( "value" );
				float portValue = reader.ReadContentAsFloat();
				port.ValueControl.Value = portValue;

				if ( reader.MoveToAttribute( "min" ) )
				{
					float minValue = reader.ReadContentAsFloat();
					port.ValueControl.MinValue = minValue;
				}

				if ( reader.MoveToAttribute( "max" ) )
				{
					float maxValue = reader.ReadContentAsFloat();
					port.ValueControl.MaxValue = maxValue;
				}
			}

			reader.MoveToElement();

			reader.ReadStartElement( "port" );
			reader.ReadStartElement( "user_data" );
			object userData = UserDataSerializer.Deserialize( reader );
			reader.ReadEndElement();
			reader.ReadEndElement();

			port.UserData = userData;
		}

		private void ReadProperties( XmlReader reader, Diagram diagram )
		{
			reader.ReadStartElement( "properties" );
			diagram.Size = ( Size ) SizeSerializer.Deserialize( reader );
			reader.ReadEndElement();
		}

	}
}
