/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using System.Xml;
using System.Diagnostics;

namespace TecnoFreak.v2.Controller.DiagramIO
{
	public class DiagramWorkRuntimeIO
	{
		#region Attributes
		private Dictionary<BaseNode, int> m_nodeToId;
		private List<BaseNode> m_idToNode;
		private OutputNode m_outputNode;
		private Dictionary<String, ParameterNode> m_parameterNameToNode;

		private List<Port> m_workParameters;
		private List<ParameterNode> m_workParameterNodes;
		#endregion

		#region Properties
		protected Dictionary<BaseNode, int> NodeToId
		{
			get { return m_nodeToId; }
			private set { m_nodeToId = value; }
		}

		protected List<BaseNode> IdToNode
		{
			get { return m_idToNode; }
			private set { m_idToNode = value; }
		}

		protected OutputNode OutputNode
		{
			get { return m_outputNode; }
			private set { m_outputNode = value; }
		}

		protected Dictionary<String, ParameterNode> ParameterNameToNode
		{
			get { return m_parameterNameToNode; }
			private set { m_parameterNameToNode = value; }
		}

		public List<Port> WorkParameters
		{
			get { return m_workParameters; }
			set { m_workParameters = value; }
		}
		#endregion

		public DiagramWorkRuntimeIO()
		{
		}

		// replace constant nodes with parameters
		// replace ports with parameters
		public void SaveDiagram( Diagram diagram, String filename )
		{
			NodeToId = new Dictionary<BaseNode, int>();
			IdToNode = new List<BaseNode>();
			ParameterNameToNode = new Dictionary<String, ParameterNode>();
			WorkParameters = new List<Port>();
			m_workParameterNodes = new List<ParameterNode>();

			ResetPortWorkNames( diagram );

			foreach ( Node node in diagram.Nodes )
			{
				BaseNode baseNode = ( BaseNode )( node );
				Debug.Assert( baseNode != null );


				if ( baseNode.GetType() == typeof( OutputNode ) )
				{
					OutputNode = ( OutputNode )( baseNode );
				}
				else if ( baseNode.GetType() == typeof( ConstantNode ) )
				{
					/* Constant nodes are replaced by parameter nodes*/
					ConstantNode constantNode = ( ConstantNode )( baseNode );

					ParameterNode constantReplace = new ParameterNode();
					Port valuePort = constantNode.Ports[ "value" ];
					constantReplace.ParameterName = valuePort.PortWorkParameterName;
					
					foreach ( Port portConnectedToConstant in valuePort.Connections )
					{
						// add a one way connection so that only the parameter node knows about this connection
						constantReplace.Ports[ "value" ].Connections.Add( portConnectedToConstant );

						// set the port connected work name to the same as the parameter name.
						portConnectedToConstant.PortWorkParameterName = constantReplace.ParameterName;
					}

					int nodeId = NodeToId.Count;
					NodeToId.Add( constantReplace, nodeId );
					IdToNode.Add( constantReplace );
					m_workParameterNodes.Add( constantReplace );

					WorkParameters.Add( valuePort );
				}
				else
				{
					if ( baseNode.RuntimeName == null )
					{
						throw new Exception( "Node " + baseNode.Name + " doesn't have a runtime name" );
					}

					int nodeId = NodeToId.Count;
					NodeToId.Add( baseNode, nodeId );
					IdToNode.Add( baseNode );
				}

				if ( baseNode.GetType() == typeof( ParameterNode ) )
				{
					ParameterNode parameterNode = ( ParameterNode )( baseNode );
					String parameterName = parameterNode.ParameterName;

					if ( !ParameterNameToNode.ContainsKey( parameterName ) )
					{
						ParameterNameToNode.Add( parameterName, parameterNode );
					}
				}
				else
				{
					// foreach unconnected port with value control create a parameter and connect it to the node at that port.
					foreach ( Port inPort in node.InPorts.Values )
					{
						if ( inPort.Connections.Count == 0 && inPort.ValueControl != null )
						{

							ParameterNode parameterNode = new ParameterNode();
							parameterNode.ParameterName = inPort.PortWorkParameterName;
							parameterNode.Ports[ "value" ].Connections.Add( inPort );

							int nodeId = NodeToId.Count;
							NodeToId.Add( parameterNode, nodeId );
							IdToNode.Add( parameterNode );
							m_workParameterNodes.Add( parameterNode );

							WorkParameters.Add( inPort );
						}
					}
				}
			}

			XmlTextWriter writer = new XmlTextWriter( filename, Encoding.UTF8 );
			writer.Formatting = Formatting.Indented;

			writer.WriteStartDocument();
			writer.WriteStartElement( "animation_system" );

			WriteNodes( writer );
			WriteConnections( writer );
			WriteOutputNodeConnection( writer );
			WriteConstants( writer );
			WriteParameters( writer );

			writer.WriteEndElement();
			writer.WriteEndDocument();

			writer.Flush();
			writer.Close();
		}

		private void WriteConstants( XmlTextWriter writer )
		{
			writer.WriteStartElement( "constants" );
			writer.WriteEndElement();
		}

		private void ResetPortWorkNames( Diagram diagram )
		{
			foreach( Node n in diagram.Nodes )
			{
				foreach ( Port p in n.Ports.Values )
				{
					p.ResetPortName();
				}
			}
		}


		private void WriteNodes( XmlTextWriter writer )
		{

			writer.WriteStartElement( "nodes" );

			for ( int nodeId = 0; nodeId < IdToNode.Count; nodeId++ )
			{
				BaseNode node = IdToNode[ nodeId ];
				WriteNode( writer, nodeId, node );
			}

			writer.WriteEndElement();
		}

		private void WriteNode( XmlTextWriter writer, int nodeId, BaseNode node )
		{
			writer.WriteStartElement( "node" );

			writer.WriteStartAttribute( "type" );
			writer.WriteValue( node.RuntimeName );
			writer.WriteEndAttribute();

			writer.WriteStartAttribute( "id" );
			writer.WriteValue( nodeId );
			writer.WriteEndAttribute();

			writer.WriteStartElement( "custom" );
			node.WriteRuntimeXmlData( writer );
			writer.WriteEndElement();

			writer.WriteEndElement();
		}

		private void WriteConnections( XmlTextWriter writer )
		{
			writer.WriteStartElement( "connections" );

			

			for ( int nodeIdIn = 0; nodeIdIn < IdToNode.Count; nodeIdIn++ )
			{
				BaseNode nodeIn = IdToNode[ nodeIdIn ];
				WriteNodeConnections( writer, nodeIdIn, nodeIn );
			}

			// write work parameter connections
			foreach ( ParameterNode workParameterNode in m_workParameterNodes )
			{
				BaseNode nodeOut = ( BaseNode )( workParameterNode );
				int nodeIdOut = NodeToId[ nodeOut ];

				foreach ( Port portIn in workParameterNode.Ports[ "value" ].Connections )
				{
					int nodeIdIn = NodeToId[ (BaseNode)(portIn.Parent)];

					writer.WriteStartElement( "connection" );

					writer.WriteStartAttribute( "node_in" );
					writer.WriteValue( nodeIdIn );
					writer.WriteEndAttribute();

					writer.WriteStartAttribute( "port_in" );
					writer.WriteValue( portIn.Name );
					writer.WriteEndAttribute();

					writer.WriteStartAttribute( "node_out" );
					writer.WriteValue( nodeIdOut );
					writer.WriteEndAttribute();

					writer.WriteEndElement();
				}
			}

			writer.WriteEndElement();
		}

		private void WriteOutputNodeConnection( XmlTextWriter writer )
		{
			if ( OutputNode.GetInPort( "in" ).Connections.Count <= 0 )
			{
				return;
			}

			BaseNode node = ( BaseNode )( OutputNode.GetInPort( "in" ).Connections[ 0 ].Parent );
			if ( node == null )
			{
				return;
			}

			int nodeId = NodeToId[ node ];

			writer.WriteStartElement( "output" );
			writer.WriteValue( nodeId );
			writer.WriteEndElement();
		}

		private void WriteNodeConnections( XmlTextWriter writer, int nodeIdIn, BaseNode nodeIn )
		{
			foreach ( Port portIn in nodeIn.InPorts.Values )
			{
				WriteConnection( writer, nodeIdIn, portIn );
			}
		}

		private void WriteConnection( XmlTextWriter writer, int nodeIdIn, Port portIn )
		{
			bool isConnected = ( 0 < portIn.Connections.Count );
			if ( isConnected )
			{
				// Just support for one connection per in port.
				BaseNode nodeOut = ( BaseNode )( portIn.Connections[ 0 ].Parent );
				Debug.Assert( nodeOut != null );

				bool isConnectedToConstant = ( nodeOut.GetType() == typeof( ConstantNode ) );
				if ( isConnectedToConstant )
				{
					// This case will be adressed when exporting port constants.
					return;
				}

				int nodeIdOut = NodeToId[ nodeOut ];

				writer.WriteStartElement( "connection" );

				writer.WriteStartAttribute( "node_in" );
				writer.WriteValue( nodeIdIn );
				writer.WriteEndAttribute();

				writer.WriteStartAttribute( "port_in" );
				writer.WriteValue( portIn.Name );
				writer.WriteEndAttribute();

				writer.WriteStartAttribute( "node_out" );
				writer.WriteValue( nodeIdOut );
				writer.WriteEndAttribute();

				writer.WriteEndElement();
			}
		}

		

		private void WriteParameters( XmlTextWriter writer )
		{
			writer.WriteStartElement( "parameters" );

			foreach ( String parameterName in ParameterNameToNode.Keys )
			{
				WriteParameter( writer, parameterName );
			}

			foreach ( Port inPort in WorkParameters )
			{
				writer.WriteStartElement( "parameter" );

				writer.WriteStartAttribute( "name" );
				writer.WriteValue( inPort.PortWorkParameterName );
				writer.WriteEndAttribute();

				writer.WriteStartAttribute( "value" );
				writer.WriteValue( inPort.ValueControl.Value );
				writer.WriteEndAttribute();

				writer.WriteEndElement();
			}

			writer.WriteEndElement();
		}

		private void WriteParameter( XmlTextWriter writer, string parameterName )
		{
			writer.WriteStartElement( "parameter" );

			writer.WriteStartAttribute( "name" );
			writer.WriteValue( parameterName );
			writer.WriteEndAttribute();

			writer.WriteStartAttribute( "value" );
			writer.WriteValue( ParameterNameToNode[ parameterName ].GetValue() );
			writer.WriteEndAttribute();

			writer.WriteEndElement();
		}
	}
}

