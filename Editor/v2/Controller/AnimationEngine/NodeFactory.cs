/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode;
using System.Windows.Forms;
using TecnoFreak.v2.View;
using System.Drawing;
using TecnoFreak.v2.Controller.AnimationEngine.Builder;
using TecnoFreak.v2.Controller.Interfaces;
using System.IO;
using System.Reflection;

namespace TecnoFreak.v2.Controller.AnimationEngine
{
	public class NodeCreatedEventArgs : EventArgs
	{
		BaseNode m_node;
		public BaseNode Node
		{
			get { return m_node; }
		}

		public NodeCreatedEventArgs( BaseNode node )
		{
			m_node = node;
		}
	}

	public class NodeFactory : INodeFactory
	{

		public event EventHandler< NodeCreatedEventArgs > NodeCreated;


		List< String > m_plugins = new List< String >();
		List< String > m_dlls = new List< String >();
		List< Assembly > m_assemblies = new List< Assembly >();

		public NodeFactory()
		{
			StreamReader reader = null;
			try
			{
				reader = new StreamReader( Application.StartupPath + "/" + "nodes.plugins.txt" );
			}
			catch ( System.Exception )
			{
				return;
			}

			while ( !reader.EndOfStream )
			{
				String line = reader.ReadLine();
				String[] tokenizedLine = line.Split( ' ' );
				if ( tokenizedLine.Length == 2 )
				{
					bool isComment = tokenizedLine[ 0 ].StartsWith( "//" );
					if ( isComment )
					{

					}
					else if ( tokenizedLine[ 0 ] == "dll" )
					{
						m_dlls.Add( tokenizedLine[ 1 ] );
					}
					else if ( tokenizedLine[ 0 ] == "plugin" )
					{
						m_plugins.Add( tokenizedLine[ 1 ] );
					}
				}
			}

			foreach ( String s in m_dlls )
			{
				InitDll( s );
			}

			Init( null, new ToolStripMenuItem() );
			
		}

		public void Init( IAnimable animable, ToolStripMenuItem menu )
		{
			m_menu = menu;
			m_menu.DropDownItems.Clear();

			if ( animable != null )
			{
				foreach ( IAnimation animation in animable.Animations.Values )
				{
					AddBuilder( "Animation:Current:" + animation.Name, new AnimationNodeBuilder( animation ) );
				}
			}
			AddBuilder( "Animation:Custom", new CustomAnimationNodeBuilder( animable ) );

			AddBuilder( "Blender:Blend", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.BlendNode" ) );
			AddBuilder( "Blender:Mix", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MixNode" ) );
			AddBuilder( "Blender:Average", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.AverageNode" ) );
			AddBuilder( "Blender:LookAt", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.LookAtNode" ) );

			AddBuilder( "Math:Abs", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.AbsNode" ) );
			AddBuilder( "Math:Ceil", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.CeilNode" ) );
			AddBuilder( "Math:Clamp", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.ClampNode" ) );
			AddBuilder( "Math:Div", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.DivNode" ) );
			AddBuilder( "Math:Map", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MapNode" ) );
			AddBuilder( "Math:Max", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MaxNode" ) );
			AddBuilder( "Math:Min", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MinNode" ) );
			AddBuilder( "Math:Minus", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MinusNode" ) );
			AddBuilder( "Math:Mul", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.MulNode" ) );
			AddBuilder( "Math:Plus", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode.PlusNode" ) );

			AddBuilder( "Mask:Multiply", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskMulNode" ) );
			AddBuilder( "Mask:Subtract", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskSubNode" ) );
			AddBuilder( "Mask:Set", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskSetNode" ) );
			AddBuilder( "Mask:Add", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskAddNode" ) );

			AddBuilder( "Transition:Select", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.TransitionSelectNode" ) );
			AddBuilder( "Transition:Random", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.RandomSelectNode" ) );

			AddBuilder( "Parameter", new ParameterNodeBuilder() );

			AddBuilder( "Constant:E", new ConstantNodeBuilder( "E", ( float )( Math.E ) ) );
			AddBuilder( "Constant:PI", new ConstantNodeBuilder( "PI", ( float )( Math.PI ) ) );
			AddBuilder( "Constant:Custom", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.ConstantNode" ) );

			AddBuilder( "SmoothValue", new GenericNodeBuilder( "TecnoFreak.v2.Model.AnimationEngine.Nodes.SmoothValueNode" ) );

			InitPlugins( animable );
		}

		void InitPlugins( IAnimable animable )
		{
			foreach ( String s in m_plugins )
			{
				InitPlugin( s, animable );
			}
		}

		void InitDll( String dllName )
		{
			try
			{
				Assembly a = Assembly.LoadFile( Application.StartupPath + "/" + dllName );
				m_assemblies.Add( a );
			}
			catch ( System.Exception )
			{
				MessageBox.Show(  "Couldn't load dll: " + dllName, "Fail!" );
			}
		}

		void InitPlugin( String pluginName, IAnimable animable )
		{
			try
			{
				Type type = null;
				Assembly assembly = null;
				for ( int i = 0; i < m_assemblies.Count && type == null; i++ )
				{
					assembly = m_assemblies[ i ];
					type = assembly.GetType( pluginName );
				}
				if ( assembly == null )
				{
					MessageBox.Show( "Couldn't create plugin: " + pluginName + "\n\nDetails:\n" + "Couldn't find plugin in loaded dlls", "Fail!" );
					return;
				}
				IPlugin plugin = ( IPlugin )( assembly.CreateInstance( pluginName ) );
				plugin.Init( this, animable );
			}
			catch ( System.Exception e )
			{
				MessageBox.Show( "Couldn't create plugin: " + pluginName + "\n\nDetails:\n" + e.Message, "Fail!" );
			}
		}

		public BaseNode CreateNodeByType( String typeString )
		{
			Type type = Type.GetType( typeString );
			Assembly assembly = Assembly.GetExecutingAssembly();

			for ( int i = 0; i < m_assemblies.Count && type == null; i++ )
			{
				assembly = m_assemblies[ i ];
				type = assembly.GetType( typeString );
			}

			if ( type == null )
			{
				MessageBox.Show( "Couldn't create node " + typeString, "Fail!" );
				return null;
			}

			BaseNode node = ( BaseNode ) assembly.CreateInstance( typeString );
			return node;
			//BaseNode node = ( BaseNode ) Activator.CreateInstance( type );
		}

		public BaseNode CreateNodeByBuilder( String builderName )
		{
			if ( ! m_nodeBuilders.ContainsKey( builderName ) )
			{
				MessageBox.Show( "Couldn't find builder for node '" + builderName + "'", "Fail!" );
				return null;
			}

			INodeBuilder builder = m_nodeBuilders[ builderName ];

			if ( builder == null )
			{
				MessageBox.Show( "Null builder for node '" + builderName + "'", "Fail!" );
				return null;
			}

			BaseNode node = builder.CreateNode();
			if ( node == null )
			{
				MessageBox.Show( "Builder could not create node '" + builderName + "'", "Fail!" );
				return null;
			}

			if ( NodeCreated != null )
			{
				NodeCreated( this, new NodeCreatedEventArgs( node ) );
			}
			
			return node;
		}

		ToolStripMenuItem FindItem( ToolStripItemCollection items, String name )
		{
			for ( int i = 0; i < items.Count; i++ )
			{
				ToolStripMenuItem item = ( ToolStripMenuItem ) items[ i ];
				if ( item.Text == name )
				{
					return item;
				}
			}

			return null;
		}

		public void AddBuilder( String builderName, INodeBuilder builder )
		{
			string[] categories = builderName.Split( ':' );
			ToolStripItemCollection items = m_menu.DropDownItems;
			for ( int i = 0; i < categories.Length - 1; i++ )
			{
				String category = categories[ i ];

				ToolStripMenuItem itemCategory = FindItem( items, category );
				if ( itemCategory == null )
				{
					itemCategory = new ToolStripMenuItem();
					itemCategory.Text = category;
					items.Add( itemCategory );
				}

				items = itemCategory.DropDownItems;
			}

			String lastItemName = categories[ categories.Length - 1 ];

			ToolStripMenuItem lastItem = FindItem( items, lastItemName );
			if ( lastItem == null )
			{
				lastItem = new ToolStripMenuItem();
				lastItem.Text = lastItemName;
				items.Add( lastItem );
			}

			lastItem.Name = builderName;
			lastItem.Click += new EventHandler( contextMenuItemClick );
			
			m_nodeBuilders[ builderName ] = builder;
		}

		void contextMenuItemClick( object sender, EventArgs e )
		{
			ToolStripMenuItem item = ( ToolStripMenuItem )( sender );
			String name = item.Name;
			CreateNodeByBuilder( name );
		}

		Dictionary< String, INodeBuilder > m_nodeBuilders = new Dictionary< String, INodeBuilder >();

		public ToolStripMenuItem CreateNodeMenu
		{
			get { return m_menu; }
		}
		ToolStripMenuItem m_menu = new ToolStripMenuItem();
	}
}
