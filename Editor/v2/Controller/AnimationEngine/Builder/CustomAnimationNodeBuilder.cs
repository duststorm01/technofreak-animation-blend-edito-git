/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using System.Windows.Forms;
using System.Drawing;
using TecnoFreak.v2.View;
using TecnoFreak.v2.Controller.Interfaces;

namespace TecnoFreak.v2.Controller.AnimationEngine.Builder
{
	public class CustomAnimationNodeBuilder : INodeBuilder
	{
		IAnimable m_animable;

		public CustomAnimationNodeBuilder( IAnimable animable )
		{
			m_animable = animable;
		}

		public BaseNode CreateNode()
		{
			if ( m_animable == null )
			{
				return null;
			}

			GetNameDialog gnd = new GetNameDialog();
			gnd.StartPosition = FormStartPosition.Manual;
			gnd.Location = new Point( Form.MousePosition.X - gnd.Width / 2, Form.MousePosition.Y - gnd.Height / 2 );
			gnd.SuggestNames = m_animable.Animations.Keys;
			DialogResult result = gnd.ShowDialog();
			if ( result != DialogResult.OK )
			{
				return null;
			}

			if ( m_animable.Animations.ContainsKey( gnd.InputName ) )
			{
				// create a valid animation
				return new AnimationNode( m_animable.Animations[ gnd.InputName ] );
			}
			else
			{
				// create an invalid animation
				return new AnimationNode( gnd.InputName );
			}
		}
	}
}
