/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using System.Windows.Forms;
using System.Drawing;
using TecnoFreak.v2.View;
using TecnoFreak.v2.Controller.Interfaces;

namespace TecnoFreak.v2.Controller.AnimationEngine.Builder
{
	public class ParameterNodeBuilder : INodeBuilder
	{
		public BaseNode CreateNode()
		{
			GetNameDialog gnd = new GetNameDialog();
			gnd.StartPosition = FormStartPosition.Manual;
			gnd.Location = new Point( Form.MousePosition.X - gnd.Width / 2, Form.MousePosition.Y - gnd.Height / 2 );

			List<String> parameterNames = new List<String>();
			foreach ( String s in ParameterNode.AllParamaterNames )
			{
				if ( !s.StartsWith( "tfwp_" ) )
				{
					parameterNames.Add( s );
				}
			}
			gnd.SuggestNames = parameterNames;
			DialogResult result = gnd.ShowDialog();
			if ( result != DialogResult.OK )
			{
				return null;
			}

			ParameterNode node = new ParameterNode();
			node.ParameterName = gnd.InputName;

			return node;
		}
	}
}
