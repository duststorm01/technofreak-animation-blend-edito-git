/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using TecnoFreakCommon;

namespace TecnoFreak.v2.Controller.ModelIO
{
	public class LoadModelTransaction : Interfaces.ITransaction
	{

		public LoadModelTransaction()
		{
		}

		public LoadModelTransaction( IAnimableLoader animableLoader )
		{
			AnimableLoader = animableLoader;
			Filename = "";
		}

		public void Execute()
		{
			OperationSuccess = false;
			if ( Filename == "" )
			{
				try
				{
					Filename = GetFilename();
					if ( Filename == null )
					{
						return;
					}
				}
				catch ( Exception )
				{
					return;
				}
			}

			try
			{
				NewModelTransaction newModel = new NewModelTransaction( AnimableLoader );
				newModel.Execute();
			}
			catch ( Exception )
			{
				return;
			}

			try
			{
				AnimableLoader.LoadAnimable( Filename );
			}
			catch ( Exception e )
			{
				String message = "Unable to load model in file '" + Filename + "'";
				String details = e.Message;
				String caption = "Open Model Error";
				View.ErrorMessageBox.Show( message, details, caption );
				return;
			}

			OperationSuccess = true;

		}

		#region Operations

		private String GetFilename()
		{
			return AnimableLoader.ShowOpenFileDialog();
		}

		#endregion

		#region Attributes
		private IAnimableLoader m_animableLoader;
		private String m_filename;
		private bool m_operationSuccess;


		
		#endregion

		#region Properties
		public IAnimableLoader AnimableLoader
		{
			protected get { return m_animableLoader; }
			set { m_animableLoader = value; }
		}

		public String Filename
		{
			get { return m_filename; }
			set { m_filename = value; }
		}

		public bool OperationSuccess
		{
			get { return m_operationSuccess; }
			private set { m_operationSuccess = value; }
		}
		#endregion
	}
}
