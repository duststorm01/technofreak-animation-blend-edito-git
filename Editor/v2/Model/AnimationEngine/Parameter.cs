/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.PortControl;

namespace TecnoFreak.v2.Model.AnimationEngine
{
	public class Parameter
	{
		private String m_name;
		private List<IPortControl> m_controls;

		public String Name
		{
			get { return m_name; }
			private set { m_name = value; }
		}
		
		public List<IPortControl> Controls
		{
			get { return m_controls; }
			private set { m_controls = value; }
		}


		public Parameter( String name )
		{
			Name = name;
			m_controls = new List<IPortControl>();
		}

		public void AddControl( IPortControl portControl )
		{
			if ( Controls.Contains( portControl ) )
			{
				return;
			}

			Controls.Add( portControl );
		}

		public void RemoveControl( IPortControl portControl )
		{
			if ( ! Controls.Contains( portControl ) )
			{
				return;
			}

			Controls.Remove( portControl );
		}

		public void Update( float value )
		{
			foreach ( IPortControl portControl in Controls )
			{
				portControl.Value = value;
			}
		}

		internal void SetMin(float p)
		{
			foreach (IPortControl portControl in Controls)
			{
				portControl.MinValue = p;
			}
		}

		internal void SetMax(float p)
		{
			foreach (IPortControl portControl in Controls)
			{
				portControl.MaxValue = p;
			}
		}
	}
}
