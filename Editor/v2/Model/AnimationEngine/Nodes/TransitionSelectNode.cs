/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Windows.Forms;
using TFControls.PortControl;
using System.Xml;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{
	

	public class TransitionSelectNode : BaseNode
	{
		IPortControl m_selectPortControl;
		private int m_animationsCount;

		public TransitionSelectNode()
			: base( "Transition : Select" )
		{
			RuntimeName = "TransitionSelectNode";

			m_selectPortControl = NewDefaultUpDown(0);
			m_selectPortControl.MinValue = 0;
			m_selectPortControl.MaxValue = 0;

			CreateNumericInPort( "select", m_selectPortControl );
			CreateNumericInPort( "time in", NewDefaultUpDown() );
			CreateNumericInPort( "time out", NewDefaultUpDown() );

			CreateAnimationOutPort( "out" );

			m_animationsCount = 0;

			CreateCustomOperationsToolStrip();

			CreateTransitionSelectAnimationPort();
		}

		private void CreateCustomOperationsToolStrip()
		{
			CustomOperations = new ToolStripMenuItem();
			CustomOperations.Text = "Transition : Select";

			ToolStripMenuItem addPort = new ToolStripMenuItem( "Add Port" );
			addPort.Click += new EventHandler( addPort_Click );
			CustomOperations.DropDownItems.Add( addPort );
		}

		void addPort_Click( object sender, EventArgs e )
		{
			CreateTransitionSelectAnimationPort();
		}

		public void CreateTransitionSelectAnimationPort()
		{
			String id = "animation " + m_animationsCount;

			CreateAnimationInPort( id );

			m_selectPortControl.MaxValue = m_animationsCount - 1;
			m_animationsCount++;
		}

		#region xml loading/saving
		public override void WriteCustomXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "number_of_animation_in_ports" );
			writer.WriteValue( m_animationsCount );
			writer.WriteEndAttribute();
		}

		public override void ReadCustomXmlData( XmlReader reader )
		{
			reader.MoveToAttribute( "number_of_animation_in_ports" );
			int animationInPortCount = reader.ReadContentAsInt();

			while ( m_animationsCount < animationInPortCount )
			{
				CreateTransitionSelectAnimationPort();
			}
			
		}

		public override void WriteRuntimeXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "size" );
			writer.WriteValue( m_animationsCount );
			writer.WriteEndAttribute();
		}
		#endregion













	}

	class TransitionInfo
	{
		public TransitionInfo()
		{
		}

		private Port m_port;

		public Port Port
		{
			get { return m_port; }
			set { m_port = value; }
		}
		private bool m_in;

		public bool In
		{
			get { return m_in; }
			set { m_in = value; }
		}
		private bool m_out;

		public bool Out
		{
			get { return m_out; }
			set { m_out = value; }
		}
		private float m_remainingTimeToTransition;

		public float RemainingTimeToTransition
		{
			get { return m_remainingTimeToTransition; }
			set { m_remainingTimeToTransition = value; }
		}
		private float m_currentWeight;

		public float CurrentWeight
		{
			get { return m_currentWeight; }
			set { m_currentWeight = value; }
		}
	}
}
