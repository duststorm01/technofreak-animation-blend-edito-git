/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Drawing;
using System.Windows.Forms;
using TFControls.PortControl;
using System.Xml;
using TecnoFreak.v2.View;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{
	public class ParameterNode : BaseNode
	{
		private static Dictionary<String, Parameter> ms_parameters = new Dictionary<String, Parameter>();

		public static ICollection<String> AllParamaterNames
		{
			get
			{
				List<String> scheduledDelete = new List<String>();
				foreach ( Parameter parameter in ms_parameters.Values )
				{
					if ( parameter.Controls.Count == 0 )
					{
						scheduledDelete.Add( parameter.Name );
					}
				}
				foreach ( String parameterName in scheduledDelete )
				{
					ms_parameters.Remove( parameterName );
				}
				//return parameterNames;
				return ms_parameters.Keys;
			}
		}

		public static float GetParameterValue( String parameterName )
		{
			return ms_parameters[ parameterName ].Controls[ 0 ].Value;
		}

		public static void SetParameterValue( string parameterName, float p )
		{
			if ( !ms_parameters.ContainsKey( parameterName ) )
			{
				return;
			}
			ms_parameters[ parameterName ].Controls[ 0 ].Value = p;
		}

		private IPortControl m_valuePortControl;
		private String m_parameterName;
		public String ParameterName
		{
			get { return m_parameterName; }
			
			set 
			{

				if ( value == ParameterName )
				{
					return;
				}

				if ( ParameterName != null && ms_parameters.ContainsKey( ParameterName ) )
				{
					Parameter oldParameter = ms_parameters[ ParameterName ];
					oldParameter.RemoveControl( m_valuePortControl );
				}

				m_parameterName = value;
				Name = "Parameter : " + ParameterName;
				if ( CustomOperations != null )
				{
					CustomOperations.Text = Name;
				}

				if ( ParameterName == "" )
				{
					return;
				}

				if ( !ms_parameters.ContainsKey( ParameterName ) )
				{
					ms_parameters[ ParameterName ] = new Parameter( ParameterName );
				}

				Parameter newParameter = ms_parameters[ ParameterName ];
				newParameter.AddControl( m_valuePortControl );
				m_valuePortControl.Value = newParameter.Controls[ 0 ].Value;
				m_valuePortControl.MinValue = newParameter.Controls[ 0 ].MinValue;
				m_valuePortControl.MaxValue = newParameter.Controls[ 0 ].MaxValue;
			}
		}

		public ParameterNode()
			: base( "Parameter : " )
		{
			RuntimeName = "ParameterNode";

			Port valuePort = CreateNumericOutPort( "value" );
			valuePort.ValueControl = new PortTrackBar();
			m_valuePortControl = valuePort.ValueControl;

			m_valuePortControl.ValueChanged += new EventHandler( OnValueChanged );

			ParameterName = "Default";

			Deleted += new NodeEventHandler( ParameterNode_Deleted );

			CreateCustomOperationsToolStrip();

			SetColours();
		}

		private void CreateCustomOperationsToolStrip()
		{
			CustomOperations = new ToolStripMenuItem();
			CustomOperations.Text = this.Name;

			ToolStripMenuItem setEditorLimits = new ToolStripMenuItem( "Set Control Properties" );

			setEditorLimits.Click += new EventHandler(setEditorLimits_Click);
			CustomOperations.DropDownItems.Add(setEditorLimits);
		}

		class ControlProperties
		{
			float m_min;

			public float Min
			{
				get { return m_min; }
				set { m_min = value; }
			}
			float m_max;

			public float Max
			{
				get { return m_max; }
				set { m_max = value; }
			}
		}

		void setEditorLimits_Click(object sender, EventArgs e)
		{
			ControlProperties p = new ControlProperties();
			p.Min = m_valuePortControl.MinValue;
			p.Max = m_valuePortControl.MaxValue;

			PropertyControl pc = new PropertyControl(p);
			DialogResult r = pc.ShowDialog();
			if ( r != DialogResult.OK )
			{
				return;
			}

			Parameter parameter = ms_parameters[ParameterName];
			parameter.SetMin(p.Min);
			parameter.SetMax(p.Max);
		}

		void ParameterNode_Deleted( object sender, EventArgs e )
		{
			ParameterName = "";
		}

		public void OnValueChanged( object sender, EventArgs e )
		{
			Parameter parameter = ms_parameters[ ParameterName ];

			parameter.Update( m_valuePortControl.Value );
		}

		private void SetColours()
		{

			NiceNodeRenderer.TitleColor1 = Color.MintCream;
			NiceNodeRenderer.TitleColor2 = Color.MediumAquamarine;

			NiceNodeRenderer.BodyColor1 = Color.Honeydew;
			NiceNodeRenderer.BodyColor2 = Color.White;

			NiceNodeRenderer.TitleTextBrush = Brushes.Black;
		}

		public float GetValue()
		{
			return m_valuePortControl.Value;
		}

		public override void WriteCustomXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "parameter" );
			writer.WriteValue( ParameterName );
			writer.WriteEndAttribute();
		}

		public override void ReadCustomXmlData( XmlReader reader )
		{
			reader.MoveToAttribute( "parameter" );

			if ( reader.HasValue )
			{
				ParameterName = reader.Value;
			}
		}

		public override void WriteRuntimeXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "parameter" );
			writer.WriteValue( ParameterName );
			writer.WriteEndAttribute();
		}




	}
}
