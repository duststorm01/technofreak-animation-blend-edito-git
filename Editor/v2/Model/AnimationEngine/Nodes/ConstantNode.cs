/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using TFControls.Diagram;
using TFControls.PortControl;
using System.Xml;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{
	public class ConstantNode : BaseNode
	{
		IPortControl m_valueControl;

		public ConstantNode()
			: base( "Constant" )
		{
			Port valuePort = CreateNumericOutPort( "value" );

			valuePort.ValueControl = NewDefaultUpDown( 2 );
			m_valueControl = valuePort.ValueControl;

			SetColours();
		}

		public ConstantNode( String name, float value )
			: base( "Constant : " + name )
		{
			Port valuePort = CreateNumericOutPort( "value" );

			valuePort.ValueControl = NewDefaultUpDown( 2, value );
			valuePort.ValueControl.Control.Enabled = false;

			m_valueControl = valuePort.ValueControl;

			SetColours();
		}

		void SetColours()
		{
			NiceNodeRenderer.TitleColor1 = Color.AntiqueWhite;
			NiceNodeRenderer.TitleColor2 = Color.MediumAquamarine;

			NiceNodeRenderer.BodyColor1 = Color.Honeydew;
			NiceNodeRenderer.BodyColor2 = Color.AntiqueWhite;

			NiceNodeRenderer.TitleTextBrush = Brushes.Black;
		}

		public float GetValue()
		{
			return m_valueControl.Value;
		}

		#region xml loading/saving
		public override void WriteCustomXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "editable" );
			writer.WriteValue( GetPort( "value" ).ValueControl.Control.Enabled );
			writer.WriteEndAttribute();
		}

		public override void ReadCustomXmlData( XmlReader reader )
		{
			reader.MoveToAttribute( "editable" );
			bool editableValue = reader.ReadContentAsBoolean();

			if ( !editableValue )
			{
				GetPort( "value" ).ValueControl.Control.Enabled = false;
			}
		}
		#endregion
	}
}
