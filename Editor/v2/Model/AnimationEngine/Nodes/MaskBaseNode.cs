/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Windows.Forms;
using System.Xml;
using TecnoFreak.v2.View;
using TecnoFreakCommon;
using System.Diagnostics;
using TecnoFreak.v2.Controller.Interfaces;


namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{
	public class MaskBaseNode : BaseNode
	{
		private float[] m_boneWeights;

		public MaskBaseNode( String name )
			: base( name )
		{
			Debug.Assert( ms_viewport != null );
			Debug.Assert( ms_animableLoader != null );

			CreateAnimationOutPort( "out" );
			CreateAnimationInPort( "in" );

			SelectedChanged += new NodeEventHandler( MaskBaseNode_SelectedChanged );
			ms_animableLoader.AnimableChanged += new EventHandler( MaskBaseNode_AnimableChanged );
		}


		void UpdateStoredBoneWeights()
		{
			int boneCount = ms_viewport.GetBoneCount();
			if ( boneCount == 0 )
			{
				return;
			}

			if ( m_boneWeights == null || m_boneWeights.Length != boneCount )
			{
				m_boneWeights = new float[ boneCount ];
			}

			bool storedDataChanged = false;

			for ( int i = 0; i < boneCount; ++i )
			{
				float boneWeight = ms_viewport.GetBoneDisplayWeightByIndex( i );
				if ( Math.Abs( m_boneWeights[ i ] - boneWeight ) > 0.01f )
				{
					m_boneWeights[ i ] = boneWeight;
					storedDataChanged = true;
				}
				
			}

			if ( storedDataChanged )
			{
				CustomDataChanged();
			}
		}

		void UpdateDisplayBoneWeights()
		{
			int boneCount = ms_viewport.GetBoneCount();
			if ( boneCount == 0 )
			{
				return;
			}

			if ( m_boneWeights == null || m_boneWeights.Length != boneCount )
			{
				m_boneWeights = new float[ boneCount ];
				for ( int i = 0; i < boneCount; ++i )
				{
					m_boneWeights[ i ] = 1.0f;
				}
			}

			for ( int i = 0; i < m_boneWeights.Length; ++i )
			{
				float weight = m_boneWeights[ i ];
				ms_viewport.SetBoneDisplayWeightByIndex( i, weight );
			}
		}

		void SetSelectedBoneWeights( float weight )
		{
			int boneCount = ms_viewport.GetBoneCount();

			if ( boneCount != m_boneWeights.Length )
			{
				return;
			}

			for ( int i = 0; i < m_boneWeights.Length; ++i )
			{
				bool selected = ms_viewport.IsBoneSelectedByIndex( i );
				if ( selected )
				{
					ms_viewport.SetBoneDisplayWeightByIndex( i, weight );
				}
			}

			UpdateStoredBoneWeights();
		}

		void MaskBaseNode_AnimableChanged( object sender, EventArgs e )
		{
			Selected = false;
		}

		void MaskBaseNode_SelectedChanged( object sender, EventArgs e )
		{
			bool nodeSelected = Selected;
			ms_viewport.SetMouseBoneSelectionMode( nodeSelected );

			if ( nodeSelected )
			{
				UpdateDisplayBoneWeights();
				ms_boneWeightController.SelectedBoneWeightValueChanged += new EventHandler( BoneWeightController_SelectedBoneWeightValueChanged );
			}
			else
			{
				ms_viewport.ResetBoneDisplayWeights();
				ms_boneWeightController.SelectedBoneWeightValueChanged -= new EventHandler( BoneWeightController_SelectedBoneWeightValueChanged );
			}
		}

		void BoneWeightController_SelectedBoneWeightValueChanged( object sender, EventArgs e )
		{
			float weight = ms_boneWeightController.GetWeight();
			SetSelectedBoneWeights( weight );
		}

		void MaskBaseNode_BoneSelectionChanged( object sender, EventArgs e )
		{
			// TODO: why did I need this again??!?!?
			// I probably need bone weight changed, but that event should come from UI!!!!
		}


		// TODO: Bone selection changed event on viewport to update stored weight values

		static IViewport ms_viewport;
		public static IViewport Viewport
		{
			set
			{
				Debug.Assert( value != null );
				ms_viewport = value;
			}
		}

		static IAnimableLoader ms_animableLoader;
		public static IAnimableLoader AnimableLoader
		{
			set
			{
				Debug.Assert( value != null );
				ms_animableLoader = value;
			}
		}

		static ISelectedBoneWeightController ms_boneWeightController;
		public static ISelectedBoneWeightController BoneWeightController
		{
			set
			{
				Debug.Assert( value != null );
				ms_boneWeightController = value;
			}
		}

		#region xml loading/saving
		public override void WriteCustomXmlData( XmlWriter writer )
		{
			if ( m_boneWeights != null )
			{
				writer.WriteStartAttribute( "number_of_bones" );
				writer.WriteValue( m_boneWeights.Length );
				writer.WriteEndAttribute();
				for ( int i = 0; i < m_boneWeights.Length; i++ )
				{
					writer.WriteStartAttribute( "bone_" + i );
					writer.WriteValue( m_boneWeights[ i ] );
					writer.WriteEndAttribute();
				}
			}
			else
			{
				writer.WriteStartAttribute( "number_of_bones" );
				writer.WriteValue( 0 );
				writer.WriteEndAttribute();
			}
		}

		public override void ReadCustomXmlData( XmlReader reader )
		{
			int numBones = 0;
			reader.MoveToAttribute( "number_of_bones" );
			numBones = reader.ReadContentAsInt();

			if ( numBones > 0 )
			{
				m_boneWeights = new float[ numBones ];
				for ( int i = 0; i < numBones; i++ )
				{
					reader.MoveToAttribute( "bone_" + i );
					m_boneWeights[ i ] = reader.ReadContentAsFloat();
				}
			}
		}

		public override void WriteRuntimeXmlData( XmlWriter writer )
		{
			if ( m_boneWeights == null )
			{
				return;
			}

			writer.WriteStartElement( "bones" );
			for ( int i = 0; i < m_boneWeights.Length; i++ )
			{
				writer.WriteStartElement( "bone" );
				writer.WriteValue( m_boneWeights[ i ] );
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}
		#endregion
	}
}
