/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Windows.Forms;
using System.Xml;
using TecnoFreak.v2.View;
using TecnoFreakCommon;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{
	public class MaskMulNode : MaskBaseNode
	{
		public MaskMulNode()
			: base( "MaskMul" )
		{
			RuntimeName = "MaskMulNode";
		}
	}
}
