/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Windows.Forms;
using System.Drawing;
using TFControls.PortControl;
using System.Xml;
using TecnoFreakCommon;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{

	public enum PortDataType
	{
		Animation,
		Number
	}

	public class BaseNode : Node
	{
		public BaseNode( String name )
			: base( name )
		{
		}

		[NonSerialized]
		private ToolStripMenuItem m_customOperations;

		public ToolStripMenuItem CustomOperations
		{
			get { return m_customOperations; }
			protected set { m_customOperations = value; }
		}

		private IAnimable m_animable;

		public IAnimable Animable
		{
			get { return m_animable; }
			set { m_animable = value; }
		}

		private String m_runtimeName;

		public String RuntimeName
		{
			get { return m_runtimeName; }
			set { m_runtimeName = value; }
		}

		protected NiceNodeRenderer NiceNodeRenderer
		{
			get { return ( NiceNodeRenderer ) NodeRenderer; }
		}

		protected void initUpDown( NumericUpDown control )
		{
			control.Maximum = decimal.MaxValue;
			control.Minimum = decimal.MinValue;
			control.DecimalPlaces = 1;
			control.Increment = 0.1M;
			control.Size = new Size( 45, 20 );
		}

		protected PortNumericUpDown NewDefaultUpDown()
		{
			PortNumericUpDown u = new PortNumericUpDown();
			initUpDown( u.NumericUpDownControl );

			return u;
		}

		protected PortNumericUpDown NewDefaultUpDown( uint decimalPlaces )
		{
			PortNumericUpDown u = new PortNumericUpDown();
			initUpDown( u.NumericUpDownControl );
			u.SetDecimalPlaces( decimalPlaces );

			return u;
		}

		protected PortNumericUpDown NewDefaultUpDown( uint decimalPlaces, float value )
		{
			PortNumericUpDown u = NewDefaultUpDown( decimalPlaces );
			u.NumericUpDownControl.Value = ( decimal ) value;

			return u;
		}

		protected Port CreateNumericInPort( String name, IPortControl valueControl )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.In, PortDataType.Number );
			p.ValueControl = valueControl;
			return p;
		}

		protected Port CreateNumericOutPort( String name )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.Out, PortDataType.Number );
			return p;
		}

		protected Port CreateAnimationInPort( String name )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.In, PortDataType.Animation );
			return p;
		}

		protected Port CreateAnimationOutPort( String name )
		{
			Port p = CreatePort( name, TFControls.Diagram.PortType.Out, PortDataType.Animation );
			return p;
		}



		// write custom data to xml node structure.
		// Anything that will be needed to reconstruct a node from the state that the
		// default constructor leaves it in should go here.
		public virtual void WriteCustomXmlData( XmlWriter writer )
		{
		}

		// read custom data from xml node structure
		// Suppose node is built from it's default constructor and port values haven't
		// been initialised.
		public virtual void ReadCustomXmlData( XmlReader reader )
		{
		}

		// Write data to the custom node xml element for the runtime to load.
		// The custom element is already created and can be directly written to.
		public virtual void WriteRuntimeXmlData( XmlWriter writer )
		{
		}
	}
}

