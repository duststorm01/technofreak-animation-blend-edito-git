/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using TFControls.Diagram;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode
{
	public class MapNode : MathNode
	{
		public MapNode()
			: base( "Map" )
		{
			RuntimeName = "MathMapNode";

			CreateNumericInPort( "min_in", NewDefaultUpDown( 1, 0 ) );
			CreateNumericInPort( "max_in", NewDefaultUpDown( 1, 1) );
			CreateNumericInPort( "min_out", NewDefaultUpDown( 1, 0) );
			CreateNumericInPort( "max_out", NewDefaultUpDown( 1, 1 ) );
			CreateNumericInPort( "value", NewDefaultUpDown( 1, 0 ) );

			CreateNumericOutPort( "out" );
		}

	}
}