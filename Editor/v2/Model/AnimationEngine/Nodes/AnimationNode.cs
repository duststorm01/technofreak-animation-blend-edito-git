/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreakCommon;
using TFControls.Diagram;
using System.Drawing;
using System.Windows.Forms;
using TFControls.PortControl;
using System.Xml;
using System.Diagnostics;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{
	public class AnimationNode : BaseNode
	{

		// This node refers to an invalid animation for the current model
		public AnimationNode()
			: base( "Undefined" )
		{
			RuntimeName = "AnimationNode";
			CreateNumericInPort( "speed", NewDefaultUpDown( 2, 1 ) );
			CreateAnimationInPort( "synch" );
			CreateAnimationOutPort( "out" );
			SetUndefinedColours();

			AnimableLoader.AnimableChanged += new EventHandler(AnimableLoader_AnimableChanged);
		}

		void AnimableLoader_AnimableChanged(object sender, EventArgs e)
		{
			if ( AnimableLoader == null )
			{
				return;
			}
			if ( AnimableLoader.GetAnimable() == null )
			{
				return;
			}

			IAnimable animable = AnimableLoader.GetAnimable();
			if ( animable.Animations.ContainsKey( Name ) )
			{
				SetColours();
			}
			else
			{
				SetUndefinedColours();
			}
			NodeView.NeedsUpdate = true;
		}

		// This node refers to an invalid animation for the current model
		public AnimationNode( String name )
			: base( name )
		{
			RuntimeName = "AnimationNode";
			CreateNumericInPort( "speed", NewDefaultUpDown( 2, 1 ) );
			CreateAnimationInPort("synch");
			CreateAnimationOutPort( "out" );
			SetUndefinedColours();

			AnimableLoader.AnimableChanged += new EventHandler(AnimableLoader_AnimableChanged);
		}

		public AnimationNode( IAnimation animation )
			: base( animation.Name )
		{
			RuntimeName = "AnimationNode";
			//Animation = animation;

			CreateNumericInPort( "speed", NewDefaultUpDown( 2, 1 ) );
			CreateAnimationInPort( "synch" );
			CreateAnimationOutPort( "out" );
			SetColours();

			AnimableLoader.AnimableChanged += new EventHandler(AnimableLoader_AnimableChanged);
		}

		private void SetColours()
		{

			NiceNodeRenderer.BodyColor1 = Color.Orange;
			NiceNodeRenderer.BodyColor2 = Color.White;

			NiceNodeRenderer.TitleColor1 = Color.Bisque;
			NiceNodeRenderer.TitleColor2 = Color.DarkOrange;

			NiceNodeRenderer.TitleTextBrush = Brushes.Black;
		}

		private void SetUndefinedColours()
		{
			SetColours();
			NiceNodeRenderer.BodyColor1 = Color.Red;
			NiceNodeRenderer.BodyColor2 = Color.Orange;

			NiceNodeRenderer.TitleColor1 = Color.DarkOrange;
			NiceNodeRenderer.TitleColor2 = Color.DarkRed;

			NiceNodeRenderer.TitleTextBrush = Brushes.White;
		}

		static IAnimableLoader ms_animableLoader;
		public static IAnimableLoader AnimableLoader
		{
			get { return ms_animableLoader; }
			set 
			{
				ms_animableLoader = value;
			}
		}

		public String AnimationName
		{
			get { return Name; }
		}

		public override void WriteRuntimeXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "animation" );
			writer.WriteValue( AnimationName );
			writer.WriteEndAttribute();
		}

	}
}
