/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TFControls.Diagram;
using System.Windows.Forms;
using System.Xml;

namespace TecnoFreak.v2.Model.AnimationEngine.Nodes
{

	public class MixNode : BaseNode
	{

		public MixNode()
			: base( "Mix" )
		{
			RuntimeName = "MixNode";

			CreateAnimationOutPort( "out" );
			CreateCustomOperationsToolStrip();
		}

		public MixNode( int size )
			: base( "Mix" )
		{
			RuntimeName = "MixNode";

			CreateAnimationOutPort( "out" );

			for ( int i = 0; i < size; i++ )
			{
				CreateMixInPort();
			}

			CreateCustomOperationsToolStrip();
		}

		private void CreateCustomOperationsToolStrip()
		{
			CustomOperations = new ToolStripMenuItem();
			CustomOperations.Text = this.Name;

			ToolStripMenuItem addPort = new ToolStripMenuItem( "Add Port" );
			addPort.Click += new EventHandler( addPort_Click );
			CustomOperations.DropDownItems.Add( addPort );
		}

		void addPort_Click( object sender, EventArgs e )
		{
			CreateMixInPort();
		}

		private void CreateMixInPort()
		{
			CreateAnimationInPort( "in" + InPorts.Count );
		}

		#region xml loading/saving
		public override void WriteCustomXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "number_of_mix_in_ports" );
			writer.WriteValue( InPorts.Count );
			writer.WriteEndAttribute();
		}

		public override void ReadCustomXmlData( XmlReader reader )
		{
			reader.MoveToAttribute( "number_of_mix_in_ports" );
			int mixInPortCount = reader.ReadContentAsInt();

			for ( int i = 0; i < mixInPortCount; i++ )
			{
				CreateMixInPort();
			}
		}

		public override void WriteRuntimeXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "size" );
			writer.WriteValue( InPorts.Count );
			writer.WriteEndAttribute();
		}
		#endregion


	}
}
