/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;

namespace TecnoFreak.v2.Model.AnimationEngine
{
	[Serializable]
	public class ConnectionValidator : TFControls.Diagram.ConnectorValidator
	{
		public override bool ValidConnection( TFControls.Diagram.Port A, TFControls.Diagram.Port B )
		{
			
			bool valid = base.ValidConnection( A, B );

			if ( !valid )
			{
				return false;
			}

			PortDataType At = ( PortDataType ) A.UserData;
			PortDataType Bt = ( PortDataType ) B.UserData;

			if ( At != Bt )
			{
				return false;
			}

			if ( Loop( A, B ) )
			{
				return false;
			}

			return true;
		}

		public bool Loop( TFControls.Diagram.Port A, TFControls.Diagram.Port B )
		{
			PortDataType At = ( PortDataType ) A.UserData;
			PortDataType Bt = ( PortDataType ) B.UserData;

			if ( A.Type == TFControls.Diagram.PortType.Out )
			{
				return LoopRec( B, A.Parent, At );
			}

			if ( B.Type == TFControls.Diagram.PortType.Out )
			{
				return LoopRec( A, B.Parent, Bt );
			}

			return false;
		}

		public bool LoopRec( TFControls.Diagram.Port port, TFControls.Diagram.Node startNode, PortDataType type )
		{
			TFControls.Diagram.Node node = port.Parent;

			if ( port.Type == TFControls.Diagram.PortType.In )
			{
				if ( node == startNode )
				{
					return true;
				}
			}


			foreach ( TFControls.Diagram.Port outPort in node.OutPorts.Values )
			{
				if ( ( PortDataType ) outPort.UserData == type )
				{
					foreach ( TFControls.Diagram.Port connectedInPort in outPort.Connections )
					{
						bool loop = LoopRec( connectedInPort, startNode, type );
						if ( loop )
						{
							return true;
						}
					}
				}
			}

			return false;

		}
	}
}
