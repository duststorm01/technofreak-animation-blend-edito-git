/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using TecnoFreak.v2.Controller.ModelIO;
using TecnoFreakCommon;
using TecnoFreak.v2.Controller.DiagramIO;

namespace TecnoFreak.v2.Model
{
	public delegate void ProjectEventHandler( object sender, EventArgs e );

	public class Project
	{

		public event ProjectEventHandler ProjectChanged;
		public event EventHandler ProjectHasChanges;

		public Project( IAnimableLoader animableLoader )
		{
			AnimableLoader = animableLoader;
			ActiveModelPath = "";

			ModelsPath.Clear();
			AnimableLoader.ClearAnimable();
			HasBeenSaved = false;
			HasChanged = false;
			HasChanges = false;
		}

		#region Attributes
		String m_name;
		String m_location;
		String m_filename;
		String m_activeModelPath;
		List< String > m_modelsPath = new List< String >();
		IAnimableLoader m_animableLoader;

		IDiagramIO m_activeDiagram;
		String m_activeDiagramPath;

		bool m_hasBeenSaved;
		bool m_hasChanged;
		bool m_hasChanges;
		#endregion

		#region Properties

		public event EventHandler ProjectSaved;

		public System.String Name
		{
			get { return m_name; }
			private set { m_name = value; }
		}
		public System.String Location
		{
			get { return m_location; }
			private set { m_location = value; }
		}
		public String Filename
		{
			get { return m_filename; }
			set { m_filename = value; }
		}
		public List<String> ModelsPath
		{
			get { return m_modelsPath; }
			private set { m_modelsPath = value; }
		}
		public IAnimableLoader AnimableLoader
		{
			get { return m_animableLoader; }
			private set { m_animableLoader = value; }
		}


		public IDiagramIO ActiveDiagram
		{
			get { return m_activeDiagram; }
			set 
			{
				m_activeDiagram = value;
				ActiveDiagram.Changed += new EventHandler( ActiveDiagram_Changed );
			}
		}

		public String ActiveDiagramPath
		{
			get { return m_activeDiagramPath; }
			private set { m_activeDiagramPath = value; }
		}

		public String ActiveModelPath
		{
			get { return m_activeModelPath; }
			private set { m_activeModelPath = value; }
		}
		public bool HasBeenSaved
		{
			get { return m_hasBeenSaved; }
			private set { m_hasBeenSaved = value; }
		}
		public bool HasChanged
		{
			get { return m_hasChanged; }
			private set 
			{
				m_hasChanged = value;
				if ( HasChanged )
				{
					if ( ProjectChanged != null )
					{
						ProjectChanged( this, new EventArgs() );
					}
					if ( ProjectHasChanges != null )
					{
						ProjectHasChanges( this, new EventArgs() );
					}
				}
			}
		}

		public bool HasChanges
		{
			get { return m_hasChanges || m_hasChanged; }
			set { m_hasChanges = value; }
		}
		#endregion

		public void New( String location, String name )
		{
			Location = location;
			Name = name;

			ActiveModelPath = "";
			ModelsPath.Clear();
			AnimableLoader.ClearAnimable();
			HasBeenSaved = false;
			HasChanged = true;
			HasChanges = false;

			// TODO: Real multiple diagram support.
			ActiveDiagramPath = Location + Path.DirectorySeparatorChar + Name + Path.DirectorySeparatorChar + "diagram.xml";
		}

		public void AddModel( String filename )
		{
			if ( ModelsPath.Contains( filename ) )
			{
				return;
			}

			ModelsPath.Add( filename );
			HasChanged = true;
		}

		public void RemoveModel( string filename )
		{
			if ( ! ModelsPath.Contains( filename ) )
			{
				return;
			}

			if ( IsActiveModel( filename) )
			{
				new NewModelTransaction( AnimableLoader ).Execute();
				ActiveModelPath = "";
			}

			ModelsPath.Remove( filename );
			HasChanged = true;
		}


		public void MarkAsActive( String activeModelPath )
		{
			ActiveModelPath = activeModelPath;
			HasChanged = true;
		}

		public bool IsActiveModel( String model )
		{
			bool activeModel = Path.Equals( model, ActiveModelPath );
			return activeModel;
		}

		public void MarkSaved()
		{
			HasChanged = false;
			HasChanges = false;
			if ( ProjectSaved != null )
			{
				ProjectSaved( this, new EventArgs() );
			}
		}

		void ActiveDiagram_Changed( object sender, EventArgs e )
		{
			HasChanges = true;
			if ( ProjectHasChanges != null )
			{
				ProjectHasChanges( this, new EventArgs() );
			}
		}

		
	}
}
