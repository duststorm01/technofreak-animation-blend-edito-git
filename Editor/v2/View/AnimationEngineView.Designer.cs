namespace TecnoFreak.v2.View
{
	partial class AnimationEngineView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AnimationEngineView ) );
			this.DiagramControl = new TFControls.Diagram.DiagramControl();
			this.contextMenu = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.contextMenu_DeleteNode = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.contextMenu_CreateNode = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// DiagramControl
			// 
			this.DiagramControl.AutoScroll = true;
			this.DiagramControl.ConnectingLocation = new System.Drawing.Point( 0, 0 );
			this.DiagramControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DiagramControl.ForceUpdate = false;
			this.DiagramControl.Location = new System.Drawing.Point( 0, 0 );
			this.DiagramControl.Name = "DiagramControl";
			this.DiagramControl.Size = new System.Drawing.Size( 618, 566 );
			this.DiagramControl.TabIndex = 0;
			// 
			// contextMenu
			// 
			this.contextMenu.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.contextMenu_CreateNode,
            this.contextMenu_DeleteNode,
            this.toolStripSeparator1} );
			this.contextMenu.Name = "contextMenu";
			this.contextMenu.Size = new System.Drawing.Size( 153, 76 );
			this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler( this.contextMenu_Opening );
			this.contextMenu.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler( this.contextMenu_Closing );
			// 
			// contextMenu_DeleteNode
			// 
			this.contextMenu_DeleteNode.Name = "contextMenu_DeleteNode";
			this.contextMenu_DeleteNode.Size = new System.Drawing.Size( 152, 22 );
			this.contextMenu_DeleteNode.Text = "DeleteNode";
			this.contextMenu_DeleteNode.Click += new System.EventHandler( this.contextMenu_DeleteNode_Click );
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size( 149, 6 );
			// 
			// contextMenu_CreateNode
			// 
			this.contextMenu_CreateNode.Name = "contextMenu_CreateNode";
			this.contextMenu_CreateNode.Size = new System.Drawing.Size( 152, 22 );
			this.contextMenu_CreateNode.Text = "Create Node";
			// 
			// AnimationEngineView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 618, 566 );
			this.Controls.Add( this.DiagramControl );
			this.HideOnClose = true;
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.Name = "AnimationEngineView";
			this.TabText = "Animation Engine";
			this.Text = "Animation Engine";
			this.contextMenu.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion

		private TFControls.Diagram.DiagramControl DiagramControl;
		private System.Windows.Forms.ContextMenuStrip contextMenu;
		private System.Windows.Forms.ToolStripMenuItem contextMenu_DeleteNode;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem contextMenu_CreateNode;
	}
}