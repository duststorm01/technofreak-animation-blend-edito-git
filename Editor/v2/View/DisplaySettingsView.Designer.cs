namespace TecnoFreak.v2.View
{
	partial class DisplaySettingsView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ViewportBackgroundColorButton = new TFControls.Buttons.ColorButton();
			this.MainLayout = new System.Windows.Forms.FlowLayoutPanel();
			this.BackgroundColourLayout = new System.Windows.Forms.FlowLayoutPanel();
			this.ViewportBackgroundColourLabel = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.MainLayout.SuspendLayout();
			this.BackgroundColourLayout.SuspendLayout();
			this.SuspendLayout();
			// 
			// ViewportBackgroundColorButton
			// 
			this.ViewportBackgroundColorButton.BackColor = System.Drawing.Color.Gray;
			this.ViewportBackgroundColorButton.Color = System.Drawing.Color.Gray;
			this.ViewportBackgroundColorButton.Location = new System.Drawing.Point( 107, 3 );
			this.ViewportBackgroundColorButton.Name = "ViewportBackgroundColorButton";
			this.ViewportBackgroundColorButton.Size = new System.Drawing.Size( 32, 32 );
			this.ViewportBackgroundColorButton.TabIndex = 0;
			this.ViewportBackgroundColorButton.UseVisualStyleBackColor = false;
			// 
			// MainLayout
			// 
			this.MainLayout.AutoScroll = true;
			this.MainLayout.Controls.Add( this.BackgroundColourLayout );
			this.MainLayout.Controls.Add( this.comboBox1 );
			this.MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.MainLayout.Location = new System.Drawing.Point( 0, 0 );
			this.MainLayout.Name = "MainLayout";
			this.MainLayout.Size = new System.Drawing.Size( 149, 245 );
			this.MainLayout.TabIndex = 1;
			// 
			// BackgroundColourLayout
			// 
			this.BackgroundColourLayout.AutoSize = true;
			this.BackgroundColourLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BackgroundColourLayout.Controls.Add( this.ViewportBackgroundColourLabel );
			this.BackgroundColourLayout.Controls.Add( this.ViewportBackgroundColorButton );
			this.BackgroundColourLayout.Location = new System.Drawing.Point( 3, 3 );
			this.BackgroundColourLayout.Name = "BackgroundColourLayout";
			this.BackgroundColourLayout.Size = new System.Drawing.Size( 142, 38 );
			this.BackgroundColourLayout.TabIndex = 2;
			// 
			// ViewportBackgroundColourLabel
			// 
			this.ViewportBackgroundColourLabel.AutoSize = true;
			this.ViewportBackgroundColourLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ViewportBackgroundColourLabel.Location = new System.Drawing.Point( 3, 0 );
			this.ViewportBackgroundColourLabel.Name = "ViewportBackgroundColourLabel";
			this.ViewportBackgroundColourLabel.Size = new System.Drawing.Size( 98, 38 );
			this.ViewportBackgroundColourLabel.TabIndex = 3;
			this.ViewportBackgroundColourLabel.Text = "Background Colour";
			this.ViewportBackgroundColourLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point( 3, 47 );
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size( 58, 21 );
			this.comboBox1.TabIndex = 3;
			// 
			// DisplaySettingsView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 149, 245 );
			this.Controls.Add( this.MainLayout );
			this.HideOnClose = true;
			this.Name = "DisplaySettingsView";
			this.TabText = "Display Settings";
			this.Text = "Display Settings";
			this.MainLayout.ResumeLayout( false );
			this.MainLayout.PerformLayout();
			this.BackgroundColourLayout.ResumeLayout( false );
			this.BackgroundColourLayout.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion

		private TFControls.Buttons.ColorButton ViewportBackgroundColorButton;
		private System.Windows.Forms.FlowLayoutPanel MainLayout;
		private System.Windows.Forms.FlowLayoutPanel BackgroundColourLayout;
		private System.Windows.Forms.Label ViewportBackgroundColourLabel;
		private System.Windows.Forms.ComboBox comboBox1;
	}
}