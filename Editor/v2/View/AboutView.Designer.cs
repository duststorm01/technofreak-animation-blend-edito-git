namespace TecnoFreak.v2.View
{
	partial class AboutView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AboutView ) );
			this.buildLabel = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.Email_Author = new System.Windows.Forms.LinkLabel();
			( ( System.ComponentModel.ISupportInitialize ) ( this.pictureBox1 ) ).BeginInit();
			this.SuspendLayout();
			// 
			// buildLabel
			// 
			this.buildLabel.AutoSize = true;
			this.buildLabel.BackColor = System.Drawing.Color.White;
			this.buildLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
			this.buildLabel.Location = new System.Drawing.Point( 27, 258 );
			this.buildLabel.Name = "buildLabel";
			this.buildLabel.Size = new System.Drawing.Size( 42, 13 );
			this.buildLabel.TabIndex = 1;
			this.buildLabel.Text = "Version";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ( ( System.Drawing.Image ) ( resources.GetObject( "pictureBox1.Image" ) ) );
			this.pictureBox1.InitialImage = null;
			this.pictureBox1.Location = new System.Drawing.Point( 0, 0 );
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size( 300, 300 );
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// Email_Author
			// 
			this.Email_Author.AutoSize = true;
			this.Email_Author.BackColor = System.Drawing.Color.White;
			this.Email_Author.Location = new System.Drawing.Point( 177, 258 );
			this.Email_Author.Name = "Email_Author";
			this.Email_Author.Size = new System.Drawing.Size( 101, 13 );
			this.Email_Author.TabIndex = 2;
			this.Email_Author.TabStop = true;
			this.Email_Author.Text = "pnovau@gmail.com";
			this.Email_Author.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.Email_Author.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler( this.Email_Author_LinkClicked );
			// 
			// AboutView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 300, 300 );
			this.Controls.Add( this.Email_Author );
			this.Controls.Add( this.buildLabel );
			this.Controls.Add( this.pictureBox1 );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ( ( System.Drawing.Icon ) ( resources.GetObject( "$this.Icon" ) ) );
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutView";
			this.ShowInTaskbar = false;
			this.Text = "WTF?";
			( ( System.ComponentModel.ISupportInitialize ) ( this.pictureBox1 ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label buildLabel;
		private System.Windows.Forms.LinkLabel Email_Author;
	}
}