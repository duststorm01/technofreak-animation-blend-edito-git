/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TecnoFreak.v2.Model;
using System.IO;
using TecnoFreak.v2.Controller.Project;

namespace TecnoFreak.v2.View
{
	public partial class ProjectExplorerView : DockableView
	{
		private int PROJECT_ICON;
		private int MODELS_ICON;
		private int MODEL_ICON;
		private int ACTIVEMODEL_ICON;
		private int DIAGRAMS_ICON;
		private int ANIMATIONDIAGRAM_ICON;
		private int ACTIVEANIMATIONDIAGRAM_ICON;

		public ProjectExplorerView( Project project )
		{
			InitializeComponent();

			Project = project;

			project.ProjectChanged += new ProjectEventHandler( OnProjectChanged );

			ProjectExplorerTreeView.ImageList = new ImageList();
			ProjectExplorerTreeView.ImageList.Images.Add( Properties.Resources.icon );
			ProjectExplorerTreeView.ImageList.Images.Add( Properties.Resources.icon2 );
			ProjectExplorerTreeView.ImageList.Images.Add( Properties.Resources.icon2active );
			ProjectExplorerTreeView.ImageList.Images.Add( Properties.Resources.animationEngineIcon );
			ProjectExplorerTreeView.ImageList.Images.Add( Properties.Resources.animationEngineIconActive );

			PROJECT_ICON = 0;
			MODEL_ICON = 1;
			ACTIVEMODEL_ICON = 2;
			ANIMATIONDIAGRAM_ICON= 3;
			ACTIVEANIMATIONDIAGRAM_ICON = 4;

			MODELS_ICON = MODEL_ICON;
			DIAGRAMS_ICON = ANIMATIONDIAGRAM_ICON;

			ProjectNode = new TreeNode( Project.Name );
			ProjectNode.ImageIndex = PROJECT_ICON;
			ProjectNode.SelectedImageIndex = PROJECT_ICON;
			ProjectNode.ContextMenuStrip = ProjectGlobalContextMenuStrip;

			ModelsNode = new TreeNode( "models" );
			ModelsNode.ImageIndex = MODELS_ICON;
			ModelsNode.SelectedImageIndex = MODELS_ICON;
			ModelsNode.ContextMenuStrip = ProjectGlobalContextMenuStrip;

			DiagramsNode = new TreeNode( "diagrams" );
			DiagramsNode.ImageIndex = DIAGRAMS_ICON;
			DiagramsNode.SelectedImageIndex = DIAGRAMS_ICON;
			DiagramsNode.ContextMenuStrip = ProjectGlobalContextMenuStrip;

			ProjectNode.Nodes.Add( ModelsNode );
			ProjectNode.Nodes.Add( DiagramsNode );
			ProjectExplorerTreeView.Nodes.Add( ProjectNode );


			//TODO: add real support for multiple diagrams.
			TreeNode tempDiagramNode = new TreeNode( "diagram.xml" );
			tempDiagramNode.ImageIndex = ACTIVEANIMATIONDIAGRAM_ICON;
			tempDiagramNode.SelectedImageIndex = ACTIVEANIMATIONDIAGRAM_ICON;
			tempDiagramNode.ContextMenuStrip = ProjectGlobalContextMenuStrip;
			DiagramsNode.Nodes.Add( tempDiagramNode );
		}

		#region Attributes
		private Project m_project;

		private TreeNode m_projectNode;


		private TreeNode m_modelsNode;

		private TreeNode m_diagramsNode;

		private TreeNode m_clickedModelNode;



		#endregion

		#region Properties
		public Project Project
		{
			get { return m_project; }
			private set { m_project = value; }
		}

		public TreeNode ProjectNode
		{
			get { return m_projectNode; }
			set { m_projectNode = value; }
		}

		public TreeNode ModelsNode
		{
			get { return m_modelsNode; }
			set { m_modelsNode = value; }
		}

		private TreeNode ClickedModelNode
		{
			get { return m_clickedModelNode; }
			set { m_clickedModelNode = value; }
		}

		public TreeNode DiagramsNode
		{
			get { return m_diagramsNode; }
			set { m_diagramsNode = value; }
		}
		#endregion

		void OnProjectChanged( object sender, EventArgs e )
		{
			UpdateProjectView();
		}

		void UpdateProjectView()
		{
			ProjectNode.Text = Project.Name;

			ModelsNode.Nodes.Clear();

			foreach ( String model in Project.ModelsPath )
			{

				String modelName = Path.GetFileNameWithoutExtension( model );
				

				TreeNode modelNode = new TreeNode( modelName );
				modelNode.ToolTipText = model;

				if ( Project.IsActiveModel( model ) )
				{
					modelNode.ImageIndex = ACTIVEMODEL_ICON;
					modelNode.SelectedImageIndex = ACTIVEMODEL_ICON;
				}
				else
				{
					modelNode.ImageIndex = MODEL_ICON;
					modelNode.SelectedImageIndex = MODEL_ICON;
				}

				modelNode.ContextMenuStrip = ModelContextMenuStrip;
				
				ModelsNode.Nodes.Add( modelNode );
			}

			ProjectNode.ExpandAll();
		
		}

		private void AddModel_ToolStripMenuItem_Click( object sender, EventArgs e )
		{
			new AddModelTransaction( Project ).Execute();
		}

		private void SetActiveModel_ToolStripMenuItem_Click( object sender, EventArgs e )
		{
			String filename = ClickedModelNode.ToolTipText;
			new ReloadModelTransaction( Project, filename ).Execute();
		}

		private void ReloadModel_ToolStripMenuItem_Click( object sender, EventArgs e )
		{
			String filename = ClickedModelNode.ToolTipText;
			new ReloadModelTransaction( Project, filename ).Execute();
		}

		private void RemoveModel_ToolStripMenuItem_Click( object sender, EventArgs e )
		{
			String filename = ClickedModelNode.ToolTipText;
			Project.RemoveModel( filename );
		}

		private void ProjectExplorerTreeView_MouseClick( object sender, MouseEventArgs e )
		{
			ClickedModelNode = ProjectExplorerTreeView.GetNodeAt( e.Location );
		}

		private void ProjectExplorerTreeView_DoubleClick( object sender, EventArgs e )
		{
			TreeNode selected = ProjectExplorerTreeView.SelectedNode;
			if ( selected == null )
			{
				return;
			}
			
			// reload models when double-clicking on their entry.
			if ( selected.Parent != null && selected.Parent.Equals( ModelsNode) )
			{
				String filename = selected.ToolTipText;
				new ReloadModelTransaction( Project, filename ).Execute();
			}
		}
	}
}