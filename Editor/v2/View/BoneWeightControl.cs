using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TecnoFreak.v2.View
{
	public partial class BoneWeightControl : UserControl
	{
		public event EventHandler WeightValueChanged;

		public BoneWeightControl( String name )
		{
			InitializeComponent();

			BoneNameLabel.Text = name;
		}

		private void Weight_ValueChanged( object sender, EventArgs e )
		{
			if ( WeightValueChanged != null )
			{
				WeightValueChanged( this, new EventArgs() );
			}
		}

		public float GetWeight()
		{
			return Weight.Value;
		}
	}
}
