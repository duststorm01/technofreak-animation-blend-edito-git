namespace TecnoFreak.v2.View
{
	partial class GetNameDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( GetNameDialog ) );
			this.OK_Button = new System.Windows.Forms.Button();
			this.Cancel_Button = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.NameComboBox = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// OK_Button
			// 
			this.OK_Button.Enabled = false;
			this.OK_Button.Location = new System.Drawing.Point( 116, 36 );
			this.OK_Button.Name = "OK_Button";
			this.OK_Button.Size = new System.Drawing.Size( 75, 27 );
			this.OK_Button.TabIndex = 0;
			this.OK_Button.Text = "Ok";
			this.OK_Button.UseVisualStyleBackColor = true;
			this.OK_Button.Click += new System.EventHandler( this.OK_Button_Click );
			// 
			// Cancel_Button
			// 
			this.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Cancel_Button.Location = new System.Drawing.Point( 197, 36 );
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new System.Drawing.Size( 75, 27 );
			this.Cancel_Button.TabIndex = 1;
			this.Cancel_Button.Text = "Cancel";
			this.Cancel_Button.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 13, 12 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 38, 13 );
			this.label1.TabIndex = 3;
			this.label1.Text = "Name:";
			// 
			// NameComboBox
			// 
			this.NameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.NameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.NameComboBox.FormattingEnabled = true;
			this.NameComboBox.Location = new System.Drawing.Point( 57, 9 );
			this.NameComboBox.Name = "NameComboBox";
			this.NameComboBox.Size = new System.Drawing.Size( 215, 21 );
			this.NameComboBox.TabIndex = 4;
			this.NameComboBox.SelectedIndexChanged += new System.EventHandler( this.NameComboBox_SelectedIndexChanged );
			this.NameComboBox.TextUpdate += new System.EventHandler( this.NameComboBox_TextUpdate );
			// 
			// GetNameDialog
			// 
			this.AcceptButton = this.OK_Button;
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Cancel_Button;
			this.ClientSize = new System.Drawing.Size( 284, 73 );
			this.Controls.Add( this.NameComboBox );
			this.Controls.Add( this.label1 );
			this.Controls.Add( this.Cancel_Button );
			this.Controls.Add( this.OK_Button );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ( ( System.Drawing.Icon ) ( resources.GetObject( "$this.Icon" ) ) );
			this.Name = "GetNameDialog";
			this.Text = "Name please!";
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button OK_Button;
		private System.Windows.Forms.Button Cancel_Button;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox NameComboBox;
	}
}