namespace TecnoFreak.v2.View
{
	partial class WelcomeView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( WelcomeView ) );
			this.OpenProjectButton = new System.Windows.Forms.Button();
			this.NewProjectButton = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.Email_Author = new System.Windows.Forms.LinkLabel();
			this.buildLabel = new System.Windows.Forms.Label();
			( ( System.ComponentModel.ISupportInitialize ) ( this.pictureBox1 ) ).BeginInit();
			this.SuspendLayout();
			// 
			// OpenProjectButton
			// 
			this.OpenProjectButton.Image = global::TecnoFreak.Properties.Resources.open_big;
			this.OpenProjectButton.Location = new System.Drawing.Point( 162, 292 );
			this.OpenProjectButton.Name = "OpenProjectButton";
			this.OpenProjectButton.Size = new System.Drawing.Size( 96, 96 );
			this.OpenProjectButton.TabIndex = 3;
			this.OpenProjectButton.Text = "Open Project";
			this.OpenProjectButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.OpenProjectButton.UseVisualStyleBackColor = true;
			this.OpenProjectButton.Click += new System.EventHandler( this.OpenProjectButton_Click );
			// 
			// NewProjectButton
			// 
			this.NewProjectButton.Image = global::TecnoFreak.Properties.Resources.new_big;
			this.NewProjectButton.Location = new System.Drawing.Point( 30, 292 );
			this.NewProjectButton.Name = "NewProjectButton";
			this.NewProjectButton.Size = new System.Drawing.Size( 96, 96 );
			this.NewProjectButton.TabIndex = 2;
			this.NewProjectButton.Text = "New Project";
			this.NewProjectButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.NewProjectButton.UseVisualStyleBackColor = true;
			this.NewProjectButton.Click += new System.EventHandler( this.NewProjectButton_Click );
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ( ( System.Drawing.Image ) ( resources.GetObject( "pictureBox1.Image" ) ) );
			this.pictureBox1.InitialImage = null;
			this.pictureBox1.Location = new System.Drawing.Point( -8, -18 );
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size( 300, 300 );
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// Email_Author
			// 
			this.Email_Author.AutoSize = true;
			this.Email_Author.BackColor = System.Drawing.Color.White;
			this.Email_Author.Location = new System.Drawing.Point( 168, 247 );
			this.Email_Author.Name = "Email_Author";
			this.Email_Author.Size = new System.Drawing.Size( 101, 13 );
			this.Email_Author.TabIndex = 5;
			this.Email_Author.TabStop = true;
			this.Email_Author.Text = "pnovau@gmail.com";
			this.Email_Author.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.Email_Author.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler( this.Email_Author_LinkClicked );
			// 
			// buildLabel
			// 
			this.buildLabel.AutoSize = true;
			this.buildLabel.BackColor = System.Drawing.Color.White;
			this.buildLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
			this.buildLabel.Location = new System.Drawing.Point( 18, 247 );
			this.buildLabel.Name = "buildLabel";
			this.buildLabel.Size = new System.Drawing.Size( 42, 13 );
			this.buildLabel.TabIndex = 4;
			this.buildLabel.Text = "Version";
			// 
			// WelcomeView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 291, 400 );
			this.Controls.Add( this.Email_Author );
			this.Controls.Add( this.buildLabel );
			this.Controls.Add( this.OpenProjectButton );
			this.Controls.Add( this.NewProjectButton );
			this.Controls.Add( this.pictureBox1 );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ( ( System.Drawing.Icon ) ( resources.GetObject( "$this.Icon" ) ) );
			this.KeyPreview = true;
			this.Name = "WelcomeView";
			this.Text = "Welcome";
			//this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler( this.WelcomeView_PreviewKeyDown );
			( ( System.ComponentModel.ISupportInitialize ) ( this.pictureBox1 ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button NewProjectButton;
		private System.Windows.Forms.Button OpenProjectButton;
		private System.Windows.Forms.LinkLabel Email_Author;
		private System.Windows.Forms.Label buildLabel;
	}
}