/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TecnoFreakCommon;
using TecnoFreak.v2.Controller.Interfaces;

namespace TecnoFreak.v2.View
{
	public partial class BoneWeightsView : DockableView, ISelectedBoneWeightController
	{
		IViewport m_viewport;

		BoneWeightControl m_selectedBonesWeightControl;

		public event EventHandler SelectedBoneWeightValueChanged;

		public BoneWeightsView( IAnimableLoader animableLoader, IViewport viewport )
		{
			InitializeComponent();

			m_viewport = viewport;

			CreateControls();

			animableLoader.AnimableChanged += new EventHandler( AnimableLoader_AnimableChanged );
			SizeChanged += new EventHandler( BoneWeightsView_SizeChanged );
		}

		void BoneWeightsView_SizeChanged( object sender, EventArgs e )
		{
			LayoutPanel.ResumeLayout();
		}

		void CreateControls()
		{
			ClearControls();

			//int boneCount = m_viewport.GetBoneCount();
			//Control[] boneWeightControls = new Control[ boneCount ];

			//for ( int i = 0; i < boneCount; ++i )
			{
				String boneName = "Selected";
				m_selectedBonesWeightControl = new BoneWeightControl( boneName );
				m_selectedBonesWeightControl.WeightValueChanged += new EventHandler( SelectedControl_WeightValueChanged );
				LayoutPanel.Controls.Add( m_selectedBonesWeightControl );
			}
		}

		void SelectedControl_WeightValueChanged( object sender, EventArgs e )
		{
			if ( SelectedBoneWeightValueChanged != null )
			{
				SelectedBoneWeightValueChanged( this, new EventArgs() );
			}
		}

		private void ClearControls()
		{
			LayoutPanel.Controls.Clear();
		}

		public float GetWeight()
		{
			return m_selectedBonesWeightControl.GetWeight();
		}

		void AnimableLoader_AnimableChanged( object sender, EventArgs e )
		{
			CreateControls();
		}

	}
}