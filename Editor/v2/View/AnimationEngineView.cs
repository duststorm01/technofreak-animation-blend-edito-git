/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TecnoFreakCommon;
using TecnoFreak.v2.Model.AnimationEngine.Nodes;
using TecnoFreak.v2.Model.AnimationEngine.Nodes.MathNode;
using TFControls.Diagram;
using TecnoFreak.v2.Controller.DiagramIO;
using System.IO;
using TecnoFreak.v2.Controller.AnimationEngine;

namespace TecnoFreak.v2.View
{
	public partial class AnimationEngineView : DockableView, IDiagramIO
	{
		private NodeFactory m_nodeFactory = new NodeFactory();

		public Diagram Diagram 
		{
			get { return DiagramControl.Diagram; }
		}

		public AnimationEngineView( IAnimable animable, IAnimableLoader model )
		{
			InitializeComponent();
			Animable = animable;

			m_nodeFactory.Init( animable, contextMenu_CreateNode );
			m_nodeFactory.NodeCreated += new EventHandler<NodeCreatedEventArgs>( m_nodeFactory_NodeCreated );

			model.AnimableChanged += new EventHandler(model_AnimableChanged);

			DiagramControl.DiagramImage.ContextMenuStrip = contextMenu;

			CurrentAnimations = new List<AnimationNode>();

			DiagramControl.Diagram.Connector.ConnectorValidator = new Model.AnimationEngine.ConnectionValidator();
			DiagramControl.Diagram.Connector.OutConnectionsUnique = false;
			New();

			Diagram.NodeCreated += new EventHandler( Diagram_DiagramChanged );
			Diagram.NodeDeleted += new EventHandler( Diagram_DiagramChanged );
			Diagram.PortConnected += new EventHandler( Diagram_DiagramChanged );
			Diagram.PortDisconnected += new EventHandler( Diagram_DiagramChanged );
			Diagram.PortValueChanged += new EventHandler( Diagram_PortValueChanged );
			Diagram.CustomDataChangedEvent += new EventHandler( Diagram_DiagramChanged );
			Diagram.NodeMoved += new EventHandler( Diagram_NodeMoved );

			Animable.AnimationTreeControl = true;
		}

		void model_AnimableChanged(object sender, EventArgs e)
		{
			Reload();
		}

		void m_nodeFactory_NodeCreated( object sender, NodeCreatedEventArgs e )
		{
			DiagramControl.AddNodeAtMouseLocation( e.Node );
			contextMenu.Hide();
		}

		void Diagram_NodeMoved( object sender, EventArgs e )
		{
			if ( Changed != null )
			{
				Changed( this, new EventArgs() );
			}
		}

		void Diagram_PortValueChanged( object sender, EventArgs e )
		{
			// TODO: update animable parameters for port that has changed.
			/*foreach ( String parameterName in ParameterNode.AllParamaterNames )
			{
				float parameterValue = ParameterNode.GetParameterValue( parameterName );
				Animable.SetParameterValue( parameterName, parameterValue );
			}*/

			Port p = ( Port )( sender );
			if ( p == null || p.ValueControl == null )
			{
				return;
			}

			String parameterName;
			if ( p.Parent.GetType() == typeof( ParameterNode ) )
			{
				ParameterNode pn = ( ParameterNode )p.Parent;
				parameterName = pn.ParameterName;
			}
			else
			{
				parameterName = p.PortWorkParameterName;
			}
			ParameterNode.SetParameterValue( parameterName, p.ValueControl.Value );
			Animable.SetParameterValue( parameterName, p.ValueControl.Value );

			if ( Changed != null )
			{
				Changed( this, new EventArgs() );
			}
		}

		void Diagram_DiagramChanged( object sender, EventArgs e )
		{
			ReloadRuntimeAnimationTree();

			if ( Changed != null )
			{
				Changed( this, new EventArgs() );
			}
		}

		void ReloadRuntimeAnimationTree()
		{
			DiagramWorkRuntimeIO drio = new DiagramWorkRuntimeIO();
			try
			{
				String tempFilename = Path.GetTempFileName();
				drio.SaveDiagram( DiagramControl.Diagram, tempFilename );
				Animable.LoadAnimationTree( tempFilename );
			}
			catch ( System.Exception )
			{
			}
		}

		void New()
		{
			DiagramControl.New();
			OutputNode = new Model.AnimationEngine.Nodes.OutputNode();
			DiagramControl.Diagram.AddNode( OutputNode );
		}

		void Reload()
		{
			ReloadRuntimeAnimationTree();
			DiagramControl.ForceUpdate = true;

			m_nodeFactory.Init( Animable, contextMenu_CreateNode );
		}

		private IAnimable m_animable;
		private OutputNode m_outputNode;
		private List<AnimationNode> m_currentAnimations;

		public List<AnimationNode> CurrentAnimations
		{
			get { return m_currentAnimations; }
			set { m_currentAnimations = value; }
		}

		public OutputNode OutputNode
		{
			get { return m_outputNode; }
			protected set { m_outputNode = value; }
		}

		public IAnimable Animable
		{
			get { return m_animable; }
			protected set { m_animable = value; }
		}

		List<ToolStripMenuItem> m_customItem;

		void ClearCustomItems()
		{
			if ( m_customItem != null )
			{
				foreach ( ToolStripMenuItem t in m_customItem )
				{
					contextMenu.Items.Remove( t );
				}
			}

			m_customItem = new List<ToolStripMenuItem>();
		}

		private void contextMenu_Opening( object sender, CancelEventArgs e )
		{
			ClearCustomItems();

			foreach( TFControls.Diagram.Node n in DiagramControl.Diagram.SelectedNodes )
			{
				BaseNode node = ( BaseNode ) n;

				if ( node.CustomOperations != null )
				{
					m_customItem.Add( node.CustomOperations );
				}
			}

			foreach ( ToolStripMenuItem t in m_customItem )
			{
				contextMenu.Items.Add( t );
			}
		}

		private void contextMenu_Closing( object sender, ToolStripDropDownClosingEventArgs e )
		{
			ClearCustomItems();
			DiagramControl.DiagramImage.Invalidate();
		}

		private void contextMenu_DeleteNode_Click( object sender, EventArgs e )
		{
			DiagramControl.Diagram.RemoveSelectedNodes();
			DiagramControl.DiagramImage.Invalidate();
		}


		#region IDiagramIO Members

		public event EventHandler Changed;

		public void SaveDiagram( string filename )
		{
			Controller.DiagramIO.DiagramIO diagramSaver = new TecnoFreak.v2.Controller.DiagramIO.DiagramIO( m_nodeFactory );
			try
			{
				diagramSaver.SaveDiagram( DiagramControl.Diagram, filename );
			}
			catch ( Exception )
			{
				// do nothing.
			}
		}

		public void LoadDiagram( string filename )
		{
			Controller.DiagramIO.DiagramIO diagramLoader = new TecnoFreak.v2.Controller.DiagramIO.DiagramIO( m_nodeFactory );

			try
			{
				diagramLoader.LoadDiagram( Animable, DiagramControl.Diagram, filename );
				OutputNode = diagramLoader.OutputNode;
				//EvaluateAnimationNodes();
				DiagramControl.ForceUpdate = true;
			}
			catch ( Exception )
			{
				New();
			}
		}

		public void NewDiagram()
		{
			New();
		}

		#endregion








	}
}