namespace TecnoFreak.v2.View
{
	partial class ProjectExplorerView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ProjectExplorerView ) );
			this.ProjectExplorerTreeView = new System.Windows.Forms.TreeView();
			this.ProjectGlobalContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.AddModel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ModelContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.SetActiveModel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.RemoveModel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ReloadModel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ProjectGlobalContextMenuStrip.SuspendLayout();
			this.ModelContextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// ProjectExplorerTreeView
			// 
			this.ProjectExplorerTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ProjectExplorerTreeView.Location = new System.Drawing.Point( 0, 0 );
			this.ProjectExplorerTreeView.Name = "ProjectExplorerTreeView";
			this.ProjectExplorerTreeView.ShowNodeToolTips = true;
			this.ProjectExplorerTreeView.Size = new System.Drawing.Size( 391, 518 );
			this.ProjectExplorerTreeView.TabIndex = 0;
			this.ProjectExplorerTreeView.MouseClick += new System.Windows.Forms.MouseEventHandler( this.ProjectExplorerTreeView_MouseClick );
			this.ProjectExplorerTreeView.DoubleClick += new System.EventHandler( this.ProjectExplorerTreeView_DoubleClick );
			// 
			// ProjectGlobalContextMenuStrip
			// 
			this.ProjectGlobalContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.AddModel_ToolStripMenuItem} );
			this.ProjectGlobalContextMenuStrip.Name = "contextMenuStrip1";
			this.ProjectGlobalContextMenuStrip.Size = new System.Drawing.Size( 153, 48 );
			// 
			// AddModel_ToolStripMenuItem
			// 
			this.AddModel_ToolStripMenuItem.Name = "AddModel_ToolStripMenuItem";
			this.AddModel_ToolStripMenuItem.Size = new System.Drawing.Size( 152, 22 );
			this.AddModel_ToolStripMenuItem.Text = "Add Model...";
			this.AddModel_ToolStripMenuItem.Click += new System.EventHandler( this.AddModel_ToolStripMenuItem_Click );
			// 
			// ModelContextMenuStrip
			// 
			this.ModelContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.SetActiveModel_ToolStripMenuItem,
            this.RemoveModel_ToolStripMenuItem,
            this.ReloadModel_ToolStripMenuItem} );
			this.ModelContextMenuStrip.Name = "contextMenuStrip2";
			this.ModelContextMenuStrip.Size = new System.Drawing.Size( 127, 70 );
			// 
			// SetActiveModel_ToolStripMenuItem
			// 
			this.SetActiveModel_ToolStripMenuItem.Name = "SetActiveModel_ToolStripMenuItem";
			this.SetActiveModel_ToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
			this.SetActiveModel_ToolStripMenuItem.Text = "Set Active";
			this.SetActiveModel_ToolStripMenuItem.Click += new System.EventHandler( this.SetActiveModel_ToolStripMenuItem_Click );
			// 
			// RemoveModel_ToolStripMenuItem
			// 
			this.RemoveModel_ToolStripMenuItem.Name = "RemoveModel_ToolStripMenuItem";
			this.RemoveModel_ToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
			this.RemoveModel_ToolStripMenuItem.Text = "Remove";
			this.RemoveModel_ToolStripMenuItem.Click += new System.EventHandler( this.RemoveModel_ToolStripMenuItem_Click );
			// 
			// ReloadModel_ToolStripMenuItem
			// 
			this.ReloadModel_ToolStripMenuItem.Name = "ReloadModel_ToolStripMenuItem";
			this.ReloadModel_ToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
			this.ReloadModel_ToolStripMenuItem.Text = "Reload";
			this.ReloadModel_ToolStripMenuItem.Click += new System.EventHandler( this.ReloadModel_ToolStripMenuItem_Click );
			// 
			// ProjectExplorerView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 391, 518 );
			this.Controls.Add( this.ProjectExplorerTreeView );
			this.HideOnClose = true;
			this.Icon = ( ( System.Drawing.Icon ) ( resources.GetObject( "$this.Icon" ) ) );
			this.Name = "ProjectExplorerView";
			this.TabText = "Project Explorer";
			this.Text = "Project Explorer";
			this.ProjectGlobalContextMenuStrip.ResumeLayout( false );
			this.ModelContextMenuStrip.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.TreeView ProjectExplorerTreeView;
		private System.Windows.Forms.ContextMenuStrip ProjectGlobalContextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem AddModel_ToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip ModelContextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem SetActiveModel_ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem RemoveModel_ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ReloadModel_ToolStripMenuItem;
	}
}