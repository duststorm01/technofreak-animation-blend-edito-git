namespace TecnoFreak.v2.View
{
	partial class ViewportView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ViewportView ) );
			this.ViewportPanel = new System.Windows.Forms.Panel();
			this.ViewportMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
			this.Display_ViewportMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
			this.Scheme_Display_ViewportMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
			this.Background_Display_ViewportMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
			this.Skeleton_Display_ViewportMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
			this.ViewportMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// ViewportPanel
			// 
			this.ViewportPanel.ContextMenuStrip = this.ViewportMenuStrip;
			this.ViewportPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ViewportPanel.Location = new System.Drawing.Point( 0, 0 );
			this.ViewportPanel.MinimumSize = new System.Drawing.Size( 64, 64 );
			this.ViewportPanel.Name = "ViewportPanel";
			this.ViewportPanel.Size = new System.Drawing.Size( 489, 438 );
			this.ViewportPanel.TabIndex = 0;
			// 
			// ViewportMenuStrip
			// 
			this.ViewportMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.Display_ViewportMenuStrip} );
			this.ViewportMenuStrip.Name = "ViewportMenuStrip";
			this.ViewportMenuStrip.Size = new System.Drawing.Size( 153, 48 );
			// 
			// Display_ViewportMenuStrip
			// 
			this.Display_ViewportMenuStrip.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.Scheme_Display_ViewportMenuStrip,
            this.Background_Display_ViewportMenuStrip,
            this.Skeleton_Display_ViewportMenuStrip} );
			this.Display_ViewportMenuStrip.Name = "Display_ViewportMenuStrip";
			this.Display_ViewportMenuStrip.Size = new System.Drawing.Size( 152, 22 );
			this.Display_ViewportMenuStrip.Text = "Display";
			// 
			// Scheme_Display_ViewportMenuStrip
			// 
			this.Scheme_Display_ViewportMenuStrip.Name = "Scheme_Display_ViewportMenuStrip";
			this.Scheme_Display_ViewportMenuStrip.Size = new System.Drawing.Size( 152, 22 );
			this.Scheme_Display_ViewportMenuStrip.Text = "Scheme";
			// 
			// Background_Display_ViewportMenuStrip
			// 
			this.Background_Display_ViewportMenuStrip.Name = "Background_Display_ViewportMenuStrip";
			this.Background_Display_ViewportMenuStrip.Size = new System.Drawing.Size( 152, 22 );
			this.Background_Display_ViewportMenuStrip.Text = "Background";
			this.Background_Display_ViewportMenuStrip.Click += new System.EventHandler( this.Background_Display_ViewportMenuStrip_Click );
			// 
			// Skeleton_Display_ViewportMenuStrip
			// 
			this.Skeleton_Display_ViewportMenuStrip.Name = "Skeleton_Display_ViewportMenuStrip";
			this.Skeleton_Display_ViewportMenuStrip.Size = new System.Drawing.Size( 152, 22 );
			this.Skeleton_Display_ViewportMenuStrip.Text = "Skeleton";
			this.Skeleton_Display_ViewportMenuStrip.Click += new System.EventHandler( this.Skeleton_Display_ViewportMenuStrip_Click );
			// 
			// ViewportView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 489, 438 );
			this.Controls.Add( this.ViewportPanel );
			this.HideOnClose = true;
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.Name = "ViewportView";
			this.TabText = "Viewport";
			this.Text = "Viewport";
			this.ViewportMenuStrip.ResumeLayout( false );
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.Panel ViewportPanel;
		private System.Windows.Forms.ContextMenuStrip ViewportMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem Display_ViewportMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem Scheme_Display_ViewportMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem Background_Display_ViewportMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem Skeleton_Display_ViewportMenuStrip;
	}
}