/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TecnoFreak.v2.View
{
	public partial class GetNameDialog : Form
	{
		public GetNameDialog()
		{
			InitializeComponent();
		}

		public ICollection< String > SuggestNames
		{
			set
			{
				NameComboBox.Items.Clear();
				NameComboBox.AutoCompleteCustomSource.Clear();
				foreach ( String s in value )
				{
					NameComboBox.AutoCompleteCustomSource.Add( s );
					NameComboBox.Items.Add( s );
				}
			}
		}

		public String InputName
		{
			get { return NameComboBox.Text.Trim(); }
		}

		private void NameComboBox_TextUpdate( object sender, EventArgs e )
		{
			OK_Button.Enabled = ( InputName.Length != 0 );
		}

		private void OK_Button_Click( object sender, EventArgs e )
		{
			this.DialogResult = DialogResult.OK;
		}

		private void NameComboBox_SelectedIndexChanged( object sender, EventArgs e )
		{
			OK_Button.Enabled = ( InputName.Length != 0 );
		}
	}
}