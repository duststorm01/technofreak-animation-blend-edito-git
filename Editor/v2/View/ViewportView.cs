/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TecnoFreakCommon;
using System.Diagnostics;
using System.Xml;
using System.Reflection;
using System.IO;

namespace TecnoFreak.v2.View
{
	public partial class ViewportView : DockableView
	{
		IPluginViewport m_customViewport;

		String m_viewportDllName = "TecnoFreakViewport.dll";
		String m_viewportClassName = "TecnoFreakViewport.MOgreViewport";
		public ViewportView()
		{
			InitializeComponent();

			ReadPluginInfo();

			try
			{
				String viewportDllName = m_viewportDllName;
				Assembly viewportDll = Assembly.LoadFile( Application.StartupPath + "/" + viewportDllName );

				String viewportClassName = m_viewportClassName;
				m_customViewport = (IPluginViewport)(viewportDll.CreateInstance(viewportClassName));
				if ( m_customViewport == null )
				{
					throw new Exception("Unable to instantiate viewport plugin!");
				}
				
				m_customViewport.Init(ViewportPanel);
			}
			catch ( Exception e )
			{
				throw new Exception("Could not create viewport: " + e.Message, e);
			}

			CreateViewportMenu();

			m_customViewport.SkeletonVisibilityChanged += new ViewportEventHandler(Viewport_SkeletonVisibilityChanged);

			DisplaySettingsView = new DisplaySettingsView( m_customViewport );
		}

		private void ReadPluginInfo()
		{
			StreamReader reader = null;
			try
			{
				reader = new StreamReader(Application.StartupPath + "/" + "viewport.plugin.txt");
			}
			catch ( System.Exception )
			{
				MessageBox.Show( "Could not open viewport.plugin.txt file" );
				return;
			}

			String line = reader.ReadLine();
			String[] tokenizedLine = line.Split(' ');
			if (tokenizedLine.Length == 2)
			{
				m_viewportDllName = tokenizedLine[0];
				m_viewportClassName = tokenizedLine[1];
			}
			else
			{
				MessageBox.Show("Viewport plugin definition could not be found");
			}
		}

		

		public void Redraw()
		{
			if ( m_customViewport != null )
			{
				m_customViewport.Redraw();
			}
		}

		public void SetSkeletonVisible( bool visible )
		{
			Skeleton_Display_ViewportMenuStrip.Checked = visible;
			Viewport.ShowSkeleton = Skeleton_Display_ViewportMenuStrip.Checked;
		}

		public bool GetSkeletonVisible()
		{
			return Viewport.ShowSkeleton;
		}

		private void CreateViewportMenu()
		{
			foreach ( String viewportMode in Viewport.AvailableViewportModes )
			{
				ToolStripMenuItem viewportModeMenuItem = new ToolStripMenuItem( viewportMode );
				viewportModeMenuItem.Click += new EventHandler( ViewportModeMenuItem_Click );
				
				Scheme_Display_ViewportMenuStrip.DropDownItems.Add( viewportModeMenuItem );
			}

			Viewport.ViewportModeChanged += new ViewportEventHandler( Viewport_ViewportModeChanged );

			Viewport_ViewportModeChanged( this, new EventArgs() );
		}

		
		#region Event Handlers

		private void ViewportModeMenuItem_Click( object sender, EventArgs e )
		{
			ToolStripMenuItem viewportModeMenuItem = ( ToolStripMenuItem ) sender;
			Debug.Assert( viewportModeMenuItem != null );

			String selectedViewportMode = viewportModeMenuItem.Text;

			Viewport.ViewportMode = selectedViewportMode;
		}

		void Viewport_ViewportModeChanged( object sender, EventArgs e )
		{
			foreach ( ToolStripMenuItem viewportModeMenuItem in Scheme_Display_ViewportMenuStrip.DropDownItems )
			{
				viewportModeMenuItem.Checked = ( viewportModeMenuItem.Text == Viewport.ViewportMode );
			}
		}

		private void Background_Display_ViewportMenuStrip_Click( object sender, EventArgs e )
		{
			ColorDialog colorDialog = new ColorDialog( Viewport.BackgroundColor );
			DialogResult result = colorDialog.ShowDialog();

			if ( result == DialogResult.OK )
			{
				Viewport.BackgroundColor = colorDialog.Color;
			}
		}

		private void Skeleton_Display_ViewportMenuStrip_Click( object sender, EventArgs e )
		{
			Skeleton_Display_ViewportMenuStrip.Checked = !Skeleton_Display_ViewportMenuStrip.Checked;
			Viewport.ShowSkeleton = Skeleton_Display_ViewportMenuStrip.Checked;
		}

		void Viewport_SkeletonVisibilityChanged( object sender, EventArgs e )
		{
			Skeleton_Display_ViewportMenuStrip.Checked = Viewport.ShowSkeleton;
		}

		#endregion


		#region Attributes
		private DisplaySettingsView m_displaySettingsView;
		#endregion

		#region Properties

		public IViewport Viewport
		{ 
			get { return m_customViewport; }
		}

		public IAnimableLoader AnimableLoader
		{
			get { return m_customViewport; }
		}

		public IPluginViewport PluginViewport
		{
			get { return m_customViewport; }
		}

		public DisplaySettingsView DisplaySettingsView
		{
			get { return m_displaySettingsView; }
			protected set { m_displaySettingsView = value; }
		}

		#endregion

	}
}