namespace TecnoFreak.v2.View
{
	partial class MainView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MainView ) );
			this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
			this.tools = new System.Windows.Forms.ToolStrip();
			this.tools_New = new System.Windows.Forms.ToolStripButton();
			this.tools_Open = new System.Windows.Forms.ToolStripButton();
			this.tools_Save = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.tools_AnimationEngine = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.tools_About = new System.Windows.Forms.ToolStripButton();
			this.menu = new System.Windows.Forms.MenuStrip();
			this.menu_File = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_File_NewProject = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_File_OpenProject = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_File_SaveProject = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_File_AddModel = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.menu_File_Exit = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Windows = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Windows_Viewport = new System.Windows.Forms.ToolStripMenuItem();
			this.animationEngineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.animationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.projectExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Build = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Build_RuntimeDiagram = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Help = new System.Windows.Forms.ToolStripMenuItem();
			this.status = new System.Windows.Forms.StatusStrip();
			this.status_Message = new System.Windows.Forms.ToolStripStatusLabel();
			this.clock = new System.Windows.Forms.Timer( this.components );
			this.ContentPanel = new System.Windows.Forms.Panel();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tools.SuspendLayout();
			this.menu.SuspendLayout();
			this.status.SuspendLayout();
			this.ContentPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// dockPanel
			// 
			this.dockPanel.ActiveAutoHideContent = null;
			this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dockPanel.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
			this.dockPanel.Location = new System.Drawing.Point( 0, 0 );
			this.dockPanel.Name = "dockPanel";
			this.dockPanel.Size = new System.Drawing.Size( 692, 506 );
			this.dockPanel.TabIndex = 0;
			this.dockPanel.ActiveContentChanged += new System.EventHandler( this.dockPanel_ActiveContentChanged );
			// 
			// tools
			// 
			this.tools.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.tools_New,
            this.tools_Open,
            this.tools_Save,
            this.toolStripSeparator1,
            this.tools_AnimationEngine,
            this.toolStripSeparator,
            this.tools_About} );
			this.tools.Location = new System.Drawing.Point( 0, 24 );
			this.tools.Name = "tools";
			this.tools.Size = new System.Drawing.Size( 692, 25 );
			this.tools.TabIndex = 7;
			this.tools.Text = "toolStrip";
			// 
			// tools_New
			// 
			this.tools_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tools_New.Image = ( ( System.Drawing.Image )( resources.GetObject( "tools_New.Image" ) ) );
			this.tools_New.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tools_New.Name = "tools_New";
			this.tools_New.Size = new System.Drawing.Size( 23, 22 );
			this.tools_New.Text = "&New";
			this.tools_New.Click += new System.EventHandler( this.tools_New_Click );
			// 
			// tools_Open
			// 
			this.tools_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tools_Open.Image = ( ( System.Drawing.Image )( resources.GetObject( "tools_Open.Image" ) ) );
			this.tools_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tools_Open.Name = "tools_Open";
			this.tools_Open.Size = new System.Drawing.Size( 23, 22 );
			this.tools_Open.Text = "&Open";
			this.tools_Open.Click += new System.EventHandler( this.tools_Open_Click );
			// 
			// tools_Save
			// 
			this.tools_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tools_Save.Image = ( ( System.Drawing.Image )( resources.GetObject( "tools_Save.Image" ) ) );
			this.tools_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tools_Save.Name = "tools_Save";
			this.tools_Save.Size = new System.Drawing.Size( 23, 22 );
			this.tools_Save.Text = "&Save";
			this.tools_Save.Click += new System.EventHandler( this.tools_Save_Click );
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size( 6, 25 );
			// 
			// tools_AnimationEngine
			// 
			this.tools_AnimationEngine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tools_AnimationEngine.Image = ( ( System.Drawing.Image )( resources.GetObject( "tools_AnimationEngine.Image" ) ) );
			this.tools_AnimationEngine.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tools_AnimationEngine.Name = "tools_AnimationEngine";
			this.tools_AnimationEngine.Size = new System.Drawing.Size( 23, 22 );
			this.tools_AnimationEngine.Text = "Animation Engine";
			this.tools_AnimationEngine.ToolTipText = "Animation Engine";
			this.tools_AnimationEngine.Click += new System.EventHandler( this.tools_AnimationEngine_Click );
			// 
			// toolStripSeparator
			// 
			this.toolStripSeparator.Name = "toolStripSeparator";
			this.toolStripSeparator.Size = new System.Drawing.Size( 6, 25 );
			// 
			// tools_About
			// 
			this.tools_About.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tools_About.Image = ( ( System.Drawing.Image )( resources.GetObject( "tools_About.Image" ) ) );
			this.tools_About.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tools_About.Name = "tools_About";
			this.tools_About.Size = new System.Drawing.Size( 23, 22 );
			this.tools_About.Text = "&About";
			this.tools_About.Click += new System.EventHandler( this.tools_About_Click );
			// 
			// menu
			// 
			this.menu.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.menu_File,
            this.menu_Windows,
            this.menu_Build,
            this.menu_Help} );
			this.menu.Location = new System.Drawing.Point( 0, 0 );
			this.menu.Name = "menu";
			this.menu.Size = new System.Drawing.Size( 692, 24 );
			this.menu.TabIndex = 8;
			this.menu.Text = "menu";
			// 
			// menu_File
			// 
			this.menu_File.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.menu_File_NewProject,
            this.menu_File_OpenProject,
            this.menu_File_SaveProject,
            this.menu_File_AddModel,
            this.toolStripSeparator2,
            this.menu_File_Exit} );
			this.menu_File.Name = "menu_File";
			this.menu_File.Size = new System.Drawing.Size( 35, 20 );
			this.menu_File.Text = "File";
			// 
			// menu_File_NewProject
			// 
			this.menu_File_NewProject.Name = "menu_File_NewProject";
			this.menu_File_NewProject.Size = new System.Drawing.Size( 160, 22 );
			this.menu_File_NewProject.Text = "New Project...";
			this.menu_File_NewProject.Click += new System.EventHandler( this.menu_File_NewProject_Click );
			// 
			// menu_File_OpenProject
			// 
			this.menu_File_OpenProject.Name = "menu_File_OpenProject";
			this.menu_File_OpenProject.Size = new System.Drawing.Size( 160, 22 );
			this.menu_File_OpenProject.Text = "Open Project...";
			this.menu_File_OpenProject.Click += new System.EventHandler( this.menu_File_OpenProject_Click );
			// 
			// menu_File_SaveProject
			// 
			this.menu_File_SaveProject.Name = "menu_File_SaveProject";
			this.menu_File_SaveProject.Size = new System.Drawing.Size( 160, 22 );
			this.menu_File_SaveProject.Text = "Save Project";
			this.menu_File_SaveProject.Click += new System.EventHandler( this.menu_File_SaveProject_Click );
			// 
			// menu_File_AddModel
			// 
			this.menu_File_AddModel.Name = "menu_File_AddModel";
			this.menu_File_AddModel.Size = new System.Drawing.Size( 160, 22 );
			this.menu_File_AddModel.Text = "Add Model...";
			this.menu_File_AddModel.Click += new System.EventHandler( this.menu_File_AddModel_Click );
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size( 157, 6 );
			// 
			// menu_File_Exit
			// 
			this.menu_File_Exit.Name = "menu_File_Exit";
			this.menu_File_Exit.Size = new System.Drawing.Size( 160, 22 );
			this.menu_File_Exit.Text = "Exit";
			this.menu_File_Exit.Click += new System.EventHandler( this.menu_File_Exit_Click );
			// 
			// menu_Windows
			// 
			this.menu_Windows.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.menu_Windows_Viewport,
            this.animationEngineToolStripMenuItem,
            this.animationsToolStripMenuItem,
            this.projectExplorerToolStripMenuItem} );
			this.menu_Windows.Name = "menu_Windows";
			this.menu_Windows.Size = new System.Drawing.Size( 62, 20 );
			this.menu_Windows.Text = "Windows";
			// 
			// menu_Windows_Viewport
			// 
			this.menu_Windows_Viewport.Name = "menu_Windows_Viewport";
			this.menu_Windows_Viewport.Size = new System.Drawing.Size( 167, 22 );
			this.menu_Windows_Viewport.Text = "Viewport";
			this.menu_Windows_Viewport.Click += new System.EventHandler( this.menu_Windows_Viewport_Click );
			// 
			// animationEngineToolStripMenuItem
			// 
			this.animationEngineToolStripMenuItem.Name = "animationEngineToolStripMenuItem";
			this.animationEngineToolStripMenuItem.Size = new System.Drawing.Size( 167, 22 );
			this.animationEngineToolStripMenuItem.Text = "Animation Engine";
			this.animationEngineToolStripMenuItem.Click += new System.EventHandler( this.animationEngineToolStripMenuItem_Click );
			// 
			// animationsToolStripMenuItem
			// 
			this.animationsToolStripMenuItem.Name = "animationsToolStripMenuItem";
			this.animationsToolStripMenuItem.Size = new System.Drawing.Size( 167, 22 );
			this.animationsToolStripMenuItem.Text = "Animations";
			this.animationsToolStripMenuItem.Click += new System.EventHandler( this.animationsToolStripMenuItem_Click );
			// 
			// projectExplorerToolStripMenuItem
			// 
			this.projectExplorerToolStripMenuItem.Name = "projectExplorerToolStripMenuItem";
			this.projectExplorerToolStripMenuItem.Size = new System.Drawing.Size( 167, 22 );
			this.projectExplorerToolStripMenuItem.Text = "Project Explorer";
			this.projectExplorerToolStripMenuItem.Click += new System.EventHandler( this.projectExplorerToolStripMenuItem_Click );
			// 
			// menu_Build
			// 
			this.menu_Build.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.menu_Build_RuntimeDiagram} );
			this.menu_Build.Name = "menu_Build";
			this.menu_Build.Size = new System.Drawing.Size( 41, 20 );
			this.menu_Build.Text = "Build";
			// 
			// menu_Build_RuntimeDiagram
			// 
			this.menu_Build_RuntimeDiagram.Name = "menu_Build_RuntimeDiagram";
			this.menu_Build_RuntimeDiagram.Size = new System.Drawing.Size( 166, 22 );
			this.menu_Build_RuntimeDiagram.Text = "Runtime Diagram";
			this.menu_Build_RuntimeDiagram.Click += new System.EventHandler( this.menu_Build_RuntimeDiagram_Click );
			// 
			// menu_Help
			// 
			this.menu_Help.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem} );
			this.menu_Help.Name = "menu_Help";
			this.menu_Help.Size = new System.Drawing.Size( 40, 20 );
			this.menu_Help.Text = "Help";
			// 
			// status
			// 
			this.status.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.status_Message} );
			this.status.Location = new System.Drawing.Point( 0, 555 );
			this.status.Name = "status";
			this.status.Size = new System.Drawing.Size( 692, 22 );
			this.status.TabIndex = 9;
			this.status.Text = "statusStrip1";
			// 
			// status_Message
			// 
			this.status_Message.Name = "status_Message";
			this.status_Message.Size = new System.Drawing.Size( 38, 17 );
			this.status_Message.Text = "Ready";
			// 
			// clock
			// 
			this.clock.Enabled = true;
			this.clock.Interval = 32;
			this.clock.Tick += new System.EventHandler( this.clock_Tick );
			// 
			// ContentPanel
			// 
			this.ContentPanel.Controls.Add( this.dockPanel );
			this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ContentPanel.Location = new System.Drawing.Point( 0, 49 );
			this.ContentPanel.Name = "ContentPanel";
			this.ContentPanel.Size = new System.Drawing.Size( 692, 506 );
			this.ContentPanel.TabIndex = 10;
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size( 152, 22 );
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler( this.aboutToolStripMenuItem_Click );
			// 
			// MainView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 692, 577 );
			this.Controls.Add( this.ContentPanel );
			this.Controls.Add( this.status );
			this.Controls.Add( this.tools );
			this.Controls.Add( this.menu );
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.MainMenuStrip = this.menu;
			this.Name = "MainView";
			this.Text = "TecnoFreak Character Tool";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.MainView_FormClosing );
			this.tools.ResumeLayout( false );
			this.tools.PerformLayout();
			this.menu.ResumeLayout( false );
			this.menu.PerformLayout();
			this.status.ResumeLayout( false );
			this.status.PerformLayout();
			this.ContentPanel.ResumeLayout( false );
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
		private System.Windows.Forms.ToolStrip tools;
		private System.Windows.Forms.ToolStripButton tools_New;
		private System.Windows.Forms.ToolStripButton tools_Open;
		private System.Windows.Forms.ToolStripButton tools_Save;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton tools_AnimationEngine;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
		private System.Windows.Forms.ToolStripButton tools_About;
		private System.Windows.Forms.MenuStrip menu;
		private System.Windows.Forms.ToolStripMenuItem menu_File;
		private System.Windows.Forms.ToolStripMenuItem menu_File_AddModel;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem menu_File_Exit;
		private System.Windows.Forms.ToolStripMenuItem menu_Windows;
		private System.Windows.Forms.ToolStripMenuItem menu_Help;
		private System.Windows.Forms.StatusStrip status;
		private System.Windows.Forms.ToolStripStatusLabel status_Message;
		private System.Windows.Forms.ToolStripMenuItem menu_Windows_Viewport;
		private System.Windows.Forms.Timer clock;
		private System.Windows.Forms.Panel ContentPanel;
		private System.Windows.Forms.ToolStripMenuItem animationEngineToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem animationsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menu_File_NewProject;
		private System.Windows.Forms.ToolStripMenuItem menu_File_OpenProject;
		private System.Windows.Forms.ToolStripMenuItem menu_File_SaveProject;
		private System.Windows.Forms.ToolStripMenuItem projectExplorerToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menu_Build;
		private System.Windows.Forms.ToolStripMenuItem menu_Build_RuntimeDiagram;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
	}
}