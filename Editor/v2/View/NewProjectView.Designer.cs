namespace TecnoFreak.v2.View
{
	partial class NewProjectView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( NewProjectView ) );
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.NameTextBox = new System.Windows.Forms.TextBox();
			this.BrowseLocationButton = new System.Windows.Forms.Button();
			this.LocationComboBox = new System.Windows.Forms.ComboBox();
			this.OKButton = new System.Windows.Forms.Button();
			this.CancelNewProjectButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 12, 9 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 38, 13 );
			this.label1.TabIndex = 0;
			this.label1.Text = "Name:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point( 12, 37 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 51, 13 );
			this.label2.TabIndex = 1;
			this.label2.Text = "Location:";
			// 
			// NameTextBox
			// 
			this.NameTextBox.Location = new System.Drawing.Point( 67, 6 );
			this.NameTextBox.Name = "NameTextBox";
			this.NameTextBox.Size = new System.Drawing.Size( 372, 20 );
			this.NameTextBox.TabIndex = 2;
			this.NameTextBox.TextChanged += new System.EventHandler( this.NameTextBox_TextChanged );
			// 
			// BrowseLocationButton
			// 
			this.BrowseLocationButton.Location = new System.Drawing.Point( 445, 30 );
			this.BrowseLocationButton.Name = "BrowseLocationButton";
			this.BrowseLocationButton.Size = new System.Drawing.Size( 98, 27 );
			this.BrowseLocationButton.TabIndex = 4;
			this.BrowseLocationButton.Text = "Browse...";
			this.BrowseLocationButton.UseVisualStyleBackColor = true;
			this.BrowseLocationButton.Click += new System.EventHandler( this.BrowseLocationButton_Click );
			// 
			// LocationComboBox
			// 
			this.LocationComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.LocationComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
			this.LocationComboBox.FormattingEnabled = true;
			this.LocationComboBox.Location = new System.Drawing.Point( 67, 34 );
			this.LocationComboBox.Name = "LocationComboBox";
			this.LocationComboBox.Size = new System.Drawing.Size( 372, 21 );
			this.LocationComboBox.TabIndex = 5;
			this.LocationComboBox.TextChanged += new System.EventHandler( this.LocationComboBox_TextChanged );
			// 
			// OKButton
			// 
			this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OKButton.Location = new System.Drawing.Point( 342, 73 );
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size( 97, 26 );
			this.OKButton.TabIndex = 6;
			this.OKButton.Text = "OK";
			this.OKButton.UseVisualStyleBackColor = true;
			// 
			// CancelNewProjectButton
			// 
			this.CancelNewProjectButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelNewProjectButton.Location = new System.Drawing.Point( 445, 73 );
			this.CancelNewProjectButton.Name = "CancelNewProjectButton";
			this.CancelNewProjectButton.Size = new System.Drawing.Size( 98, 26 );
			this.CancelNewProjectButton.TabIndex = 7;
			this.CancelNewProjectButton.Text = "Cancel";
			this.CancelNewProjectButton.UseVisualStyleBackColor = true;
			// 
			// NewProjectView
			// 
			this.AcceptButton = this.OKButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size( 556, 112 );
			this.Controls.Add( this.CancelNewProjectButton );
			this.Controls.Add( this.OKButton );
			this.Controls.Add( this.LocationComboBox );
			this.Controls.Add( this.BrowseLocationButton );
			this.Controls.Add( this.NameTextBox );
			this.Controls.Add( this.label2 );
			this.Controls.Add( this.label1 );
			this.Icon = ( ( System.Drawing.Icon ) ( resources.GetObject( "$this.Icon" ) ) );
			this.MaximumSize = new System.Drawing.Size( 572, 148 );
			this.MinimumSize = new System.Drawing.Size( 572, 148 );
			this.Name = "NewProjectView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "New Project";
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox NameTextBox;
		private System.Windows.Forms.Button BrowseLocationButton;
		private System.Windows.Forms.ComboBox LocationComboBox;
		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button CancelNewProjectButton;
	}
}