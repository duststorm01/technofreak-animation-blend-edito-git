namespace TecnoFreak.v2.View
{
	partial class AnimationControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.LayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.Weight = new TFControls.TrackBar.TrackBar();
            this.Speed = new System.Windows.Forms.NumericUpDown();
            this.AnimationName = new System.Windows.Forms.CheckBox();
            this.LayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Speed)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutPanel
            // 
            this.LayoutPanel.AutoSize = true;
            this.LayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LayoutPanel.Controls.Add(this.Weight);
            this.LayoutPanel.Controls.Add(this.Speed);
            this.LayoutPanel.Controls.Add(this.AnimationName);
            this.LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.LayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LayoutPanel.Name = "LayoutPanel";
            this.LayoutPanel.Padding = new System.Windows.Forms.Padding(0, 0, 27, 0);
            this.LayoutPanel.Size = new System.Drawing.Size(263, 33);
            this.LayoutPanel.TabIndex = 0;
            // 
            // Weight
            // 
            this.Weight.Color1 = System.Drawing.Color.DarkRed;
            this.Weight.Color2 = System.Drawing.Color.DarkSalmon;
            this.Weight.Location = new System.Drawing.Point(4, 4);
            this.Weight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Weight.MaxValue = 1F;
            this.Weight.MinValue = 0F;
            this.Weight.Name = "Weight";
            this.Weight.RoundRadius = 2F;
            this.Weight.ShowPercent = true;
            this.Weight.Size = new System.Drawing.Size(85, 25);
            this.Weight.TabIndex = 3;
            this.Weight.TabStop = false;
            this.Weight.Value = 1F;
            this.Weight.ValueChanged += new System.EventHandler(this.Weight_ValueChanged);
            // 
            // Speed
            // 
            this.Speed.DecimalPlaces = 2;
            this.Speed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Speed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.Speed.Location = new System.Drawing.Point(97, 4);
            this.Speed.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Speed.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.Speed.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.Speed.Name = "Speed";
            this.Speed.Size = new System.Drawing.Size(60, 22);
            this.Speed.TabIndex = 2;
            this.Speed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Speed.ValueChanged += new System.EventHandler(this.Speed_ValueChanged);
            // 
            // AnimationName
            // 
            this.AnimationName.AutoSize = true;
            this.AnimationName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AnimationName.Location = new System.Drawing.Point(165, 4);
            this.AnimationName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AnimationName.Name = "AnimationName";
            this.AnimationName.Size = new System.Drawing.Size(67, 25);
            this.AnimationName.TabIndex = 1;
            this.AnimationName.Text = "Name";
            this.AnimationName.UseVisualStyleBackColor = true;
            this.AnimationName.CheckedChanged += new System.EventHandler(this.Name_CheckedChanged);
            // 
            // AnimationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.LayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AnimationControl";
            this.Size = new System.Drawing.Size(263, 33);
            this.LayoutPanel.ResumeLayout(false);
            this.LayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Speed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel LayoutPanel;
		private System.Windows.Forms.NumericUpDown Speed;
		private System.Windows.Forms.CheckBox AnimationName;
		private TFControls.TrackBar.TrackBar Weight;
	}
}
