/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TecnoFreak.v2.Controller.ModelIO;
using WeifenLuo.WinFormsUI.Docking;
using TecnoFreak.v2.Controller.Project;
using System.IO;
using TecnoFreakCommon;

namespace TecnoFreak.v2.View
{
	public partial class MainView : Form
	{


		/*
		 * Constructor 
		 */
		public MainView()
		{
	
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;

			DockPanel.DockLeftPortion = 0.125;



			ViewportView = new ViewportView();
			ViewportView.Show( DockPanel, DockState.DockRight );
			TecnoFreak.v2.Model.AnimationEngine.Nodes.AnimationNode.AnimableLoader = ViewportView.PluginViewport;

			TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskBaseNode.Viewport = ViewportView.PluginViewport;
			TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskBaseNode.AnimableLoader = ViewportView.PluginViewport;
			

			/*
			LogView = new LogView();
			LogView.Show( DockPanel, DockState.DockTop );
			*/

			/*
			ViewportView2 = new ViewportView();
			ViewportView2.Show( DockPanel );
			ViewportView2.Viewport.ViewportMode = "Hidden Line";

			 * */

			Project = new Model.Project( ViewportView.AnimableLoader );
			ProjectExplorerView = new ProjectExplorerView( Project );
			ProjectExplorerView.Show( DockPanel, DockState.DockLeft );


			//ViewportView.CameraController.Model = Model;

			AnimationsView = new AnimationsView( Model );
			//DockPane viewportDockPanel = DockPanel.Panes[ 0 ];
			DockPane viewportDockPanel = ViewportView.Pane;
			
			AnimationsView.Show( viewportDockPanel, DockAlignment.Bottom, 0.5 );
			AnimationsView.Hide();
			
			AboutView = new AboutView();
			AboutView.StartPosition = FormStartPosition.CenterScreen;

			AnimationEngineView = new AnimationEngineView( ViewportView.AnimableLoader.GetAnimable(), ViewportView.AnimableLoader );
			AnimationEngineView.Show( DockPanel );
			Project.ActiveDiagram = AnimationEngineView; // really ugly...

			BoneWeightsView = new BoneWeightsView( ViewportView.AnimableLoader, ViewportView.PluginViewport );
			BoneWeightsView.Show( viewportDockPanel, DockAlignment.Bottom, 0.15 );
			TecnoFreak.v2.Model.AnimationEngine.Nodes.MaskBaseNode.BoneWeightController = BoneWeightsView;

			Project.ProjectHasChanges += new EventHandler( Project_ProjectChanged );
			Project.ProjectSaved += new EventHandler( Project_ProjectSaved );

			WelcomeView welcomeView = new WelcomeView( Project );
			welcomeView.StartPosition = FormStartPosition.CenterScreen;
			DialogResult result = welcomeView.ShowDialog();
			if ( result == DialogResult.Cancel )
			{
				throw new UserExitException();
			}

			//AnimationEngineView2 = new AnimationEngineView2();
			//AnimationEngineView2.Show( DockPanel );
		}

		void Project_ProjectSaved( object sender, EventArgs e )
		{
			EnableSaveButtons( false );
		}

		void Project_ProjectChanged( object sender, EventArgs e )
		{
			EnableSaveButtons( true );
		}


		void EnableSaveButtons( bool enable )
		{
			tools_Save.Enabled = enable;
			menu_File_SaveProject.Enabled = enable;
		}





		/*
		 * event handlers
		 */
		#region event handlers




		/*
		 *  clock event
		 */
		private void clock_Tick( object sender, EventArgs e )
		{
			if ( ViewportView != null )
			{
				ViewportView.Redraw();
			}
		}





		/*
		 * tools Event Handlers
		 */
		#region tools event handlers

		private void tools_New_Click( object sender, EventArgs e )
		{
			new NewProjectTransaction( Project ).Execute();
		}

		private void tools_Open_Click( object sender, EventArgs e )
		{
			new LoadProjectTransaction( Project ).Execute();
		}

		private void tools_Save_Click( object sender, EventArgs e )
		{
			new SaveProjectTransaction( Project ).Execute();
		}

		private void tools_About_Click( object sender, EventArgs e )
		{
			AboutView.Show();
		}

		#endregion





		/*
		 * menu Event Handlers
		 */
		#region menu event handlers

		/*
		 * menu File Event Handlers
		 */
		#region menu file event handlers
		private void menu_File_New_Click( object sender, EventArgs e )
		{
			new NewModelTransaction( ViewportView.AnimableLoader ).Execute();
		}

		private void menu_File_AddModel_Click( object sender, EventArgs e )
		{
			//new LoadModelTransaction( Model ).Execute();
			new AddModelTransaction( Project ).Execute();
		}

		private void menu_File_Save_Click( object sender, EventArgs e )
		{
			//new SaveModelTransaction( Model ).Execute();
		}

		private void menu_File_Exit_Click( object sender, EventArgs e )
		{
			Close();
		}
		#endregion

		/*
		 * menu Window Event Handlers
		 */
		#region menu window event handlers
		private void menu_Windows_Viewport_Click( object sender, EventArgs e )
		{
			ViewportView.Show();
		}

		private void menu_Windows_Viewport2_Click( object sender, EventArgs e )
		{

		}
		#endregion

		#endregion

		#endregion






		/*
		* Attributes 
		*/
		#region Attributes

		private ViewportView m_viewportView = null;
		private ViewportView m_viewportView2;

		private AnimationsView m_animationsView;

		private BoneWeightsView m_boneWeightView;

		private AboutView m_aboutView;

		//private LogView m_logView;

		private AnimationEngineView m_animationEngineView;

		private Model.Project m_project;

		private ProjectExplorerView m_projectExplorerView;

		#endregion


		/*
		 * Properties 
		 */
		#region Properties

		public IAnimable Model
		{
			get { return Project.AnimableLoader.GetAnimable(); }
		}

		public ViewportView ViewportView
		{
			get { return m_viewportView; }
			private set { m_viewportView = value; }
		}

		public ViewportView ViewportView2
		{
			get { return m_viewportView2; }
			private set { m_viewportView2 = value; }
		}

		public AnimationsView AnimationsView
		{
			get { return m_animationsView; }
			protected set { m_animationsView = value; }
		}

		public AboutView AboutView
		{
			get { return m_aboutView; }
			protected set { m_aboutView = value; }
		}

// 		public LogView LogView
// 		{
// 			get { return m_logView; }
// 			protected set { m_logView = value; }
// 		}

		public AnimationEngineView AnimationEngineView
		{
			get { return m_animationEngineView; }
			protected set { m_animationEngineView = value; }
		}

		public BoneWeightsView BoneWeightsView
		{
			get { return m_boneWeightView;  }
			protected set { m_boneWeightView = value; }
		}

		protected DockPanel DockPanel
		{
			get { return dockPanel; }
		}

// 		public Root Root
// 		{
// 			get { return Root.Singleton; }
// 		}

		public Model.Project Project
		{
			get { return m_project; }
			protected set { m_project = value; }
		}

		public ProjectExplorerView ProjectExplorerView
		{
			get { return m_projectExplorerView; }
			protected set { m_projectExplorerView = value; }
		}

		#endregion

		private void animationEngineToolStripMenuItem_Click( object sender, EventArgs e )
		{
			AnimationEngineView.Show();
		}

		private void tools_AnimationEngine_Click( object sender, EventArgs e )
		{
			AnimationEngineView.Show();
		}

		private void animationsToolStripMenuItem_Click( object sender, EventArgs e )
		{
			AnimationsView.Show();
		}

		private void menu_File_NewProject_Click( object sender, EventArgs e )
		{
			new NewProjectTransaction( Project ).Execute();
			Project.MarkSaved();
		}

		private void projectExplorerToolStripMenuItem_Click( object sender, EventArgs e )
		{
			ProjectExplorerView.Show();
		}

		private void menu_File_OpenProject_Click( object sender, EventArgs e )
		{
			new LoadProjectTransaction( Project ).Execute();
			Project.MarkSaved();
		}

		private void menu_File_SaveProject_Click( object sender, EventArgs e )
		{
			new SaveProjectTransaction( Project ).Execute();
		}

		private void menu_Build_RuntimeDiagram_Click( object sender, EventArgs e )
		{
			new SaveForRuntimeTransaction( Project, AnimationEngineView.Diagram ).Execute();
		}

		private void MainView_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( e.CloseReason == CloseReason.UserClosing )
			{
				SaveChangesTransaction svt = new SaveChangesTransaction( Project );
				svt.Execute();

				bool userCancelled = ( svt.OperationSuccess == false );
				if ( userCancelled )
				{
					e.Cancel = true;
				}
			}
		}

		private void dockPanel_ActiveContentChanged(object sender, EventArgs e)
		{

		}

		private void aboutToolStripMenuItem_Click( object sender, EventArgs e )
		{
			AboutView.Show();
		}

	}
}