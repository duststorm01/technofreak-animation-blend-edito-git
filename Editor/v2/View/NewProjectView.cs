/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TecnoFreak.v2.Controller.Project;

namespace TecnoFreak.v2.View
{
	public partial class NewProjectView : Form
	{
		public NewProjectView()
		{
			InitializeComponent();

			if ( Properties.Settings.Default.ProjectLocations != null )
			{
				foreach ( String item in Properties.Settings.Default.ProjectLocations )
				{
					LocationComboBox.Items.Add( item );
				}

				if ( 0 < LocationComboBox.Items.Count )
				{
					LocationComboBox.SelectedIndex = 0;
				}
			}

			OKButton.Enabled = false;
		}

		public String ProjectLocation
		{
			get { return LocationComboBox.Text; }
		}

		public String ProjectName
		{
			get { return NameTextBox.Text; }
		}

		private void BrowseLocationButton_Click( object sender, EventArgs e )
		{
			/*
			 * Better looking file browser control using an OpenFileDialog
			 * Sometimes difficult to select folder that have sub folders -> NOT USING IT T_T
			 *

			OpenFileDialog browserDialog = new OpenFileDialog();

			browserDialog.Title = "Please choose a folder";
			browserDialog.FileName = LocationComboBox.Text;
			browserDialog.CheckFileExists = false;

			browserDialog.FileName = "[Get Folder�]";
			browserDialog.Filter = "Folders|no.files";
			
			*/
			FolderBrowserDialog browserDialog = new FolderBrowserDialog();
			browserDialog.SelectedPath = LocationComboBox.Text;

			DialogResult result = browserDialog.ShowDialog();

			if ( result == DialogResult.OK )
			{
				//String location = Path.GetDirectoryName( browserDialog.FileName );
				String location = browserDialog.SelectedPath;
				if ( location != null && 0 < location.Length )
				{
					LocationComboBox.Text = location;
				}
			} 
			
		}

		private void UpdateLocationComboBox()
		{
			try
			{
				DirectoryInfo projectDirectory = new DirectoryInfo( LocationComboBox.Text );
				String location = LocationComboBox.Text;

				if ( !location.TrimEnd().EndsWith( new String( Path.DirectorySeparatorChar, 1 ) ) )
				{
					location += new String( Path.DirectorySeparatorChar, 1 );
				}

				if ( projectDirectory.Exists )
				{
					bool modifyItems = true;
					if ( LocationComboBox.Items.Contains( location ) )
					{
						modifyItems = ( LocationComboBox.Items.IndexOf( location ) != 0 );
						if ( modifyItems )
						{
							LocationComboBox.Items.Remove( location );
						}
					}
					if ( modifyItems )
					{
						LocationComboBox.Items.Insert( 0, location );

						Properties.Settings.Default.ProjectLocations = new System.Collections.Specialized.StringCollection();

						String[] array = new String[ LocationComboBox.Items.Count ];
						LocationComboBox.Items.CopyTo( array, 0 );
	
						Properties.Settings.Default.ProjectLocations.AddRange( array );
						Properties.Settings.Default.Save();
					}
				}
			}
			catch ( Exception )
			{
				// Do nothing...
			}
		}

		private void LocationComboBox_TextChanged( object sender, EventArgs e )
		{
			UpdateLocationComboBox();
			ValidateProjectName();
		}

		private void ValidateProjectName()
		{
			bool validProjectName = ProjectNameValidator.validName( LocationComboBox.Text, NameTextBox.Text );
			OKButton.Enabled = validProjectName;
		}

		private void NameTextBox_TextChanged( object sender, EventArgs e )
		{
			ValidateProjectName();
		}

	}
}