namespace TecnoFreak.v2.View
{
	partial class AnimationEngineView2
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.diagram1 = new TFControls.Diagram2.Diagram();
			( ( System.ComponentModel.ISupportInitialize ) ( this.diagram1 ) ).BeginInit();
			this.SuspendLayout();
			// 
			// diagram1
			// 
			this.diagram1.Location = new System.Drawing.Point( 0, 0 );
			this.diagram1.Name = "diagram1";
			this.diagram1.Size = new System.Drawing.Size( 485, 435 );
			this.diagram1.TabIndex = 0;
			this.diagram1.TabStop = false;
			// 
			// AnimationEngineView2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size( 485, 435 );
			this.Controls.Add( this.diagram1 );
			this.Name = "AnimationEngineView2";
			this.TabText = "AnimationEngineView2";
			this.Text = "AnimationEngineView2";
			( ( System.ComponentModel.ISupportInitialize ) ( this.diagram1 ) ).EndInit();
			this.ResumeLayout( false );

		}

		#endregion

		private TFControls.Diagram2.Diagram diagram1;
	}
}