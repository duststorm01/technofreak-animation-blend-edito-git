namespace TecnoFreak.v2.View
{
	partial class AnimationsView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AnimationsView ) );
			this.LayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.SuspendLayout();
			// 
			// LayoutPanel
			// 
			this.LayoutPanel.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
						| System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.LayoutPanel.AutoScroll = true;
			this.LayoutPanel.BackColor = System.Drawing.SystemColors.Control;
			this.LayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.LayoutPanel.Location = new System.Drawing.Point( 0, 0 );
			this.LayoutPanel.Name = "LayoutPanel";
			this.LayoutPanel.Size = new System.Drawing.Size( 400, 0 );
			this.LayoutPanel.TabIndex = 0;
			this.LayoutPanel.WrapContents = false;
			// 
			// AnimationsView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 392, 29 );
			this.Controls.Add( this.LayoutPanel );
			this.HideOnClose = true;
			this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
			this.Name = "AnimationsView";
			this.TabText = "AnimationsView";
			this.Text = "Animations";
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel LayoutPanel;
	}
}