/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using TecnoFreakCommon;

namespace TecnoFreak.v2.View
{
	public partial class AnimationControl : UserControl
	{
		public AnimationControl( IAnimation animation )
		{
			InitializeComponent();

			Animation = animation;

			Animation.WeightChanged += new AnimationEventHandler( Animation_WeightChanged );
			Animation.SpeedChanged += new AnimationEventHandler( Animation_SpeedChanged );
			Animation.EnabledChanged += new AnimationEventHandler( Animation_EnabledChanged );

			AnimationName.Text = Animation.Name;

			Speed.Minimum = decimal.MinValue;
			Speed.Maximum = decimal.MaxValue;
		}

		#region Attributes
		private IAnimation m_animation;
		#endregion

		#region Properties
		public IAnimation Animation
		{
			get { return m_animation; }
			protected set { m_animation = value; }
		}
		#endregion

		private void Weight_ValueChanged( object sender, EventArgs e )
		{
			Animation.Weight = Weight.Value;
		}

		private void Speed_ValueChanged( object sender, EventArgs e )
		{
			Animation.Speed = ( float ) Speed.Value;
		}

		private void Name_CheckedChanged( object sender, EventArgs e )
		{
			Animation.Enabled = AnimationName.Checked;
		}

		void Animation_EnabledChanged( object sender, EventArgs e )
		{
			AnimationName.Checked = Animation.Enabled;
		}

		void Animation_SpeedChanged( object sender, EventArgs e )
		{
			Speed.Value = ( decimal ) Animation.Speed;
		}

		void Animation_WeightChanged( object sender, EventArgs e )
		{
			Weight.Value = Animation.Weight;
		}
	}
}
