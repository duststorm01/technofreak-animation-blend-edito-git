namespace TecnoFreak.v2.View
{
	partial class BoneWeightControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Weight = new TFControls.TrackBar.TrackBar();
			this.BoneNameLabel = new System.Windows.Forms.Label();
			( ( System.ComponentModel.ISupportInitialize )( this.Weight ) ).BeginInit();
			this.SuspendLayout();
			// 
			// Weight
			// 
			this.Weight.Color1 = System.Drawing.Color.DarkRed;
			this.Weight.Color2 = System.Drawing.Color.DarkSalmon;
			this.Weight.Location = new System.Drawing.Point( 3, 3 );
			this.Weight.MaxValue = 1F;
			this.Weight.MinValue = 0F;
			this.Weight.Name = "Weight";
			this.Weight.RoundRadius = 2F;
			this.Weight.ShowPercent = true;
			this.Weight.Size = new System.Drawing.Size( 64, 20 );
			this.Weight.TabIndex = 4;
			this.Weight.TabStop = false;
			this.Weight.Value = 1F;
			this.Weight.ValueChanged += new System.EventHandler( this.Weight_ValueChanged );
			// 
			// BoneNameLabel
			// 
			this.BoneNameLabel.AutoSize = true;
			this.BoneNameLabel.Location = new System.Drawing.Point( 73, 7 );
			this.BoneNameLabel.Name = "BoneNameLabel";
			this.BoneNameLabel.Size = new System.Drawing.Size( 63, 13 );
			this.BoneNameLabel.TabIndex = 5;
			this.BoneNameLabel.Text = "Bone Name";
			// 
			// BoneWeightControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.Controls.Add( this.BoneNameLabel );
			this.Controls.Add( this.Weight );
			this.Name = "BoneWeightControl";
			this.Padding = new System.Windows.Forms.Padding( 0, 0, 20, 0 );
			this.Size = new System.Drawing.Size( 201, 26 );
			( ( System.ComponentModel.ISupportInitialize )( this.Weight ) ).EndInit();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private TFControls.TrackBar.TrackBar Weight;
		private System.Windows.Forms.Label BoneNameLabel;

	}
}
