/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace TFControls.TrackBar
{
	public class TrackBar : System.Windows.Forms.PictureBox
	{
		public event EventHandler ValueChanged;

		private float m_value;
		public float Value
		{
			get
			{
				return m_value;
			}

			set
			{

				float clampedValue = Math.Min(MaxValue, Math.Max(MinValue, value));

				if ( Value == clampedValue )
				{
					return;
				}


				m_value = clampedValue;
				if ( ValueChanged != null )
				{
					this.Invalidate();
					ValueChanged( this, new EventArgs() );
				}
			}
		}

		private float m_minValue = 0;
		public float MinValue
		{
			get { return m_minValue; }
			set 
			{ 
				m_minValue = value; 
				ClampValue(); 
			}
		}

		private void ClampValue()
		{
			Value = Math.Min(MaxValue, Math.Max(MinValue, Value));
		}

		private float m_maxValue = 1;
		public float MaxValue
		{
			get { return m_maxValue; }
			set
			{
				m_maxValue = value;
				ClampValue();
			}
		}

		float m_roundRadius;

		public float RoundRadius
		{
			get { return m_roundRadius; }
			set { m_roundRadius = value; }
		}

		Color m_color1;

		public Color Color1
		{
			get { return m_color1; }
			set { m_color1 = value; }
		}
		Color m_color2;

		public Color Color2
		{
			get { return m_color2; }
			set { m_color2 = value; }
		}

		bool m_showPercent;

		public bool ShowPercent
		{
			get { return m_showPercent; }
			set { m_showPercent = value; }
		}

		public TrackBar()
		{
			this.Size = new Size( 40, 15 );
			this.Value = 1;
			RoundRadius = 0;
			this.MouseClick += new System.Windows.Forms.MouseEventHandler( TrackBar_MouseClick );
			this.MouseMove += new System.Windows.Forms.MouseEventHandler( TrackBar_MouseMove );
			this.MouseDown += new System.Windows.Forms.MouseEventHandler( TrackBar_MouseDown );
			//this.BackColor = Color.Transparent;

			this.Location = new Point( 0, 0 );

			Color1 = Color.DarkRed;
			Color2 = Color.DarkSalmon;

			ShowPercent = true;
		}

		void SetValueFromMouseX( float x, float width )
		{
			if ( width <= 0 )
			{
				return;
			}

			float normalisedPoint = x / width;

			Value = MinValue + (normalisedPoint * GetRange());

			this.Invalidate();
		}

		float GetRange()
		{
			float range = Math.Max(0, MaxValue - MinValue);
			return range;
		}

		float GetNormalisedValue()
		{
			return GetNormalisedValue(Value);
		}

		float GetNormalisedValue( float value )
		{
			float range = GetRange();
			if (range == 0)
			{
				return 1;
			}

			return (value - MinValue) / range;
		}

		void TrackBar_MouseDown( object sender, System.Windows.Forms.MouseEventArgs e )
		{
			if ( e.Button == System.Windows.Forms.MouseButtons.Left )
			{
				SetValueFromMouseX((float)e.X, (float)ClientRectangle.Width);
			}
		}

		void TrackBar_MouseClick( object sender, System.Windows.Forms.MouseEventArgs e )
		{
			SetValueFromMouseX((float)e.X, (float)ClientRectangle.Width);
		}

		void TrackBar_MouseMove( object sender, System.Windows.Forms.MouseEventArgs e )
		{
			if ( e.Button == System.Windows.Forms.MouseButtons.Left )
			{
				SetValueFromMouseX((float)e.X, (float)ClientRectangle.Width);
			}
		}

		protected override void OnPaint( System.Windows.Forms.PaintEventArgs pe )
		{
			Rectangle r = new Rectangle( ClientRectangle.Location, ClientRectangle.Size );
			r.Width -= 1;
			r.Height -= 1;
			pe.Graphics.SmoothingMode = SmoothingMode.HighQuality;


			Rectangle r2 = new Rectangle( r.Location, r.Size );

			r2.Width = (int)(r2.Width * GetNormalisedValue() );
			FillRoundRectangle( r, new SolidBrush( Color.FromArgb( 50, Color.Black ) ), pe.Graphics );
			if ( 1 < r2.Width )
			{
				if ( Enabled )
				{
					Brush colorBrush = new LinearGradientBrush( this.ClientRectangle, Color1, Color2, LinearGradientMode.Horizontal );
					FillRoundRectangle( r2, colorBrush, pe.Graphics );
				}
				else
				{
					Brush colorBrush = new LinearGradientBrush( this.ClientRectangle, Color.DarkGray, Color.LightGray, LinearGradientMode.Horizontal );
					
					FillRoundRectangle( r2, colorBrush, pe.Graphics );
					
				}
			}
			DrawRoundRectangle( r, Pens.Black, pe.Graphics );

			if ( ShowPercent )
			{
				Decimal d = new Decimal( Value );
				String ds = d.ToString();
				if ( 4 < ds.Length )
				{
					ds = ds.Remove( 4 );
				}
				pe.Graphics.DrawString( ds, Parent.Font, Brushes.Black, ClientRectangle );
			}

		}

		private void FillRoundRectangle( Rectangle rectangle, Brush brush, Graphics g )
		{
			if ( RoundRadius <= 0 )
			{
				g.FillRectangle( brush, rectangle );
				return;
			}

			float size = RoundRadius * 2f;

			GraphicsPath gp = new GraphicsPath();
			gp.AddArc( rectangle.X, rectangle.Y, size, size, 180, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y, size, size, 270, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y + rectangle.Height - size, size, size, 0, 90 );
			gp.AddArc( rectangle.X, rectangle.Y + rectangle.Height - size, size, size, 90, 90 );
			gp.CloseFigure();
			g.FillPath( brush, gp );
			gp.Dispose();
		}

		protected void DrawRoundRectangle( Rectangle rectangle, Pen pen, Graphics g )
		{
			if ( RoundRadius <= 0 )
			{
				g.DrawRectangle( pen, rectangle );
				return;
			}

			float size = RoundRadius * 2f;

			GraphicsPath gp = new GraphicsPath();
			gp.AddArc( rectangle.X, rectangle.Y, size, size, 180, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y, size, size, 270, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y + rectangle.Height - size, size, size, 0, 90 );
			gp.AddArc( rectangle.X, rectangle.Y + rectangle.Height - size, size, size, 90, 90 );
			gp.CloseFigure();
			g.DrawPath( pen, gp );
			gp.Dispose();
		}

	}
}
