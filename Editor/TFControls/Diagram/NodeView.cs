/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.IO;
using System.Resources;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace TFControls.Diagram
{
	[Serializable]
	public class NodeView
	{
		public NodeView()
		{
			BoundingBox = new Rectangle();
			TitleBoundingBox = new Rectangle();
			BodyBoundingBox = new Rectangle();

			Node.NameChanged += new NodeEventHandler( Node_NameChanged );

			NeedsUpdate = true;
		}

		public NodeView( Node node )
		{
			Node = node;

			BoundingBox = new Rectangle();
			TitleBoundingBox = new Rectangle();
			BodyBoundingBox = new Rectangle();

			Node.NameChanged += new NodeEventHandler( Node_NameChanged );
			
			NeedsUpdate = true;
		}



		#region Operations

		public void Move(Point increment)
		{
			Move(increment.X, increment.Y);
		}

		public void Move(int x, int y)
		{
			Position = new Point(Position.X + x, Position.Y + y);
		}


		public bool IntersectsPoint(int x, int y)
		{
			bool insideX = (BoundingBox.Left <= x && x <= BoundingBox.Right);
			bool insideY = (BoundingBox.Top <= y && y <= BoundingBox.Bottom);

			return (insideX && insideY);
		}

		#endregion





		#region Event Handlers
		private void Node_NameChanged(object sender, EventArgs e)
		{
			NeedsUpdate = true;
		}
		#endregion

		#region Attributes
		private Node m_node;
		private bool m_needsUpdate;
		private Rectangle m_boundingBox;
		private Rectangle m_titleBoundingBox;
		private Rectangle m_bodyBoundingBox;


		private Point m_position;
		#endregion

		#region Properties
		public Node Node
		{
			get { return m_node; }
			private set { m_node = value; }
		}

		public bool NeedsUpdate
		{
			get { return m_needsUpdate; }
			set { m_needsUpdate = value; }
		}

		public Rectangle BoundingBox
		{
			get { return m_boundingBox; }
			set { m_boundingBox = value; }
		}

		public Rectangle TitleBoundingBox
		{
			get { return m_titleBoundingBox; }
			set { m_titleBoundingBox = value; }
		}

		public Rectangle BodyBoundingBox
		{
			get { return m_bodyBoundingBox; }
			set { m_bodyBoundingBox = value; }
		}

		public Point Position
		{
			get { return m_position; }
			set
			{ 
				m_position = value;
				NeedsUpdate = true;
			}
		}
		#endregion

	}
}
