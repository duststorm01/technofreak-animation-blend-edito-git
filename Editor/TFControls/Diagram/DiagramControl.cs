/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.IO;
using System.Resources;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TFControls.Diagram
{
	public class DiagramControl : Panel
	{

		public DiagramControl()
		{
			Diagram = new Diagram();
			Diagram.SizeChanged += new EventHandler( Diagram_SizeChanged );

			Diagram.Connector.InConnectionsUnique = true;
			Diagram.Connector.OutConnectionsUnique = true;


			InitializeComponent();

			Connecting = false;
			ForceUpdate = false;
		}

		void Diagram_SizeChanged( object sender, EventArgs e )
		{
			DiagramImage.Size = new Size( Diagram.Size.Width, Diagram.Size.Height );			
		}

		public void New()
		{
			Diagram.New();
			DiagramImage.Invalidate();
		}

		public void Save( String file )
		{
			try
			{
				XmlSerializer serializer = new XmlSerializer( typeof( Diagram ) );
				using ( FileStream stream = File.OpenWrite( file ) )
				{
					serializer.Serialize( stream, Diagram );
				}
			}
			catch ( Exception e )
			{
				System.Console.WriteLine( "stop here " + e.Message );
			}
			
		}

		public void Load( String file )
		{
			New();
			try
			{
				
			}
			catch ( Exception e )
			{
				System.Console.WriteLine( "stop here " + e.Message );
			}
		}

		public void AddNodeAtMouseLocation( Node node )
		{
			node.NodeView.Position = MouseLocation;
			Diagram.AddNode( node );
			DiagramImage.Invalidate();
		}

		private void InitializeComponent()
		{
			DiagramImage = new PictureBox();
			( ( ISupportInitialize ) ( DiagramImage ) ).BeginInit();

			SuspendLayout();

			DiagramImage.BackColor = SystemColors.AppWorkspace;
			DiagramImage.Location = new Point( 0, 0 );
			DiagramImage.Name = "DiagramImage";
			DiagramImage.Size = new Size( Diagram.Size.Width, Diagram.Size.Height );

			DiagramImage.Paint += new PaintEventHandler( DiagramImage_Paint );
			DiagramImage.MouseClick += new MouseEventHandler( DiagramImage_MouseClick );
			DiagramImage.MouseDown += new MouseEventHandler( DiagramImage_MouseDown );
			DiagramImage.MouseUp += new MouseEventHandler( DiagramImage_MouseUp );
			DiagramImage.MouseMove += new MouseEventHandler( DiagramImage_MouseMove );

			Controls.Add( DiagramImage );
			Size = new Size( 256, 256 );
			( ( ISupportInitialize ) ( DiagramImage ) ).EndInit();
			
			ResumeLayout( false );
		}

		#region Event Handlers
		private void DiagramImage_Paint( object sender, PaintEventArgs e )
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

			if ( ForceUpdate )
			{
				// update all nodes before painting any connection.
				// solves problem when lots of nodes are updated simultaneously
				// and connections are painted before one of the connected nodes
				// is updated.
				foreach ( Node node in Diagram.Nodes )
				{
					node.NodeRenderer.UpdateNode( node.NodeView, e.Graphics, Font );
				}

				ForceUpdate = false;
			}

			foreach ( Node node in Diagram.Nodes )
			{
				node.Register( DiagramImage );
				node.NodeRenderer.RenderConnections( node.NodeView, e.Graphics, Font );
			}

			foreach ( Node node in Diagram.Nodes )
			{
				node.NodeRenderer.RenderNode( node.NodeView, e.Graphics, Font );
			}

			if ( Connecting )
			{
				Debug.Assert( Diagram.SelectedPort != null );
				Port portUnderMouse = Diagram.GetPortByPosition( Diagram.GetNodeByPosition( ConnectingLocation ), ConnectingLocation );

				if ( portUnderMouse != null && portUnderMouse != Diagram.SelectedPort )
				{
					ConnectingLocation = portUnderMouse.PortView.AnchorPoint;
				}

				bool validConnection = Diagram.Connector.ConnectorValidator.ValidConnection( Diagram.SelectedPort, portUnderMouse );

				Diagram.SelectedPort.Parent.NodeRenderer.RenderEndangeredConnections( Diagram.SelectedPort.PortView, portUnderMouse, Diagram.Connector, e.Graphics, Font );
				Diagram.SelectedPort.Parent.NodeRenderer.RenderFreeConnection( Diagram.SelectedPort.PortView, ConnectingLocation, validConnection, e.Graphics, Font );
			}

			//e.Graphics.DrawRectangle( Pens.White, Diagram.SelectionBoundingBox );
		}

		protected void DiagramImage_MouseClick( object sender, MouseEventArgs e )
		{
			MouseLocation = e.Location;

			Focus();

			DiagramImage.Invalidate();
		}

		protected void DiagramImage_MouseMove( object sender, MouseEventArgs e )
		{
			MouseLocation = e.Location;

			if ( e.Button == MouseButtons.Left )
			{
				Diagram.MoveSelectedNodesToPosition( e.Location );

				ConnectingLocation = e.Location;
				DiagramImage.Invalidate();
			}
		}

		protected void DiagramImage_MouseUp( object sender, MouseEventArgs e )
		{
			MouseLocation = e.Location;

			Diagram.AllowMovement = false;

			if ( !Connecting )
			{
				return;
			}

			Node nodeUnderMouse = Diagram.GetNodeByPosition( e.Location );
			Port portUnderMouse = Diagram.GetPortByPosition( nodeUnderMouse, e.Location );

			if ( portUnderMouse != null )
			{
				Diagram.Connector.Connect( Diagram.SelectedPort, portUnderMouse );
			}
			else
			{
				Diagram.Connector.Disconnect( Diagram.SelectedPort );
			}

			Diagram.SelectedPort = portUnderMouse;
			Connecting = false;

			DiagramImage.Invalidate();
		}

		protected void DiagramImage_MouseDown( object sender, MouseEventArgs e )
		{

			MouseLocation = e.Location;

			if ( e.Button == MouseButtons.Right && Connecting )
			{
				Connecting = false;
				DiagramImage.Invalidate();
				return;
			}

			bool addToSelection = ( Control.ModifierKeys == Keys.Control || Control.ModifierKeys == Keys.Shift );

			Node nodeUnderMouse = Diagram.GetNodeByPosition( e.Location );
			if ( nodeUnderMouse == null )
			{
				if ( !addToSelection )
				{
					Diagram.ClearSelectedNodes();
				}
			}
			else
			{
				Diagram.AllowMovement = true;
				Port portUnderMouse = Diagram.GetPortByPosition( nodeUnderMouse, e.Location );

				if ( portUnderMouse != null )
				{
					Diagram.ClearSelectedNodes();
					Diagram.SelectedPort = portUnderMouse;

					if ( e.Button == MouseButtons.Left )
					{
						ConnectingLocation = e.Location;
						Connecting = true;
					}
				}
				else
				{
					if ( addToSelection )
					{
						Diagram.ToggleSelectNodeByPosition( e.Location );
					}
					else
					{
						if ( !nodeUnderMouse.Selected )
						{
							Diagram.ClearSelectedNodes();
						}
						Diagram.SelectNodeByPosition( e.Location );
					}
				}
			}

			DiagramImage.Invalidate();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			if ( e.KeyCode == Keys.Delete )
			{
				Diagram.RemoveSelectedNodes();
				DiagramImage.Invalidate();
			}

 			base.OnKeyDown(e);
		}

		#endregion


		#region Attributes
		private PictureBox m_diagramImage;
		private Diagram m_diagram;

		private bool m_connecting;
		private Point m_connectingLocation;

		private Point m_mouseLocation;

		private bool m_forceUpdate;
		#endregion

		#region Properties
		public PictureBox DiagramImage
		{
			get { return m_diagramImage; }
			private set { m_diagramImage = value; }
		}

		public Diagram Diagram
		{
			get { return m_diagram; }
			private set { m_diagram = value; }
		}

		protected bool Connecting
		{
			get { return m_connecting; }
			set { m_connecting = value; }
		}

		public Point ConnectingLocation
		{
			get { return m_connectingLocation; }
			set { m_connectingLocation = value; }
		}

		public Point MouseLocation
		{
			get { return m_mouseLocation; }
			protected set { m_mouseLocation = value; }
		}

		public bool ForceUpdate
		{
			get { return m_forceUpdate; }
			set { m_forceUpdate = value; }
		}
		#endregion

	}
}
