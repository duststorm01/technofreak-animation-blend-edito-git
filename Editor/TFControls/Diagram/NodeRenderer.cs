/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.IO;
using System.Resources;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace TFControls.Diagram
{
	[Serializable]
	public abstract class NodeRenderer
	{
		public NodeRenderer()
		{
		}

		#region Operations 
		public void RenderConnections( NodeView nodeView, Graphics g, Font f )
		{
			if ( nodeView.NeedsUpdate )
			{
				UpdateNode( nodeView, g, f );
			}

			OnRenderConnections( nodeView, g, f );
		}

		public void RenderNode( NodeView nodeView, Graphics g, Font f )
		{
			if ( nodeView.NeedsUpdate )
			{
				UpdateNode( nodeView, g, f );
			}

			OnRenderNode( nodeView, g, f );
		}

		public void UpdateNode( NodeView nodeView, Graphics g, Font f )
		{
			OnUpdateNode( nodeView, g, f );

			nodeView.NeedsUpdate = false;
		}

		public void RenderFreeConnection( PortView portView, Point location, bool valid, Graphics g, Font f )
		{
			OnDrawFreeConnection( portView, location, valid, g, f );
		}

		public void RenderEndangeredConnections( PortView portView, Port destPort, Connector connector, Graphics g, Font f )
		{
			RenderEndangeredConnection( destPort, portView.Port, connector, g, f );
			RenderEndangeredConnection( portView.Port, destPort, connector, g, f );
		}

		private void RenderEndangeredConnection( Port A, Port B, Connector connector, Graphics g, Font f )
		{
			if ( A == null )
			{
				return;
			}

			bool uniqueConnection = true;
			if ( A.Type == PortType.In )
			{
				uniqueConnection = connector.InConnectionsUnique;
			}
			else if ( A.Type == PortType.Out )
			{
				uniqueConnection = connector.OutConnectionsUnique;
			}
			else
			{
				return;
			}

			if ( B == null || uniqueConnection )
			{
				OnRenderEndangeredConnection( A.PortView, g, f );
			}
		}

		#endregion

		#region Functions To Override

		protected abstract void OnRenderConnections( NodeView nodeView, Graphics g, Font f );

		protected abstract void OnDrawFreeConnection( PortView portView, Point location, bool valid, Graphics g, Font f );

		protected abstract void OnRenderEndangeredConnection( PortView A, Graphics g, Font f );

		protected abstract void OnRenderNode( NodeView nodeView, Graphics g, Font f );

		// This function has to recalculate the bounding boxes of the node and all of its ports.
		protected abstract void OnUpdateNode( NodeView nodeView, Graphics g, Font f );
		
		#endregion

	}
}
