/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TFControls.Diagram
{
	public delegate void NodeEventHandler( object sender, EventArgs e );

	[Serializable]
	public class Node
	{
		public event NodeEventHandler NameChanged;
		public event NodeEventHandler SelectedChanged;
		public event NodeEventHandler Deleted;
		public event EventHandler PortValueChanged;
		public event EventHandler CustomDataChangedEvent;

		public Node()
		{
			Name = "";

			Ports = new Dictionary<String, Port>();
			InPorts = new Dictionary<String, Port>();
			OutPorts = new Dictionary<String, Port>();
			NodeView = new NodeView( this );
			NodeRenderer = new NiceNodeRenderer();

			Deletable = true;
		}

		public Node( String name )
		{
			Name = name;
			Ports = new Dictionary<String, Port>();
			InPorts = new Dictionary<String, Port>();
			OutPorts = new Dictionary<String, Port>();
			NodeView = new NodeView( this );
			NodeRenderer = new NiceNodeRenderer();

			Deletable = true;
		}

		#region Port Operations

		public void Register( Control parentControl )
		{
			foreach ( Port port in Ports.Values )
			{
				port.Register( parentControl );
			}
		}

		public void Unregister()
		{
			foreach ( Port port in Ports.Values )
			{
				port.Unregister();

				foreach ( Port connPort in port.Connections )
				{
					connPort.Connections.Remove( port );
				}

				port.Connections.Clear();
			}

			if ( Deleted != null )
			{
				Deleted( this, new EventArgs() );
			}
		}

		public Port CreatePort( String portName, PortType portType, object userData )
		{
			if ( ContainsPort( portName ) )
			{
				throw new Exception( "Node " + Name + " already contains a Port called " + portName );
			}

			Port port = new Port( this, portName, portType );
			port.UserData = userData;
			Ports[ portName ] = port;
			
			if ( portType == PortType.In )
			{
				InPorts[ portName ] = port;
			}
			else if ( portType == PortType.Out )
			{
				OutPorts[ portName ] = port;
			}

			port.ValueChanged += new EventHandler( port_ValueChanged );

			this.NodeView.NeedsUpdate = true;

			return port;
		}

		void port_ValueChanged( object sender, EventArgs e )
		{
			if ( PortValueChanged != null )
			{
				PortValueChanged( sender, e );
			}
		}

		protected void CustomDataChanged()
		{
			if ( CustomDataChangedEvent != null )
			{
				CustomDataChangedEvent(this, EventArgs.Empty);
			}
		}

		public bool ContainsPort( String portName )
		{
			return Ports.ContainsKey( portName );
		}

		public bool ContainsInPort( String portName )
		{
			return InPorts.ContainsKey( portName );
		}

		public bool ContainsOutPort( String portName )
		{
			return OutPorts.ContainsKey( portName );
		}

		public Port GetPort( String portName )
		{
			if ( !ContainsPort( portName ) )
			{
				throw new Exception( "Node " + Name + " doesn't contain a Port called " + portName );
			}

			return Ports[ portName ];
		}

		public Port GetInPort( String portName )
		{
			if ( !ContainsInPort( portName ) )
			{
				throw new Exception( "Node " + Name + " doesn't contain an In Port called " + portName );
			}

			return InPorts[ portName ];
		}

		public Port GetOutPort( String portName )
		{
			if ( !ContainsOutPort( portName ) )
			{
				throw new Exception( "Node " + Name + " doesn't contain an Out Port called " + portName );
			}

			return OutPorts[ portName ];
		}

		#endregion

		#region Attributes
		private String m_name;
		private bool m_deletable;
		private bool m_movable;
		private bool m_selected;
		private Dictionary<String, Port> m_ports;
		private Dictionary<String, Port> m_inPorts;
		private Dictionary<String, Port> m_outPorts;

		[NonSerialized]
		private NodeRenderer m_nodeRenderer;
		private NodeView m_nodeView;
		#endregion

		#region Properties
		public String Name
		{
			get { return m_name; }
			set
			{
				if ( Name == value )
				{
					return;
				}

				m_name = value;
				if ( NameChanged != null )
				{
					NameChanged( this, new EventArgs() );
				}
			}
		}

		public bool Deletable
		{
			get { return m_deletable; }
			set { m_deletable = value; }
		}

		public bool Movable
		{
			get { return m_movable; }
			set { m_movable = value; }
		}

		[XmlIgnore]
		public bool Selected
		{
			get { return m_selected; }
			set
			{
				if ( Selected == value )
				{
					return;
				}

				m_selected = value;
				if ( SelectedChanged != null )
				{
					SelectedChanged( this, new EventArgs() );
				}
			}
		}

		[XmlIgnore]
		public Dictionary<String, Port> Ports
		{
			get { return m_ports; }
			private set { m_ports = value; }
		}

		[XmlIgnore]
		public Dictionary<String, Port> InPorts
		{
			get { return m_inPorts; }
			private set { m_inPorts = value; }
		}

		[XmlIgnore]
		public Dictionary<String, Port> OutPorts
		{
			get { return m_outPorts; }
			private set { m_outPorts = value; }
		}

		[XmlIgnore]
		public NodeRenderer NodeRenderer
		{
			get { return m_nodeRenderer; }
			set { m_nodeRenderer = value; }
		}

		[XmlIgnore]
		public NodeView NodeView
		{
			get { return m_nodeView; }
			set { m_nodeView = value; }
		}
		#endregion
	}
}
