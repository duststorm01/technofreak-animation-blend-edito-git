/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace TFControls.Diagram
{
	[Serializable]
	public class ConnectorValidator
	{
		public ConnectorValidator()
		{
		}

		public virtual bool ValidConnection( Port A, Port B )
		{
			if ( A == null )
			{
				return false;
			}

			if ( B == null )
			{
				return false;
			}

			if ( A == B )
			{
				return false;
			}

			if ( A.Type == B.Type )
			{
				return false;
			}

			if ( A.Parent == B.Parent )
			{
				return false;
			}

			return true;
		}
	}
}
