/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Xml.Serialization;

namespace TFControls.Diagram
{
	public class Diagram : IXmlSerializable
	{
		public event EventHandler NodeMoved;
		public event EventHandler NodeCreated;
		public event EventHandler NodeDeleted;
		public event EventHandler PortConnected;
		public event EventHandler PortDisconnected;
		public event EventHandler PortValueChanged;
		public event EventHandler CustomDataChangedEvent;
		public event EventHandler DiagramChanged;

		public event EventHandler SizeChanged;

		public Diagram()
		{
			Size = new Size( 1024, 1024 );
			Nodes = new List<Node>();
			SelectedNodes = new List<Node>();
			Connector = new Connector();
			Connector.PortConnected += new EventHandler( Connector_PortConnected );
			Connector.PortDisconnected += new EventHandler( Connector_PortDisconnected );

			NodeMoved += new EventHandler( onDiagramChange );
			NodeCreated += new EventHandler( onDiagramChange );
			NodeDeleted += new EventHandler( onDiagramChange );
			PortConnected += new EventHandler( onDiagramChange );
			PortDisconnected += new EventHandler( onDiagramChange );
			PortValueChanged += new EventHandler( onDiagramChange );
			CustomDataChangedEvent += new EventHandler( onDiagramChange );

			AllowMovement = true;
		}

		void onDiagramChange( object sender, EventArgs e )
		{
			if ( DiagramChanged != null )
			{
				DiagramChanged( sender, e );
			}
		}

		void Connector_PortDisconnected( object sender, EventArgs e )
		{
			if ( PortDisconnected != null )
			{
				PortDisconnected( sender, e );
			}
		}

		void Connector_PortConnected( object sender, EventArgs e )
		{
			if ( PortConnected != null )
			{
				PortConnected( sender, e );
			}
		}

		public void New()
		{
			ClearSelectedNodes();
			foreach ( Node node in Nodes )
			{
				node.Unregister();
			}
			Nodes.Clear();
		}

		public void Save()
		{
		}

		public void Load()
		{
		}

		public void AddNode( Node node )
		{
			Nodes.Add( node );

			node.PortValueChanged += new EventHandler( node_PortValueChanged );
			node.CustomDataChangedEvent += new EventHandler(node_CustomDataChanged);

			if ( NodeCreated != null )
			{
				NodeCreated( this, new EventArgs() );
			}
		}

		void node_PortValueChanged( object sender, EventArgs e )
		{
			if ( PortValueChanged != null )
			{
				PortValueChanged( sender, e );
			}
		}

		void node_CustomDataChanged(object sender, EventArgs e)
		{
			if ( CustomDataChangedEvent != null )
			{
				CustomDataChangedEvent( sender, e );
			}
		}

		public void RemoveNode( Node node )
		{
			if ( ! node.Deletable )
			{
				return;
			}

			RemoveNodeFromSelection( node );
			Nodes.Remove( node );
			node.Unregister();

			if ( NodeDeleted != null )
			{
				NodeDeleted( this, new EventArgs() );
			}
		}

		public void BringToFront( Node node )
		{
			Nodes.Remove( node );
			Nodes.Add( node );
		}

		public void AddNodeToSelection( Node node )
		{
			SelectedPort = null;

			if ( SelectedNodes.Contains( node ) )
			{
				return;
			}

			SelectedNodes.Add( node );
			node.Selected = true;

			BringToFront( node );

			UpdateSelectionBoundingBox();
		}

		public void RemoveNodeFromSelection( Node node )
		{
			if ( !SelectedNodes.Contains( node ) )
			{
				return;
			}

			node.Selected = false;
			SelectedNodes.Remove( node );

			UpdateSelectionBoundingBox();
		}

		public void ClearSelectedNodes()
		{
			foreach ( Node node in SelectedNodes )
			{
				node.Selected = false;
			}
			SelectedNodes.Clear();

			UpdateSelectionBoundingBox();
		}

		private void UpdateSelectionBoundingBox()
		{
			if ( SelectedNodes.Count == 0 )
			{
				SelectionBoundingBox = new Rectangle();
				return;
			}

			int minX = Size.Width;
			int minY = Size.Height;
			int maxX = 0;
			int maxY = 0;

			foreach ( Node node in SelectedNodes )
			{
				Rectangle nodeBoundingBox = node.NodeView.BoundingBox;

				if ( nodeBoundingBox.Left < minX )
				{
					minX = nodeBoundingBox.Left;
				}
				if ( maxX < nodeBoundingBox.Right )
				{
					maxX = nodeBoundingBox.Right;
				}

				if ( nodeBoundingBox.Top < minY )
				{
					minY = nodeBoundingBox.Top;
				}
				if ( maxY < nodeBoundingBox.Bottom )
				{
					maxY = nodeBoundingBox.Bottom;
				}
			}

			Debug.Assert( minX < maxX );
			Debug.Assert( minY < maxY );

			int sizeX = maxX - minX;
			int sizeY = maxY - minY;

			SelectionBoundingBox = new Rectangle( minX, minY, sizeX, sizeY );
		}

		public Node SelectNodeByPosition( Point position )
		{
			return SelectNodeByPosition( position.X, position.Y );
		}

		public Node SelectNodeByPosition( int x, int y )
		{
			Node node = GetNodeByPosition( x, y );

			if ( node != null )
			{
				AddNodeToSelection( node );
				UpdateSelectionOffset( x, y );
			}

			return node;
		}

		public Node ToggleSelectNodeByPosition( Point position )
		{
			return ToggleSelectNodeByPosition( position.X, position.Y );
		}

		private Node ToggleSelectNodeByPosition( int x, int y )
		{
			Node node = GetNodeByPosition( x, y );

			if ( node != null )
			{
				if ( SelectedNodes.Contains( node ) )
				{
					RemoveNodeFromSelection( node );
				}
				else
				{
					AddNodeToSelection( node );
					UpdateSelectionOffset( x, y );
				}
			}

			return node;
		}

		public Node GetNodeByPosition( Point position )
		{
			return GetNodeByPosition( position.X, position.Y );	
		}

		public Node GetNodeByPosition( int x, int y )
		{
			Rectangle point = new Rectangle( x, y, 1, 1 );
			foreach ( Node node in ReverseIterator( Nodes ) )
			{
				if ( node.NodeView.BoundingBox.IntersectsWith( point ) )
				{
					return node;
				}
			}
			return null;
		}

		public Port GetPortByPosition( Node nodeToCheck, Point position )
		{
			Rectangle point = new Rectangle( position, new Size( 1, 1 ) );
			return GetPortByPosition( nodeToCheck, position.X, position.Y );
		}

		public Port GetPortByPosition( Node nodeToCheck, int x, int y )
		{
			if ( nodeToCheck == null )
			{
				return null;
			}

			Rectangle point = new Rectangle( x, y, 1, 1 );

			foreach ( Port port in nodeToCheck.Ports.Values )
			{
				PortView portView = port.PortView;
				if ( portView.BoundingBox.IntersectsWith( point ) )
				{
					return port;
				}
			}

			return null;
		}

		public void MoveSelectedNodesToPosition( Point position )
		{
			MoveSelectedNodesToPosition( position.X, position.Y );
		}

		public void MoveSelectedNodesToPosition( int x, int y )
		{
			if ( !AllowMovement )
			{
				return;
			}

			if ( SelectedNodes.Count == 0 )
			{
				return;
			}

			Point minPosition = new Point( 0, 0 );
			Point maxPosition = new Point( Size.Width - SelectionBoundingBox.Width, Size.Height - SelectionBoundingBox.Height );

			Point desiredPosition = new Point( x - SelectionOffset.X, y - SelectionOffset.Y );

			int destinationX = Math.Max( minPosition.X, Math.Min( desiredPosition.X, maxPosition.X ) );
			int destinationY = Math.Max( minPosition.Y, Math.Min( desiredPosition.Y, maxPosition.Y ) );

			int movementX = destinationX - SelectionBoundingBox.X;
			int movementY = destinationY - SelectionBoundingBox.Y;

			foreach ( Node node in SelectedNodes )
			{
				node.NodeView.Move( movementX, movementY );
			}

			SelectionBoundingBox = new Rectangle( destinationX, destinationY, SelectionBoundingBox.Width, SelectionBoundingBox.Height );

			if ( NodeMoved != null )
			{
				NodeMoved( this, new EventArgs() );
			}
		}

		public void RemoveSelectedNodes()
		{
			foreach ( Node node in SelectedNodes )
			{
				if ( node.Deletable )
				{
					Nodes.Remove( node );
					node.Unregister();
				}
			}

			ClearSelectedNodes();

			if ( NodeDeleted != null )
			{
				NodeDeleted( this, new EventArgs() );
			}
		}

		private void UpdateSelectionOffset( int x, int y )
		{
			SelectionOffset = new Point( x - SelectionBoundingBox.X, y - SelectionBoundingBox.Y );
		}

		#region Attributes
		private Size m_size;

		private List<Node> m_nodes;


		private List<Node> m_selectedNodes;

		private Rectangle m_selectionBoundingBox;
		private Point m_selectionOffset;

		private Connector m_connector;

		private Port m_selectedPort;

		private bool m_allowMovement;
		#endregion

		#region Properties
		public Size Size
		{
			get { return m_size; }
			set 
			{
				if ( value == m_size )
				{
					return;
				}

				m_size = value;
				if ( SizeChanged != null )
				{
					SizeChanged( this, null );
				}
			}
		}

		public List<Node> Nodes
		{
			get { return m_nodes; }
			set { m_nodes = value; }
		}

		[XmlIgnore]
		public List<Node> SelectedNodes
		{
			get { return m_selectedNodes; }
			private set { m_selectedNodes = value; }
		}

		[XmlIgnore]
		public Point SelectionOffset
		{
			get { return m_selectionOffset; }
			set { m_selectionOffset = value; }
		}

		[XmlIgnore]
		public Rectangle SelectionBoundingBox
		{
			get { return m_selectionBoundingBox; }
			private set { m_selectionBoundingBox = value; }
		}

		[XmlIgnore]
		public Connector Connector
		{
			get { return m_connector; }
			set { m_connector = value; }
		}

		[XmlIgnore]
		public Port SelectedPort
		{
			get { return m_selectedPort; }
			set
			{
				if ( SelectedPort == value )
				{
					return;
				}

				if ( SelectedPort != null )
				{
					SelectedPort.Selected = false;
				}
				m_selectedPort = value;
				if ( SelectedPort != null )
				{
					SelectedPort.Selected = true;
				}
			}
		}

		public bool AllowMovement
		{
			get { return m_allowMovement; }
			set { m_allowMovement = value; }
		}
		#endregion


		static IEnumerable<T> ReverseIterator<T>( IList<T> list )
		{
			for ( int i = list.Count - 1; 0 <= i; i-- )
			{
				yield return list[ i ];
			}
		}


		#region IXmlSerializable Members

		public System.Xml.Schema.XmlSchema GetSchema()
		{
			return null;
		}

		public void ReadXml( System.Xml.XmlReader reader )
		{
			
		}

		public void WriteXml( System.Xml.XmlWriter writer )
		{
			throw new Exception( "The method or operation is not implemented." );

		}

		#endregion
	}
}
