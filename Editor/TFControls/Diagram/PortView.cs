/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.IO;
using System.Resources;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace TFControls.Diagram
{
	public delegate void PortViewEventHandler( object sender, EventArgs e );

	[Serializable]
	public class PortView
	{
		public event PortViewEventHandler UpdateRequested;

		public PortView()
		{
			BoundingBox = new Rectangle();
			NameBoundingBox = new Rectangle();
			ConnectorBoundingBox = new Rectangle();
			ValueBoundingBox = new Rectangle();

			NeedsUpdate = false;
		}

		public PortView( Port port )
		{
			Port = port;

			BoundingBox = new Rectangle();
			NameBoundingBox = new Rectangle();
			ConnectorBoundingBox = new Rectangle();
			ValueBoundingBox = new Rectangle();

			NeedsUpdate = false;
		}

		private Port m_port;
		private Rectangle m_boundingBox;
		private Rectangle m_connectorBoundingBox;
		private Rectangle m_nameBoundingBox;
		private Rectangle m_valueBoundingBox;
		private bool m_needsUpdate;
		private Point m_anchorPoint;


		public Port Port
		{
			get { return m_port; }
			set { m_port = value; }
		}
		public Rectangle BoundingBox
		{
			get { return m_boundingBox; }
			set { m_boundingBox = value; }
		}
		public Rectangle ConnectorBoundingBox
		{
			get { return m_connectorBoundingBox; }
			set { m_connectorBoundingBox = value; }
		}
		public Rectangle NameBoundingBox
		{
			get { return m_nameBoundingBox; }
			set { m_nameBoundingBox = value; }
		}
		public Rectangle ValueBoundingBox
		{
			get { return m_valueBoundingBox; }
			set { m_valueBoundingBox = value; }
		}


		public Point AnchorPoint
		{
			get { return m_anchorPoint; }
			set { m_anchorPoint = value; }
		}

		public bool NeedsUpdate
		{
			get { return m_needsUpdate; }
			set
			{
				if ( NeedsUpdate == value )
				{
					return;
				}

				m_needsUpdate = value;
				if ( NeedsUpdate && UpdateRequested != null )
				{
					UpdateRequested( this, new EventArgs() );
				}
			}
		}
	}
}
