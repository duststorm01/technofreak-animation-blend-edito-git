/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace TFControls.Diagram
{
	public delegate void PortEventHandler( object sender, EventArgs e );

	public enum PortType
	{
		In,
		Out
	}

	[Serializable]
	public class Port
	{
		public event PortEventHandler SelectedChanged;
		public event EventHandler ValueChanged;

		public Port()
		{
			PortView = new PortView( this );
			Connections = new List<Port>();

			Selected = false;

			ResetPortName();
		}

		public Port( Node parent, String name, PortType type )
		{
			Parent = parent;
			Name = name;

			PortView = new PortView( this );
			Connections = new List<Port>();

			Type = type;

			Selected = false;

			ResetPortName();
		}

		public void ResetPortName()
		{
			m_portWorkParameterName = "tfwp_" + System.Guid.NewGuid().ToString();
		}

		public void Register( Control parentControl )
		{
			if ( ValueControl == null )
			{
				return;
			}

			if ( ValueControl.Control != null )
			{
				ValueControl.ValueChanged -= new EventHandler( Port_ValueChanged );
				ValueControl.ValueChanged += new EventHandler( Port_ValueChanged );

				ValueControl.Control.Parent = parentControl;

				if ( this.Type == PortType.In )
				{
					ValueControl.NotifyConnectionChange( this.Connections.Count );
				}

			}

			
		}

		void Port_ValueChanged( object sender, EventArgs e )
		{
			if ( ValueChanged != null )
			{
				ValueChanged( this, new EventArgs() );
			}
		}

		public void Unregister()
		{
			if ( ValueControl == null )
			{
				return;
			}

			if ( ValueControl.Control != null )
			{
				ValueControl.Control.Parent = null;
			}
		}

		#region Attributes
		private String m_name;
		private bool m_selected;
		private Node m_parent;
		private PortType m_type;
		private PortView m_portView;

		String m_portWorkParameterName;

		private List<Port> m_connections;

		private object m_userData;

		private PortControl.IPortControl m_valueControl;
		#endregion

		#region Properties
		public String Name
		{
			get { return m_name; }
			private set { m_name = value; }
		}

		public bool Selected
		{
			get { return m_selected; }
			set
			{
				if ( Selected == value )
				{
					return;
				}

				m_selected = value;
				if ( SelectedChanged != null )
				{
					SelectedChanged( this, new EventArgs() );
				}
			}
		}

		public Node Parent
		{
			get { return m_parent; }
			private set { m_parent = value; }
		}

		public PortType Type
		{
			get { return m_type; }
			private set { m_type = value; }
		}

		public PortView PortView
		{
			get { return m_portView; }
			set { m_portView = value; }
		}

		public List<Port> Connections
		{
			get { return m_connections; }
			protected set { m_connections = value; }
		}

		public object UserData
		{
			get { return m_userData; }
			set { m_userData = value; }
		}

		public PortControl.IPortControl ValueControl
		{
			get { return m_valueControl; }
			set { m_valueControl = value; }
		}

		public String PortWorkParameterName
		{
			get { return m_portWorkParameterName; }
			set { m_portWorkParameterName = value; }
		}
		#endregion
	}
}
