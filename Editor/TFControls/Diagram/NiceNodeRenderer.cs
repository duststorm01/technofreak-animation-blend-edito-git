/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.IO;
using System.Resources;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;


namespace TFControls.Diagram
{
	[Serializable]
	public class NiceNodeRenderer : DebugNodeRenderer
	{
		Pen m_connectionPen;
		Pen m_shadowPen;
		Pen m_freeConnectionPen;
		Pen m_invalidConnectionPen;
		Pen m_endangeredConnectionPen;

		Brush m_titleTextBrush;
		Brush m_portTextBrush;

		Brush m_shadowBrush;
		Color m_titleColor1;

		Pen m_outlinePen;
		Pen m_selectedOutlinePen;

		public Pen SelectedOutlinePen
		{
			get { return m_selectedOutlinePen; }
			set { m_selectedOutlinePen = value; }
		}

		public Pen OutlinePen
		{
			get { return m_outlinePen; }
			set { m_outlinePen = value; }
		}

		public Color TitleColor1
		{
			get { return m_titleColor1; }
			set { m_titleColor1 = value; }
		}
		Color m_titleColor2;

		public Color TitleColor2
		{
			get { return m_titleColor2; }
			set { m_titleColor2 = value; }
		}
		Color m_bodyColor1;

		public Color BodyColor1
		{
			get { return m_bodyColor1; }
			set { m_bodyColor1 = value; }
		}
		Color m_bodyColor2;

		public Color BodyColor2
		{
			get { return m_bodyColor2; }
			set { m_bodyColor2 = value; }
		}

		float m_roundRadius;

		public float RoundRadius
		{
			get { return m_roundRadius; }
			set { m_roundRadius = value; }
		}


		public Brush ShadowBrush
		{
			get { return m_shadowBrush; }
			set { m_shadowBrush = value; }
		}

		public Brush PortTextBrush
		{
			get { return m_portTextBrush; }
			set { m_portTextBrush = value; }
		}

		public Brush TitleTextBrush
		{
			get { return m_titleTextBrush; }
			set { m_titleTextBrush = value; }
		}


		public Pen EndangeredConnectionPen
		{
			get { return m_endangeredConnectionPen; }
			set { m_endangeredConnectionPen = value; }
		}

		public Pen FreeConnectionPen
		{
			get { return m_freeConnectionPen; }
			set { m_freeConnectionPen = value; }
		}

		public Pen InvalidConnectionPen
		{
			get { return m_invalidConnectionPen; }
			set { m_invalidConnectionPen = value; }
		}

		public Pen ShadowPen
		{
			get { return m_shadowPen; }
			set { m_shadowPen = value; }
		}

		public Pen ConnectionPen
		{
			get { return m_connectionPen; }
			set { m_connectionPen = value; }
		}


		public NiceNodeRenderer()
		{
			ConnectionPen = new Pen( Color.Black, 2 );
			ShadowPen = new Pen( Color.FromArgb( 44, Color.Black ), 2 );
			FreeConnectionPen = new Pen( Color.White, 2 );
			InvalidConnectionPen = new Pen( Color.DarkRed, 2 );
			EndangeredConnectionPen = new Pen( Color.DarkGray, 2 );

			TitleTextBrush = new SolidBrush( Color.White );
			PortTextBrush = new SolidBrush( Color.Black );

			ShadowBrush = new SolidBrush( ShadowPen.Color );

			TitleColor1 = Color.Black;
			TitleColor2 = Color.DarkGray;

			BodyColor1 = Color.LightGray;
			BodyColor2 = Color.White;

			OutlinePen = new Pen( Color.Black );
			SelectedOutlinePen = new Pen( Color.Black, 2 );

			RoundRadius = 5;
		}


		protected override void OnRenderConnections( NodeView nodeView, System.Drawing.Graphics g, System.Drawing.Font f )
		{
			foreach ( Port In in nodeView.Node.InPorts.Values )
			{
				foreach ( Port Out in In.Connections )
				{
					RenderConnection( Out.PortView.AnchorPoint, In.PortView.AnchorPoint, ConnectionPen, g );
				}
			}
		}

		protected override void OnDrawFreeConnection( PortView portView, Point location, bool valid, Graphics g, Font f )
		{
			Pen pen = ( valid ) ? FreeConnectionPen : InvalidConnectionPen;

			if ( portView.Port.Type == PortType.Out )
			{
				RenderConnection( portView.AnchorPoint, location, pen, g );
			}
			else
			{
				RenderConnection( location, portView.AnchorPoint, pen, g );
			}
		}

		protected override void OnRenderEndangeredConnection( PortView A, Graphics g, Font f )
		{
			foreach ( Port B in A.Port.Connections )
			{
				if ( A.Port.Type == PortType.Out )
				{
					RenderConnection( A.AnchorPoint, B.PortView.AnchorPoint, EndangeredConnectionPen, g );
				}
				else
				{
					RenderConnection( B.PortView.AnchorPoint, A.AnchorPoint, EndangeredConnectionPen, g );
				}
			}
		}

		protected override void OnRenderNode( NodeView nodeView, Graphics g, Font f )
		{
			DrawShadow( nodeView, g, f );
			DrawBody( nodeView, g, f );
			DrawTitle( nodeView, g, f );
			
			DrawOutline( nodeView, g, f );
			DrawPorts( nodeView, g, f );
		}

		private void DrawOutline( NodeView nodeView, Graphics g, Font f )
		{
			Pen pen = ( nodeView.Node.Selected ) ? SelectedOutlinePen : OutlinePen;
			
			DrawRoundRectangle( nodeView.BodyBoundingBox, pen, g );			
		}

		private void DrawPorts( NodeView nodeView, Graphics g, Font f )
		{
			Node node = nodeView.Node;
			foreach ( Port port in node.Ports.Values )
			{
				PortView portView = port.PortView;


				g.DrawString( port.Name, f, PortTextBrush, portView.NameBoundingBox.X, portView.NameBoundingBox.Y );

				Image portImage = Properties.Resources.PortImage2;
				g.DrawImage( portImage, portView.ConnectorBoundingBox );
				//FillRoundRectangle( portView.ConnectorBoundingBox, PortTextBrush, g );
			}
		}

		private void DrawBody( NodeView nodeView, Graphics g, Font f )
		{
			LinearGradientBrush bodyBrush = new LinearGradientBrush( nodeView.BodyBoundingBox, BodyColor1, BodyColor2, LinearGradientMode.Vertical );
			FillRoundRectangle( nodeView.BodyBoundingBox, bodyBrush, g );
		}
		
		private void DrawTitle( NodeView nodeView, Graphics g, Font f )
		{
			LinearGradientBrush titleBrush = new LinearGradientBrush( nodeView.TitleBoundingBox, TitleColor1, TitleColor2, LinearGradientMode.Vertical );
			FillRoundRectangle( nodeView.TitleBoundingBox, titleBrush, g );

			
			g.DrawString( nodeView.Node.Name, f, TitleTextBrush, nodeView.TitleBoundingBox.Location );
		}

		private void DrawShadow( NodeView nodeView, Graphics g, Font f )
		{
			Rectangle body = nodeView.BodyBoundingBox;
			Rectangle shadowRectangle = new Rectangle( body.Location, body.Size );

			for ( int i = 0; i < 3; i++ )
			{
				shadowRectangle.Offset( 1, 1 );
				FillRoundRectangle( shadowRectangle, ShadowBrush, g );
			}
		}

		private void FillRoundRectangle( Rectangle rectangle, Brush brush, Graphics g )
		{
			if ( RoundRadius <= 0 )
			{
				g.FillRectangle( brush, rectangle );
				return;
			}

			float size = RoundRadius * 2f;

			GraphicsPath gp = new GraphicsPath();
			gp.AddArc( rectangle.X, rectangle.Y, size, size, 180, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y, size, size, 270, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y + rectangle.Height - size, size, size, 0, 90 );
			gp.AddArc( rectangle.X, rectangle.Y + rectangle.Height - size, size, size, 90, 90 );
			gp.CloseFigure();
			g.FillPath( brush, gp );
			gp.Dispose();
		}

		protected void DrawRoundRectangle( Rectangle rectangle, Pen pen, Graphics g )
		{
			if ( RoundRadius <= 0 )
			{
				g.DrawRectangle( pen, rectangle );
				return;
			}

			float size = RoundRadius * 2f;

			GraphicsPath gp = new GraphicsPath();
			gp.AddArc( rectangle.X, rectangle.Y, size, size, 180, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y, size, size, 270, 90 );
			gp.AddArc( rectangle.X + rectangle.Width - size, rectangle.Y + rectangle.Height - size, size, size, 0, 90 );
			gp.AddArc( rectangle.X, rectangle.Y + rectangle.Height - size, size, size, 90, 90 );
			gp.CloseFigure();
			g.DrawPath( pen, gp );
			gp.Dispose();
		}



		void RenderConnection( Point start, Point end, Pen pen, Graphics g )
		{
			int distance = 40;
			int shadowDistance = 6;

			Point A = start;
			Point B = new Point( A.X + distance, A.Y );
			Point BS = new Point( A.X + distance + shadowDistance, A.Y + shadowDistance );
			
			Point D = end;
			Point C = new Point( D.X - distance, D.Y );
			Point CS = new Point( D.X - distance - shadowDistance, D.Y + shadowDistance );

			g.DrawBezier( ShadowPen, A, BS, CS, D );
			g.DrawBezier( pen, A, B, C, D );
		}

		Size GetControlSize( Port port )
		{
			if ( port.ValueControl == null || port.ValueControl.Control == null )
			{
				return new Size( 0, 0 );
			}



			return port.ValueControl.Control.Size;
		}

		
	}
}
