/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.IO;
using System.Resources;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace TFControls.Diagram
{
	[Serializable]
	public class DebugNodeRenderer : NodeRenderer
	{
		#region Ugly as hell debug rendering
		protected override void OnRenderConnections( NodeView nodeView, Graphics g, Font f )
		{
			foreach ( Port In in nodeView.Node.InPorts.Values )
			{
				Point inPoint = In.PortView.AnchorPoint;
				Point inPointB = new Point( inPoint.X - 40, inPoint.Y );
				foreach ( Port Out in In.Connections )
				{
					Point outPoint = Out.PortView.AnchorPoint;
					Point outPointB = new Point( outPoint.X + 40, outPoint.Y );
					g.DrawBezier( Pens.Ivory, inPoint, inPointB, outPointB, outPoint );
				}
			}
		}

		protected override void OnDrawFreeConnection( PortView portView, Point location, bool valid, Graphics g, Font f )
		{
			Point inPoint = portView.AnchorPoint;
			Point outPoint = location;
			if ( portView.Port.Type == PortType.Out )
			{
				inPoint = location;
				outPoint = portView.AnchorPoint;
			}


			Point inPointB = new Point( inPoint.X - 40, inPoint.Y );
			Point outPointB = new Point( outPoint.X + 40, outPoint.Y );

			if ( valid )
			{
				g.DrawBezier( Pens.Ivory, inPoint, inPointB, outPointB, outPoint );
			}
			else
			{
				g.DrawBezier( Pens.Pink, inPoint, inPointB, outPointB, outPoint );
			}

		}

		protected override void OnRenderEndangeredConnection( PortView A, Graphics g, Font f )
		{
			foreach ( Port B in A.Port.Connections )
			{
				Point inPoint = A.AnchorPoint;
				Point outPoint = B.PortView.AnchorPoint;
				if ( A.Port.Type == PortType.Out )
				{
					inPoint = B.PortView.AnchorPoint;
					outPoint = A.AnchorPoint;
				}

				Point inPointB = new Point( inPoint.X - 40, inPoint.Y );
				Point outPointB = new Point( outPoint.X + 40, outPoint.Y );

				g.DrawBezier( Pens.PowderBlue, inPoint, inPointB, outPointB, outPoint );
			}
		}

		protected override void OnRenderNode( NodeView nodeView, Graphics g, Font f )
		{
			Node node = nodeView.Node;

			if ( node.Selected )
			{
				g.FillRectangle( Brushes.Blue, nodeView.BodyBoundingBox );
			}
			else
			{
				g.FillRectangle( Brushes.Black, nodeView.BodyBoundingBox );
			}
			g.FillRectangle( Brushes.DarkRed, nodeView.TitleBoundingBox );
			g.DrawString( node.Name, f, Brushes.White, nodeView.TitleBoundingBox.X, nodeView.TitleBoundingBox.Y );



			foreach ( Port port in node.Ports.Values )
			{
				PortView portView = port.PortView;

				g.FillRectangle( Brushes.DarkRed, portView.NameBoundingBox );
				g.DrawString( port.Name, f, Brushes.White, portView.NameBoundingBox.X, portView.NameBoundingBox.Y );
				g.FillRectangle( Brushes.Magenta, portView.ConnectorBoundingBox );
				if ( port.Selected )
				{
					g.DrawRectangle( Pens.LemonChiffon, portView.BoundingBox );
				}
				else
				{
					g.DrawRectangle( Pens.LimeGreen, portView.BoundingBox );
				}
			}
		}

		// This function has to recalculate the bounding boxes of the node and all of its ports.
		protected override void OnUpdateNode( NodeView nodeView, Graphics g, Font f )
		{
			Node node = nodeView.Node;


			Size titleSize = MeasureString( g, node.Name, f );

			nodeView.TitleBoundingBox = new Rectangle( nodeView.Position, titleSize );

			int maxInPortRight = nodeView.Position.X + 6;
			int portOffsetY = nodeView.Position.Y + nodeView.TitleBoundingBox.Height + 4;
			int portOffsetX = nodeView.Position.X + 6;
			foreach ( Port port in node.InPorts.Values )
			{
				PortView portView = port.PortView;

				Size portTitleSize = MeasureString( g, port.Name, f );
				portView.NameBoundingBox = new Rectangle( new Point( portOffsetX, portOffsetY ), portTitleSize );

				Size portConnectorSize = new Size( portTitleSize.Height, portTitleSize.Height );
				portView.ConnectorBoundingBox = new Rectangle( new Point( portOffsetX - portTitleSize.Height - 2, portOffsetY ), portConnectorSize );



				portView.AnchorPoint = new Point( ( int ) ( portView.ConnectorBoundingBox.X + portConnectorSize.Width * 0.5 ), ( int ) ( portView.ConnectorBoundingBox.Y + portConnectorSize.Height * 0.5 ) );

				int maxRight = portView.NameBoundingBox.Right;

				if ( port.ValueControl != null && port.ValueControl.Control != null )
				{
					Control c = port.ValueControl.Control;
					c.Location = new Point( portView.NameBoundingBox.Right + 4, portOffsetY );

					maxRight = c.Right;

					if ( portView.NameBoundingBox.Height < c.Height )
					{
						portOffsetY += c.Height + 2;
					}
					else
					{
						portOffsetY += portView.NameBoundingBox.Height + 2;
					}
				}
				else
				{
					portOffsetY += portView.NameBoundingBox.Height + 2;
				}

				if ( maxInPortRight < maxRight )
				{
					maxInPortRight = maxRight;
				}

				
			}
			int maxOffsetY = portOffsetY;

			int maxOutPortRight = Math.Max( maxInPortRight, nodeView.TitleBoundingBox.Right );
			portOffsetX = maxInPortRight + 12;
			portOffsetY = nodeView.Position.Y + nodeView.TitleBoundingBox.Height + 4;
			foreach ( Port port in node.OutPorts.Values )
			{
				PortView portView = port.PortView;


				int offsetY = portOffsetY;
				int offsetX = portOffsetX;

				if ( port.ValueControl != null && port.ValueControl.Control != null )
				{
					Control c = port.ValueControl.Control;
					c.Location = new Point( offsetX, portOffsetY );

					offsetX = c.Right + 4;

					if ( portView.NameBoundingBox.Height < c.Height )
					{
						portOffsetY += c.Height + 2;
					}
					else
					{
						portOffsetY += portView.NameBoundingBox.Height + 2;
					}
				}
				else
				{
					portOffsetY += portView.NameBoundingBox.Height + 2;
				}

				Size portTitleSize = MeasureString( g, port.Name, f );
				portView.NameBoundingBox = new Rectangle( new Point( offsetX, offsetY ), portTitleSize );

				
				Size portConnectorSize = new Size( portTitleSize.Height, portTitleSize.Height );
				portView.ConnectorBoundingBox = new Rectangle( new Point( portView.NameBoundingBox.Right + 2, offsetY ), portConnectorSize );

				if ( maxOutPortRight < portView.NameBoundingBox.Right )
				{
					maxOutPortRight = portView.NameBoundingBox.Right;
				}

				portOffsetY += portView.NameBoundingBox.Height + 2;
			}
			maxOffsetY = Math.Max( portOffsetY, maxOffsetY );

			// align right
			foreach ( Port port in node.OutPorts.Values )
			{
				PortView portView = port.PortView;
				if ( portView.NameBoundingBox.Right < maxOutPortRight )
				{
					int offsetX = maxOutPortRight - portView.NameBoundingBox.Right;

					portView.NameBoundingBox = OffsetedCopy( portView.NameBoundingBox, offsetX, 0 );
					portView.ConnectorBoundingBox = OffsetedCopy( portView.ConnectorBoundingBox, offsetX, 0 );
				}

				portView.AnchorPoint = new Point( ( int ) ( portView.ConnectorBoundingBox.X + portView.ConnectorBoundingBox.Size.Width * 0.5 ), ( int ) ( portView.ConnectorBoundingBox.Y + portView.ConnectorBoundingBox.Size.Height * 0.5 ) );
			}

			foreach ( Port port in node.Ports.Values )
			{
				PortView portView = port.PortView;

				portView.BoundingBox = JoinBoundingBoxes( portView.NameBoundingBox, portView.ConnectorBoundingBox );
			}

			int maxPortWidth = maxOutPortRight + 6 - nodeView.Position.X;

			nodeView.BodyBoundingBox = new Rectangle( new Point( nodeView.Position.X, nodeView.Position.Y ), new Size( Math.Max( maxPortWidth, titleSize.Width ), maxOffsetY - nodeView.TitleBoundingBox.Top ) );
			nodeView.BoundingBox = InflatedCopy( nodeView.BodyBoundingBox, 10, 0 );

			// grow name
			nodeView.TitleBoundingBox = new Rectangle( nodeView.TitleBoundingBox.Location, new Size( nodeView.BodyBoundingBox.Width, nodeView.TitleBoundingBox.Height ) );
		}

		#endregion

		protected static Rectangle GetRectangle( int x, int y, SizeF size )
		{
			return GetRectangle( new Point( x, y ), size );
		}

		protected static Rectangle GetRectangle( Point position, SizeF size )
		{
			int w = ( int ) size.Width;
			int h = ( int ) size.Height;
			return new Rectangle( position, new Size( w, h ) );
		}

		protected static Size MeasureString( Graphics g, String text, Font f )
		{
			SizeF s = g.MeasureString( text, f );
			return new Size( ( int ) s.Width, ( int ) s.Height );
		}

		protected static Rectangle OffsetedCopy( Rectangle r, int x, int y )
		{
			return new Rectangle( r.X + x, r.Y + y, r.Width, r.Height );
		}

		protected static Rectangle InflatedCopy( Rectangle r, int x, int y )
		{
			return new Rectangle( r.X - x, r.Y - y, r.Width + ( 2 * x ), r.Height + ( 2 * y ) );
		}

		protected static Rectangle JoinBoundingBoxes( Rectangle a, Rectangle b )
		{
			int minX = Math.Min( a.X, b.X );
			int minY = Math.Min( a.Y, b.Y );
			int maxRight = Math.Max( a.Right, b.Right );
			int maxBottom = Math.Max( a.Bottom, b.Bottom );
			return new Rectangle( minX, minY, maxRight - minX, maxBottom - minY );
		}
	}
}
