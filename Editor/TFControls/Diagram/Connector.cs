/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace TFControls.Diagram
{
	[Serializable]
	public class Connector
	{

		public event EventHandler PortConnected;
		public event EventHandler PortDisconnected;

		public Connector()
		{
			ConnectorValidator = new ConnectorValidator();
			InConnectionsUnique = true;
			OutConnectionsUnique = true;
		}

		public void Connect( Port A, Port B )
		{
			if ( ! ConnectorValidator.ValidConnection( A, B ) )
			{
				return;
			}

			if ( InConnectionsUnique )
			{
				if ( A.Type == PortType.In )
				{
					Disconnect( A );
				}
				if ( B.Type == PortType.In )
				{
					Disconnect( B );
				}
			}

			if ( OutConnectionsUnique )
			{
				if ( A.Type == PortType.Out )
				{
					Disconnect( A );
				}
				if ( B.Type == PortType.Out )
				{
					Disconnect( B );
				}
			}

			A.Connections.Add( B );
			B.Connections.Add( A );

			if ( PortConnected != null )
			{
				PortConnected( this, new EventArgs() );
			}
		}

		public void Disconnect( Port A )
		{
			foreach ( Port B in A.Connections )
			{
				B.Connections.Remove( A );
			}

			A.Connections.Clear();

			if ( PortDisconnected != null )
			{
				PortDisconnected( this, new EventArgs() );
			}
		}

		private ConnectorValidator m_connectorValidator;
		private bool m_inConnectionsUnique;
		private bool m_outConnectionsUnique;

		public ConnectorValidator ConnectorValidator
		{
			get { return m_connectorValidator; }
			set { m_connectorValidator = value; }
		}

		public bool InConnectionsUnique
		{
			get { return m_inConnectionsUnique; }
			set { m_inConnectionsUnique = value; }
		}

		public bool OutConnectionsUnique
		{
			get { return m_outConnectionsUnique; }
			set { m_outConnectionsUnique = value; }
		}


	}
}
