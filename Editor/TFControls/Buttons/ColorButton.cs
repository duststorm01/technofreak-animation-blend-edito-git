/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace TFControls.Buttons
{

	public delegate void ColorButtonEventHandler( object sender, EventArgs e );

	public class ColorButton : Button
	{
		public event ColorButtonEventHandler ColorChanged;

		public ColorButton()
		{
			BackColorChanged += new EventHandler( ColorButton_BackColorChanged );
		}


		#region Event Handlers


		private void ColorButton_BackColorChanged( object sender, EventArgs e )
		{
			if ( ColorChanged != null )
			{
				ColorChanged( this, new EventArgs() );
			}
		}

		protected override void OnClick( EventArgs e )
		{
			ColorDialog colorDialog = new ColorDialog();
			colorDialog.Color = BackColor;
			colorDialog.FullOpen = true;

			DialogResult colourResult = colorDialog.ShowDialog();

			if ( colourResult == DialogResult.OK )
			{
				Color = colorDialog.Color;
			}
		}

		#endregion

		#region Attributes
		#endregion

		#region Properties
		public Color Color
		{
			get { return BackColor; }
			set { BackColor = value; }
		}
		#endregion

		private void InitializeComponent()
		{
			this.SuspendLayout();
			this.ResumeLayout( false );

		}
	}
}
