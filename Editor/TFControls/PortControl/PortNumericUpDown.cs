/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFControls.PortControl
{
	public class PortNumericUpDown : IPortControl
	{
		public PortNumericUpDown()
		{
			Control = new System.Windows.Forms.NumericUpDown();

			NumericUpDownControl.Maximum = decimal.MaxValue;
			NumericUpDownControl.Minimum = decimal.MinValue;

			SetDecimalPlaces( 1 );

			NumericUpDownControl.Size = new Size( 45, 20 );

			m_hideOnConnection = true;
		}

		public void SetDecimalPlaces( uint decimalPlaces )
		{
			decimal increment = ( decimal ) ( 1.0 / Math.Pow( 10, decimalPlaces ) );
			NumericUpDownControl.Increment = increment;
			NumericUpDownControl.DecimalPlaces = ( int ) decimalPlaces;
		}

		private System.Windows.Forms.NumericUpDown m_numericUpDownControl;
		public System.Windows.Forms.NumericUpDown NumericUpDownControl
		{
			get
			{
				return m_numericUpDownControl;
			}
			set
			{
				m_numericUpDownControl = value;
			}
		}

		public decimal Maximum
		{
			get { return NumericUpDownControl.Maximum; }
			set { NumericUpDownControl.Maximum = value; }
		}

		public decimal Minimum
		{
			get { return NumericUpDownControl.Minimum; }
			set { NumericUpDownControl.Minimum = value; }
		}

		public void OnValueChanged( object sender, EventArgs e )
		{
			if ( ValueChanged != null )
			{
				ValueChanged( this, new EventArgs() );
			}
		}

		#region IPortControl Members


		public System.Windows.Forms.Control Control
		{
			get
			{
				return m_numericUpDownControl;
			}
			set
			{
				if ( m_numericUpDownControl != null )
				{
					return;
				}
				m_numericUpDownControl = ( System.Windows.Forms.NumericUpDown ) value;
				m_numericUpDownControl.ValueChanged += new EventHandler( OnValueChanged );
			}
		}

		public float Value
		{
			get { return ( float ) NumericUpDownControl.Value; }
			set { NumericUpDownControl.Value = ( decimal ) value; }
		}

		public float MinValue
		{
			get { return (float)Minimum; }
			set
			{
				try
				{
					Minimum = (decimal)value;
				}
				catch (System.Exception)
				{
					Minimum = decimal.MinValue;
				}
				
			}
		}

		public float MaxValue
		{
			get { return (float)Maximum; }
			set
			{
				try
				{
					Maximum = (decimal)value;
				}
				catch (System.Exception)
				{
					Maximum = decimal.MaxValue;
				}
			}
		}

		public event EventHandler ValueChanged;

		bool m_hideOnConnection;

		public bool HideOnConnection
		{
			get { return m_hideOnConnection; }
			set { m_hideOnConnection = value; }
		}

		public void Show()
		{
			if (Control == null)
			{
				return;
			}
			Control.Enabled = true;
			Control.Show();
		}

		public void Hide()
		{
			if (Control == null)
			{
				return;
			}
			Control.Enabled = false;
			Control.Hide();
		}

		public void NotifyConnectionChange(int connections)
		{
			if ( connections == 0 )
			{
				Show();
			}
			else
			{
				if ( HideOnConnection )
				{
					Hide();
				}
			}
		}

		#endregion

	}
}
