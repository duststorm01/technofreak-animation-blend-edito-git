/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (C) 2009 Pau Novau Lebrato

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free Software 
Foundation; either version 2.1 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place - Suite 330, Boston, MA 02111-1307, USA
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace TFControls.PortControl
{
	public class PortTrackBar : IPortControl
	{
		public PortTrackBar()
		{
			Control = new TFControls.TrackBar.TrackBar();

			m_hideOnConnection = true;
		}

		TrackBar.TrackBar m_trackBarControl;

		public TrackBar.TrackBar TrackBarControl
		{
			get { return m_trackBarControl; }
			set { m_trackBarControl = value; }
		}

		public void OnValueChanged( object sender, EventArgs e )
		{
			if ( ValueChanged != null )
			{
				ValueChanged( this, new EventArgs() );
			}
		}


		#region IPortControl Members
		public System.Windows.Forms.Control Control
		{
			get
			{
				return m_trackBarControl;
			}
			set
			{
				if ( m_trackBarControl != null )
				{
					return;
				}

				m_trackBarControl = ( TrackBar.TrackBar ) value;
				m_trackBarControl.ValueChanged += new EventHandler( OnValueChanged );

				
			}
		}


		public float Value
		{
			get { return TrackBarControl.Value; }
			set { TrackBarControl.Value = value; }
		}

		public float MinValue
		{
			get { return TrackBarControl.MinValue; }
			set { TrackBarControl.MinValue = value; }
		}

		public float MaxValue
		{
			get { return TrackBarControl.MaxValue; }
			set { TrackBarControl.MaxValue = value; }
		}

		public event EventHandler ValueChanged;

		bool m_hideOnConnection;

		public bool HideOnConnection
		{
			get { return m_hideOnConnection; }
			set { m_hideOnConnection = value; }
		}

		public void Show()
		{
			if (Control == null)
			{
				return;
			}
			Control.Enabled = true;
			Control.Show();
		}

		public void Hide()
		{
			if (Control == null)
			{
				return;
			}
			Control.Enabled = false;
			Control.Hide();
		}

		public void NotifyConnectionChange(int connections)
		{
			if (connections == 0)
			{
				Show();
			}
			else
			{
				if (HideOnConnection)
				{
					Hide();
				}
			}
		}

		#endregion

	}
}
