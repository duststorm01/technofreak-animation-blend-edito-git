/*
TecnoFreak Animation System Editor
http://sourceforge.net/projects/tecnofreakanima/

Copyright (c) 2008 Pau Novau Lebrato

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TecnoFreak.v2.TecnoFreakPlugin;
using System.Windows.Forms;
using TecnoFreakCommon;

namespace ExamplePlugin
{
	class ExampleNodeAdvanced : PluginNode
	{
		public ExampleNodeAdvanced()
			: base( "Example Advanced Node" )
		{
			Init( 1 );
		}

		public ExampleNodeAdvanced( int blahPorts )
			: base( "Example Advanced Node" )
		{
			Init( blahPorts );
		}

		private void Init( int blahPorts )
		{
			blahPorts = Math.Max( 0, blahPorts );

			RuntimeName = "AdvancedPluginNode";

			CreateAnimationOutPort( "out" );
			for ( int i = 0; i < blahPorts; i++ )
			{
				CreateBlahPort();
			}

			CreateCustomOperationsMenu();
		}

		private void CreateBlahPort()
		{
			CreateAnimationInPort( "blah" + InPorts.Count );
		}

		#region custom menu operations
		private void CreateCustomOperationsMenu()
		{
			CustomOperations = new ToolStripMenuItem();
			CustomOperations.Text = this.Name;

			ToolStripMenuItem addBlahPort = new ToolStripMenuItem( "Add Blah Port" );
			addBlahPort.Click += new EventHandler( addBlahPort_Click );
			CustomOperations.DropDownItems.Add( addBlahPort );
		}

		void addBlahPort_Click( object sender, EventArgs e )
		{
			CreateBlahPort();
		}
		#endregion

		#region xml saving and loading
		public override void WriteCustomXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "number_of_blah_ports" );
			writer.WriteValue( InPorts.Count );
			writer.WriteEndAttribute();
		}

		public override void ReadCustomXmlData( XmlReader reader )
		{
			reader.MoveToAttribute( "number_of_blah_ports" );
			int blahPortsCount = reader.ReadContentAsInt();

			for ( int i = 0; i < blahPortsCount; i++ )
			{
				CreateBlahPort();
			}
		}

		public override void WriteRuntimeXmlData( XmlWriter writer )
		{
			writer.WriteStartAttribute( "blah_ports" );
			writer.WriteValue( InPorts.Count );
			writer.WriteEndAttribute();
		}
		#endregion
	}

	public class ExampleNodeAdvancedBuilder : PluginNodeBuilder
	{
		int m_maxBlahPortsAtCreationTime = 0;
		public ExampleNodeAdvancedBuilder( IAnimable animable )
		{
			if ( animable != null )
			{
				m_maxBlahPortsAtCreationTime = animable.Animations.Count;
			}
		}

		public override PluginNode OnCreateNode()
		{
			DialogResult result = MessageBox.Show( "We can ask the user for input before creating a new node..." );
			if ( result != DialogResult.OK )
			{
				return null;
			}
			
			// And do whatever we want afterwards, of course!
			Random rand = new Random();
			int numberOfBlahports = rand.Next( m_maxBlahPortsAtCreationTime );

			return new ExampleNodeAdvanced( numberOfBlahports );
		}
	}
}
